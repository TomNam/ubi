<div class="container-fluid p-t-80">
    <div class="row">
        <div class="col-sm-12 col-md-3 category-left">
            <div class="row logo-category">
                <div class="col-sm-12 col-md-12">
                    <img src="images/category-regata-logo.png" alt="">
                    <hr class="hidden-sm">
                </div>
            </div><!-- / row -->
            
            <div class="row text-category">
                <hr class="visible-sm">
                <p class="col-sm-12 col-md-12 specs-category">
                    Designed for the new generation of increasingly performant cruisers, the blocks in the YACHT CLUB series follow a new lay-out, which foresees the use of shackles to be fixed, and are guaranteed to meet the highest strength, versatility and
                    design criteria... <a class="text-red read-more-cat" href="">Read all</a> 
                    <!-- The whole block is made to the highest resistance and reliability standards as each component is manifactured by machining and no structural parts are cast or molded. <br> Despite the fact that this range is designed
                    for cruisers, each component guarantees high level performance interms of both load capacity and longevity- features which are generally only off ered by high levels products. <br> Available in sizes ranging from a 40mm sheave diameter
                    (these are the only products on the market with this characteristic). <br> Their layout allows for the use of YC’s both with a traditional mechanical connection or with a textile connection.-->
                </p>
                <div class="col-sm-12 col-md-12 specs-category">
                    <ul class="categoryTech unstyled-list inline-list hidden-sm text-center">
                        <hr>
                        <li><a href="#">TECHNICAL SPECS</a></li>
                        <hr>
                    </ul>
                    <small class="hidden-sm">
                        <p class="text-white">LEGEND</p>
                        <table class="text-white hint-text">
                        <tr>
                            <td width="10%">YC</td>
                            <td width="40%">Yacht Club</td>
                            <td width="10%">TAL</td>
                            <td width="40%">Triple and side becket </td>
                        </tr>
                        <tr>
                            <td>S</td>
                            <td>Single</td>
                            <td>TAC</td>
                            <td>Triple and central backet </td>
                        </tr>
                        <tr>
                            <td>SA</td>
                            <td>Single and becket </td>
                            <td>V</td>
                            <td>Fiddle</td>
                        </tr>
                        <tr>
                            <td>D</td>
                            <td>Double</td>
                            <td>VA</td>
                            <td>Fiddle and becket</td>
                        </tr>
                        <tr>
                            <td>DA</td>
                            <td>Double and becket</td>
                            <td>M</td>
                            <td>Stand-up</td>
                        </tr>
                        <tr>
                            <td>T</td>
                            <td>Triple</td>
                            <td>R</td>
                            <td>Cheek</td>
                        </tr>
                        </table>
                    </small>
                </div>  
            </div> <!-- / row -->

            <div class="row visible-sm mobile-cat">
                <div class="col-sm-4" style="border-right:1px solid #555;text-align:center;">
                    <div class="categoryTech unstyled-list inline-list">
                        <a href="#" class="text-red">TECHNICAL SPECS</a> 
                    </div>
                </div>
                <div class="col-sm-4" style="border-right:1px solid #555;text-align:center;">
                    <div class="categoryTech unstyled-list inline-list">
                        <a href="#" class="text-red">TECHNICAL SHEET</a> 
                    </div>
                </div>
                <div class="col-sm-4" style="border-right:1px solid #555;text-align:center;">    
                    <small>
                        <a href="#" data-toggle="modal" data-target="#myModal" class="text-white legend">LEGEND</a>
                    </small>
                    <!-- Modal -->
                    <div id="myModal" class="modal fade" role="dialog">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>
                                <div class="modal-body">
                                    <table class="text-white hint-text">
                                        <tr>
                                            <td width="10%">YC</td>
                                            <td width="40%">Yacht Club</td>
                                            <td width="10%">TAL</td>
                                            <td width="40%">Triple and side becket </td>
                                        </tr>
                                        <tr>
                                            <td>S</td>
                                            <td>Single</td>
                                            <td>TAC</td>
                                            <td>Triple and central backet </td>
                                        </tr>
                                        <tr>
                                            <td>SA</td>
                                            <td>Single and becket </td>
                                            <td>V</td>
                                            <td>Fiddle</td>
                                        </tr>
                                        <tr>
                                            <td>D</td>
                                            <td>Double</td>
                                            <td>VA</td>
                                            <td>Fiddle and becket</td>
                                        </tr>
                                        <tr>
                                            <td>DA</td>
                                            <td>Double and becket</td>
                                            <td>M</td>
                                            <td>Stand-up</td>
                                        </tr>
                                        <tr>
                                            <td>T</td>
                                            <td>Triple</td>
                                            <td>R</td>
                                            <td>Cheek</td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                            <!-- / modal content-->
                        </div>
                    </div><!--/ myModal -->
                </div>
            </div><!-- / row -->

        </div><!-- / category left -->
        
        <div class="col-sm-12 col-md-9 products-container">
          <div class="categoryFilter clearfix">
            <div class="col-sm-4">
              <small>Select <strong>Category</strong></small>
              <ul class="list-inline">
                <li class="selected-filter">CLASSIC</li>
                <li>ULTRA</li>
                <li>EVE</li>
              </ul>
            </div>
            <div class="col-sm-5 col-lg-4">
              <small>Select <strong>Size</strong></small>
              <ul class="list-inline">
                <li class="selected-filter">60</li>
                <li>80</li>
                <li>100</li>
                <li>125</li>
                <li>150</li>
              </ul>
            </div>
            <div class="col-sm-3 col-lg-4">
              <small>Select <strong>Load</strong></small>
              <ul class="list-inline">
                <li class="selected-filter">LOW</li>
                <li>HIGH</li>
              </ul>
            </div>
          </div>
          <!-- fine filtro -->

          <div class="row" style="clear:both;">
          
          <!-- inizio prodotto -->
            <div class="col-sm-6 product">
                <div class="col-sm-4 productImage">
                    <img src="images/example-product.png" alt="">
                </div>
                <div class="col-sm-8">
                    <table>
                        <tr>
                            <td></td>
                            <td class="productName">RT60</td>
                        </tr>
                        <tr>
                            <td><img src="images/product-diameter.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>ø sheave: <span class="bold text-info">40mm</span></li>
                                    <li>ø shackle: <span class="bold text-info">5mm</span></li>
                                    <li>max ø line: <span class="bold text-info">8mm</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-weight.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>Max working load: <span class="bold text-info">500Kg</span></li>
                                    <li>Breaking loads: <span class="bold text-info">1000Kg</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-size.png" alt=""></td>
                            <td>
                              <ul class="unstyled-list">
                                  <li>Length: <span class="bold text-info">72mm</span></li>
                                  <li>Weight: <span class="bold text-info">60g</span></li>
                              </ul>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Description: <span class="bold text-info">Single, becket</span></td>
                        </tr>
                        <tr>
                            <td class="price"><i class="fa fa-euro"></i></td>
                            <td><strong>123,00</strong> <a href="" class="order"><i class="fa fa-star"></i> Wishlist </a></td>
                        </tr>
                    </table>
                </div>
            </div>
          <!-- fine prodotto -->
          <!-- inizio prodotto -->
            <div class="col-sm-6 product">
                <div class="col-sm-4 productImage">
                    <img src="images/example-product.png" alt="">
                </div>
                <div class="col-sm-8">
                    <table>
                        <tr>
                            <td></td>
                            <td class="productName">RT60</td>
                        </tr>
                        <tr>
                            <td><img src="images/product-diameter.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>ø sheave: <span class="bold text-info">40mm</span></li>
                                    <li>ø shackle: <span class="bold text-info">5mm</span></li>
                                    <li>max ø line: <span class="bold text-info">8mm</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-weight.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>Max working load: <span class="bold text-info">500Kg</span></li>
                                    <li>Breaking loads: <span class="bold text-info">1000Kg</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-size.png" alt=""></td>
                            <td>
                              <ul class="unstyled-list">
                                  <li>Length: <span class="bold text-info">72mm</span></li>
                                  <li>Weight: <span class="bold text-info">60g</span></li>
                              </ul>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Description: <span class="bold text-info">Single, becket</span></td>
                        </tr>
                        <tr>
                            <td class="price"><i class="fa fa-euro"></i></td>
                            <td><strong>123,00</strong> <a href="" class="order"><i class="fa fa-star"></i> Wishlist </a></td>
                        </tr>
                    </table>
                </div>
            </div>
          <!-- fine prodotto -->
          <!-- inizio prodotto -->
            <div class="col-sm-6 product">
                <div class="col-sm-4 productImage">
                    <img src="images/example-product.png" alt="">
                </div>
                <div class="col-sm-8">
                    <table>
                        <tr>
                            <td></td>
                            <td class="productName">RT60</td>
                        </tr>
                        <tr>
                            <td><img src="images/product-diameter.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>ø sheave: <span class="bold text-info">40mm</span></li>
                                    <li>ø shackle: <span class="bold text-info">5mm</span></li>
                                    <li>max ø line: <span class="bold text-info">8mm</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-weight.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>Max working load: <span class="bold text-info">500Kg</span></li>
                                    <li>Breaking loads: <span class="bold text-info">1000Kg</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-size.png" alt=""></td>
                            <td>
                              <ul class="unstyled-list">
                                  <li>Length: <span class="bold text-info">72mm</span></li>
                                  <li>Weight: <span class="bold text-info">60g</span></li>
                              </ul>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Description: <span class="bold text-info">Single, becket</span></td>
                        </tr>
                        <tr>
                            <td class="price"><i class="fa fa-euro"></i></td>
                            <td><strong>123,00</strong> <a href="" class="order"><i class="fa fa-star"></i> Wishlist </a></td>
                        </tr>
                    </table>
                </div>
            </div>
          <!-- fine prodotto -->
          <!-- inizio prodotto -->
            <div class="col-sm-6 product">
                <div class="col-sm-4 productImage">
                    <img src="images/example-product.png" alt="">
                </div>
                <div class="col-sm-8">
                    <table>
                        <tr>
                            <td></td>
                            <td class="productName">RT60</td>
                        </tr>
                        <tr>
                            <td><img src="images/product-diameter.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>ø sheave: <span class="bold text-info">40mm</span></li>
                                    <li>ø shackle: <span class="bold text-info">5mm</span></li>
                                    <li>max ø line: <span class="bold text-info">8mm</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-weight.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>Max working load: <span class="bold text-info">500Kg</span></li>
                                    <li>Breaking loads: <span class="bold text-info">1000Kg</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-size.png" alt=""></td>
                            <td>
                              <ul class="unstyled-list">
                                  <li>Length: <span class="bold text-info">72mm</span></li>
                                  <li>Weight: <span class="bold text-info">60g</span></li>
                              </ul>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Description: <span class="bold text-info">Single, becket</span></td>
                        </tr>
                        <tr>
                            <td class="price"><i class="fa fa-euro"></i></td>
                            <td><strong>123,00</strong> <a href="" class="order"><i class="fa fa-star"></i> Wishlist </a></td>
                        </tr>
                    </table>
                </div>
            </div>
          <!-- fine prodotto -->
          <!-- inizio prodotto -->
            <div class="col-sm-6 product">
                <div class="col-sm-4 productImage">
                    <img src="images/example-product.png" alt="">
                </div>
                <div class="col-sm-8">
                    <table>
                        <tr>
                            <td></td>
                            <td class="productName">RT60</td>
                        </tr>
                        <tr>
                            <td><img src="images/product-diameter.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>ø sheave: <span class="bold text-info">40mm</span></li>
                                    <li>ø shackle: <span class="bold text-info">5mm</span></li>
                                    <li>max ø line: <span class="bold text-info">8mm</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-weight.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>Max working load: <span class="bold text-info">500Kg</span></li>
                                    <li>Breaking loads: <span class="bold text-info">1000Kg</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-size.png" alt=""></td>
                            <td>
                              <ul class="unstyled-list">
                                  <li>Length: <span class="bold text-info">72mm</span></li>
                                  <li>Weight: <span class="bold text-info">60g</span></li>
                              </ul>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Description: <span class="bold text-info">Single, becket</span></td>
                        </tr>
                        <tr>
                            <td class="price"><i class="fa fa-euro"></i></td>
                            <td><strong>123,00</strong> <a href="" class="order"><i class="fa fa-star"></i> Wishlist </a></td>
                        </tr>
                    </table>
                </div>
            </div>
          <!-- fine prodotto -->
          <!-- inizio prodotto -->
            <div class="col-sm-6 product">
                <div class="col-sm-4 productImage">
                    <img src="images/example-product.png" alt="">
                </div>
                <div class="col-sm-8">
                    <table>
                        <tr>
                            <td></td>
                            <td class="productName">RT60</td>
                        </tr>
                        <tr>
                            <td><img src="images/product-diameter.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>ø sheave: <span class="bold text-info">40mm</span></li>
                                    <li>ø shackle: <span class="bold text-info">5mm</span></li>
                                    <li>max ø line: <span class="bold text-info">8mm</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-weight.png" alt=""></td>
                            <td>
                                <ul class="unstyled-list">
                                    <li>Max working load: <span class="bold text-info">500Kg</span></li>
                                    <li>Breaking loads: <span class="bold text-info">1000Kg</span></li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td><img src="images/product-size.png" alt=""></td>
                            <td>
                              <ul class="unstyled-list">
                                  <li>Length: <span class="bold text-info">72mm</span></li>
                                  <li>Weight: <span class="bold text-info">60g</span></li>
                              </ul>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Description: <span class="bold text-info">Single, becket</span></td>
                        </tr>
                        <tr>
                            <td class="price"><i class="fa fa-euro"></i></td>
                            <td><strong>123,00</strong> <a href="" class="order"><i class="fa fa-star"></i> Wishlist </a></td>
                        </tr>
                    </table>
                </div>
            </div>
          <!-- fine prodotto -->
        </div>
        </div>
    </div>
</div>
