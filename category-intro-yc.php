<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">YACHT CLUB</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_YC_INTRO']?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSER']?> </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="110" class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CHEEKS']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_YC_CHEEKS_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_YC_SHEAVE_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_BALLS']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_YC_BALLS_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_OTHER']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_YC_OTHER_DESC']?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/category-yc.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-yc">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_YC_DESC1']?>
        </p>            
        <a class="btn btn-lrg btn-bordered yc-but" href="index.php?p=catalog&idl=4">
            <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>            

    </div><!-- / category-intro-text-->
</div>
<div class="category-intro-text visible-xs-inline-block">
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_YC_DESC1']?>
    </p>            
    <a class="btn btn-lrg btn-bordered yc-but" href="index.php?p=catalog&idl=4">
        <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / category-intro-text-->
<!---------------------- category options ---------------------->
<div class="container-fluid p-b-50"> 
    <div class="col-sm-4 col-sm-offset-1 text-center">
        <img src="images/cat-yc/yc60s.png" class="image-responsive m-t-50" width="100%" alt="">
    </div>
    <div class="col-sm-5 col-md-offset-1 col-lg-offset-1 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-40">YC YACHT CLUB</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=$lang[$_COOKIE['ubi_lang']]['_YC_TECH_INTRO']?>
        </p>
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?><span class="bold"> 40-80 mm</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?><span class="bold"> 8-16 mm</span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?> <span class="bold"> 500-1800 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?><span class="bold"> 1000-3600 Kg</span> <br>
                </td>
            </tr>
        </table>
        <div class="cat-option-button m-t-20">
            <a href="index.php?p=catalog&idl=4" class="btn btn-bordered yc-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

</div><!---------------------- / category options ---------------------->

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-yc/yc-photo-drawing.jpg">
                    <img src="images/cat-yc/yc-photo-drawing.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-yc/yc-drawing.jpg">
                    <img src="images/cat-yc/yc-drawing.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive"> 
                </a>
            </div>
        </div>
    </div>
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-yc/yc-drawing2.jpg">
                    <img src="images/cat-yc/yc-drawing2.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-yc/yc-photo-drawing2.jpg">
                    <img src="images/cat-yc/yc-photo-drawing2.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive"> 
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="yc-esploso"></div>
</div>

<div class="product-carousel">
  <img src="images/cat-yc/yc-slider1.jpg" alt="Yacht Club - slideshow">
  <img src="images/cat-yc/yc-slider2.jpg" alt="Yacht Club - slideshow">
  <img src="images/cat-yc/yc-slider3.jpg" alt="Yacht Club - slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-yc/yc-footer.png" alt="Ubi Maior Italia - Regata Line" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=8" target="_blank" class="btn btn-bordered yc-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_YC_CATALOGUE']?></a>
  </div>
</div>
</div>


