<?php error_reporting(1); include("./lib/dbconn.php"); include('./lib/stdlib.php');
$keyword = $_REQUEST['keyword'];
$arr_prod = array(
    "catalogue" => [],
    "apparel" => []
);
$prodotti = $mysqli->query("SELECT c.*,l.Linea, l.Sigla as codice_linea,t.Tipologia,t.TipologiaEn,t.Sigla as codice_tipologia FROM catalogo c LEFT JOIN linee l ON l.id=c.IdLInea LEFT JOIN tipologie t ON t.Id=c.IdTipologia WHERE l.Linea LIKE '%".$keyword."%' OR t.Tipologia LIKE '%".$keyword."%' OR t.TipologiaEn LIKE '%".$keyword."%' OR c.codice LIKE '%".$keyword."%' OR c.Descrizione LIKE '%".$keyword."%' OR c.DescrizioneInglese LIKE '%".$keyword."%' OR c.Peso LIKE '%".$keyword."%' OR c.MWL LIKE '%".$keyword."%'");
if($prodotti->num_rows > 0){
    while($prodotto = $prodotti->fetch_object()){

        $nomi_voci = $mysqli->query("SELECT Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE lingua=2 AND IdTipologia='".$prodotto->IdTipologia."'");
        $nome_voce = $nomi_voci->fetch_object();


        if($prodotto->Val06){
            $prodotto->voce06label = $nome_voce->Voce06;
        }

        if($prodotto->Val07){
            if($nome_voce->Voce07=='WEIGHT' || $nome_voce->Voce07=='BL'){
                $valori_peso07 = $mysqli->query("SELECT Voce07 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
                $valore_peso07 = $valori_peso07->fetch_object();
            }
            $prodotto->voce07label = $nome_voce->Voce07;
            $prodotto->voce07unit = $valore_peso07->Voce07;
        }

        if($prodotto->Val08){
            if($nome_voce->Voce08=='WEIGHT'){
                $valori_peso08 = $mysqli->query("SELECT Voce08 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
                $valore_peso08 = $valori_peso08->fetch_object();
            }
            $prodotto->voce08label = $nome_voce->Voce08;
            $prodotto->voce08unit = $valore_peso08->Voce08;
        }

        if($prodotto->NPrezzo!=="0"){
            $prodotto->show_price = false;
        }else{
            $prodotto->show_price = true;
        }


        /*GESTIONE PRODUCT UNIT*/
        $valori_unit = $mysqli->query("SELECT Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_unit = $valori_unit->fetch_object();
        /*GESTIONE PRODUCT UNIT*/

        $prodotto->Tipologia = ($_COOKIE['ubi_lang']=='it') ? $prodotto->Tipologia : $prodotto->TipologiaEn;

        $arr_prod['catalogo'][] = $prodotto;
    }
}else{
    $arr_prod['catalogo'] = [];
}

$languages = $mysqli->query("SELECT id from languages WHERE slug='".$_COOKIE['ubi_lang']."'");
$language = $languages->fetch_object();

$query = "SELECT a.*, at.type as product_type, ad.content as description FROM apparel a LEFT JOIN apparel_type at ON at.id=a.id_type LEFT JOIN apparel_description ad ON a.id=ad.id_apparel WHERE ad.id_lang=".$language->id." AND (a.codice LIKE '%".$keyword."%' OR a.taglia LIKE '%".$keyword."%' OR a.prezzo LIKE '%".$keyword."%' OR at.type LIKE '%".$keyword."%' OR ad.content LIKE '%".$keyword."%')";
$prodotti = $mysqli->query($query);
if($prodotti->num_rows>0){
    while($prodotto = $prodotti->fetch_object()){    
        if(!$prodotto->taglia){
            $prodotto->taglia = ($_COOKIE['ubi_lang']==='it') ? 'Taglia Unica' : 'One Size';
        }        
        $arr_prod['apparel'][] = $prodotto;
    }
}else{
    $arr_prod['apparel'] = [];
}


echo json_encode(array('catalogo'=> $arr_prod['catalogo'], 'apparel'=>$arr_prod['apparel']));
?>
