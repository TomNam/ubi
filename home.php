<style media="screen">
  .vela{
    position: absolute;
    top: 230px;
    left: 50%;
    margin-left: -40px;
  }
</style>

<div class="full-height slide-intro">
    <!--<div class="logo_fiera">
        <a href="https://registration.n200.com/survey/3pnntu1l5hjl6?actioncode=NTWO001813QTH&partner-contact=25nlr71rvlp0v&partner-invite=3m090ej91iu2l" target="_blank"><img src="images/mets.png" alt="UbiMaior Italia - Mets trade 2017"></a>
    </div>-->
    <div class="clearfix wrapper-img-home">
        <div class="col-md-6 col-xs-8 col-xs-push-2 col-md-push-0">
            <img src="images/disegni_home.png" alt="Ubi major design" class="img-home image-responsive-height first-image" />
        </div>
        <div class="col-md-6 col-xs-8 col-xs-push-2 col-md-push-0">
            <h1 class="second-image">
                <img src="images/logo_ubi_white.png" alt="Ubi major logo" class="img-home image-responsive-height"/><br>
                <span class="second-claim"><?=$lang[$_COOKIE['ubi_lang']]['_HOME_CLAIM1']?></span>
            </h1>
        </div>
        <!--<img src="images/vela-intro.png" class="vela" alt="">-->
        <div class="subtitle col-xs-10 col-xs-push-1 text-center text-white">
            <h2 class="text-white"><?=$lang[$_COOKIE['ubi_lang']]['_HOME_CLAIM2']?></h2>
        </div>
    </div>
    <div class="text-center p-t-50 xs-p-t-0">
        <img src="images/new_zealand_logo.png" alt="New Zealand Emirates Team">
    </div>
    <div class="mouse-wrapper">
        <div class="mouse">
            <div class="mouse-scroll"></div>
        </div>
    </div>
</div>
<div id="nz-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-sm-push-1 text-center">
                <h2 class="m-b-20"><img src="images/emirates-writing.png" width="100%"> <b>TEAM NEW ZEALAND</b></h2>
                <p class="m-b-50">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_NZ1']?>
                </p>
                <p class="bold-ita">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_NZ2']?>
                </p>
                <p class="fs-13">
                    <em>MAX SIRENA, Emirates Team New Zealand</em>
                </p>
            </div>
        </div>
    </div>
</div>
<div id="who-we-are-section" class="text-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6">
                <h2 class="text-red p-b-25">UBI MAIOR ITALIA&#174;</h2>
                <ul class="unstyled-list feature-list">
                    <li><?=$lang[$_COOKIE['ubi_lang']]['_HOME_CHISIAMO1']?></li>
                    <li><?=$lang[$_COOKIE['ubi_lang']]['_HOME_CHISIAMO2']?></li>
                    <li><?=$lang[$_COOKIE['ubi_lang']]['_HOME_CHISIAMO3']?></li>
                    <li><?=$lang[$_COOKIE['ubi_lang']]['_HOME_CHISIAMO4']?></li>
                </ul>
                <!--<a href="#" class="btn btn-bordered m-t-50">Chi siamo</a>-->
                <div class="row p-t-50">
                    <div class="col-sm-8">
                        <img src="images/home-tech-small.png" class="img-responsive" alt="Ubi major craft">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------- SEZIONE CATEGORIE NUOVA ------------------>
<div id="category-section" class="category-section">
    <div class="container-fluid">
        <div class="row text-center">

            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag feat-yatch"></div>
                    <img src="images/category-yc.png" class="cat-image-size" alt="Featured product X3M">
                    <h3 class="text-white">YC Yacht Club</h3>
                    <p class="m-b-20">
                        <?=$lang[$_COOKIE['ubi_lang']]['_HOME_YC']?>...
                        <a href="index.php?p=category-intro-yc" class="yc-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                    </p>
                    <a href="index.php?p=catalog&idl=4" class="btn btn-bordered yc-but m-b-20">
                        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag feat-regata"></div>
                    <img src="images/category-rt.png" class="cat-image-size" alt="Ubi Maior Italia - product category - Regata">
                    <h3 class="text-white">RT Regata</h3>
                    <p class="m-b-20">
                        <?=$lang[$_COOKIE['ubi_lang']]['_HOME_RT']?> ...
                        <a href="index.php?p=category-intro-regata" class="rt-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                    </p>
                    <a href="index.php?p=catalog&idl=3" class="btn btn-bordered m-b-20 rt-but">
                        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag feat-x3m"></div>
                    <img src="images/category-x3m.png" class="cat-image-size" alt="Featured product X3M">
                    <h3 class="text-white">X3M Flight</h3>
                    <p class="m-b-20">
                        <?=$lang[$_COOKIE['ubi_lang']]['_HOME_X3M']?> ...
                        <a href="index.php?p=category-intro-x3m" class="x3m-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                    </p>
                    <a href="index.php?p=catalog&idl=2" class="btn btn-bordered x3m-but m-b-20">
                        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag feat-jiber"></div>
                    <img src="images/category-jiber.png" class="cat-image-size" alt="Featured product Jiber">
                    <h3 class="text-white">JB Jiber</h3>
                    <p class="m-b-20">
                        <?=$lang[$_COOKIE['ubi_lang']]['_HOME_JB']?> ...
                        <a href="index.php?p=category-intro-jiber" class="jib-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                    </p>
                    <a href="index.php?p=catalog&idl=8" class="btn btn-bordered jib-but m-b-20">
                        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag feat-avvolgitori"></div>
                    <img src="images/category-fr.png" class="cat-image-size" alt="Featured product FR">
                    <h3 class="text-white">FR Avvolgitori</h3>
                    <p class="m-b-20">
                        <?=$lang[$_COOKIE['ubi_lang']]['_HOME_FR']?> ...
                        <a href="index.php?p=category-intro-fr" class="fr-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                    </p>
                   <a href="index.php?p=catalog&idl=1" class="btn btn-bordered fr-but m-b-20">
                        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 col-lg-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag feat-accessories"></div>
                    <img src="images/category-accessories.png" class="cat-image-size" alt="Featured product FR">
                    <h3 class="text-white">Accessories</h3>
                    <p class="m-b-20">
                        <?=$lang[$_COOKIE['ubi_lang']]['_HOME_ACC']?> ...
                        <a href="index.php?p=category-intro-accessories" class="acc-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                    </p>
                    <a href="index.php?p=catalog&idl=5" class="btn btn-bordered acc-but m-b-20">
                        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                    </a>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 bottom-category">
                <div class="col-sm-12 featured-product">
                    <div class="feature-flag feat-sailmakers"></div>
                    <img src="images/category-sailmakers.png" class="cat-image-size" alt="Featured product FR">
                    <div class="content-right">
                        <h3 class="text-white">Sailmakers</h3>
                        <p class="m-b-20">
                            <?=$lang[$_COOKIE['ubi_lang']]['_HOME_SAIL']?> ...
                            <a href="index.php?p=category-intro-sailmakers" class="sail-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                        </p>
                        <a href="pdf/UbiMaiorItalia_2017_Catalog_eng.pdf#page=58" target="_blank" class="btn btn-bordered sail-but m-b-20">
                            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 bottom-category">
                <div class="col-sm-12 featured-product">
                    <div class="feature-flag feat-custom"></div>
                    <img src="images/category-custom.png" class="cat-image-size" alt="Featured product FR">
                    <div class="content-right">
                        <h3 class="text-white">Custom</h3>
                        <p class="m-b-20">
                            <?=$lang[$_COOKIE['ubi_lang']]['_HOME_CUST']?> ...
                            <a href="index.php?p=category-intro-custom" class="cust-but readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                        </p>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 bottom-category">
                <div class="col-sm-12 featured-product">
                    <div class="feature-flag feat-apparel"></div>
                    <img src="images/category-apparel.png" class="cat-image-size" alt="Featured product Apparel">
                    <div class="content-right">
                        <h3 class="text-white">Apparel</h3>
                        <p class="m-b-20">
                            <?=$lang[$_COOKIE['ubi_lang']]['_HOME_APPAREL']?> ...
                        </p>
                        <a href="index.php?p=apparel" class="btn btn-bordered apparel-but m-b-20">
                            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 bottom-category">
                <div class="col-sm-12 featured-product">
                    <div class="feature-flag feat-brands"></div>
                    <img src="images/category-otherbrands.png" class="cat-image-size" alt="Featured product Other brands">
                    <div class="content-right">
                        <h3 class="text-white"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_OTHER_BRANDS']?></h3>
                        <p class="m-b-20">
                            <?=$lang[$_COOKIE['ubi_lang']]['_HOME_OTHER_BRANDS']?> ...
                            <a href="index.php?p=other-brands" class="brands-color readMore"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_READMORE']?> &raquo;</i></a>
                        </p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="warranty text-center m-t-20 m-b-60">
    <a class="btn btn-bordered jib-but" rel="group" href="pdf/WLG-<?=$_COOKIE['ubi_lang']?>.pdf" target="_blank"> <span class="fa fa-file-pdf-o"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_HOME_WARRANTY']?></a>
</div>

<section>  
    <style>
        .item_box{
        height:500px;
    }
    
    .photo-thumb{
        width:90%;
        height:auto;
        border: thin solid #d1d1d1;
        margin:0 1em .5em 0;
        float:left; 
    }
    </style>
    <?php
    // use this instagram access token generator http://instagram.pixelunion.net/
    $access_token="1233663761.1677ed0.1151af0884c24a94ae8b7d5c92c8b52c";
    $photo_count=12;
        
    $json_link="https://api.instagram.com/v1/users/self/media/recent/?";
    $json_link.="access_token={$access_token}&count={$photo_count}";
    $json = file_get_contents($json_link);
    $obj = json_decode($json, true, 512, JSON_BIGINT_AS_STRING);

    ?>    
    <div>
        <div class="col-xs-12 text-center m-b-15">            
            <h3>
                <a class="text-fff" href="https://www.instagram.com/ubimaioritalia/" target="_blank">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_INSTA']?> @ubimaioritalia
                </a>
            </h3>
        </div>
    </div>
    <div>
       <?php
       foreach($obj["data"] as $key => $value){
           ?>
           <div class="col-xs-6 col-sm-4 col-md-4 col-lg-3 m-b-30">
               <img src="<?=$value["images"]["standard_resolution"]["url"]?>" alt="Instafeed - UbiMaior" class="img-responsive" style=" width: 90%;margin-left: 5%;margin-top: 5%;">
           </div>
           <?php
       }
       ?>
    </div>
</section>

<div class="spacer50"></div>

<div id="carousel-section">
  <div class="product-carousel">
    <img src="images/home-gal-0.jpg" alt="UbiMaior Italia - Emirates New Zealand Team">
    <img src="images/home-gal-0.2.jpg" alt="UbiMaior Italia - Emirates New Zealand Team">
    <img src="images/home-gal-1.jpg" alt="UbiMaior Italia - Emirates New Zealand Team">
    <img src="images/home-gal-4.jpg" alt="UbiMaior Italia - SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS">
    <img src="images/home-gal-5.jpg" alt="UbiMaior Italia - SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS">
  </div>
</div>
