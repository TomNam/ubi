<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">X3M FLIGHT</h1>
            <h5 class="text-white hint-text">
        X3M FLIGHT blocks are designed to support extremely heavy weights, and boast a highly beneficial load /dimension/ weight ratio. These blocks combine technology, functionality and design, and are designed to be used exclusively with textile fixings (included)
      </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i>Look closer </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td class="all-caps text-white">Structure and sheave</td>
                        <td>Made of AISI 316 STEEL (optionally in TITANIUM) and ALUMINIUM alloy 6082T6 silver ion-anodized 100 % machined from a single piece</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">Body</td>
                        <td>Made of POM.C RESIN, molded to contain the rigging</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">Bearing</td>
                        <td>Made of TECHNOPOLYMER, to ensure exceptional performance. No maintenance is required</td>

                    </tr>
                    <tr>
                        <td class="all-caps text-white">Fixing</td>
                        <td>Unidirectional covered DYNEEMA&#174; loop 17-4pH STEEL T-Bone</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">Other</td>
                        <td>Brand strap with a VELCRO&#174; lock</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/x3m/x3m-first.png" alt="X3M" class="image-responsive-height">
        </div>
    </div>
</div>
<div class="bg-white clearfix x3m-ft features-img-right">
    <div class="container-fluid clearfix  p-b-50">
        <div class="row equal p-t-50">
            <div class="col-md-5 col-sm-8 text-center">
                <h2 class="padding-20">
                  Features
                </h2>
                <p class="fs-15 m-b-50">
                    On X3M FLIGHT blocks, the load is centralized, thus releasing both the primary and the trasversal loads on technopolymer bearings, which allow for incredbly heavy loads (even static) and no maintenance. <br> The unidirectional covered
                    Dyneema loop acts on an aluminum/inox body (with an aluminum/titanium option), holding the bearing.
                </p>
                <img src="images/x3m/x3m-esploso.png" alt="X3M" class="img-responsive">
                <p class="fs-15 m-t-50">
                    All the aluminum elements are silver ion-anodized to prevent bacterial growths or mold on the loop. The body, made of POM.C resin, carries the rigging and holds the loop, thanks to a VELCRO&#174; strap. <br> The block is fixed by a steel
                    T-Bone This product received the Quality Design Award at Sea-Tec in Carrara
                </p>
                <a href="#" class="btn btn-gray btn-bordered m-t-30 all-caps">Technical Specifications</a>

            </div>
            <div class="col-sm-6 clearfix"></div>
        </div>
    </div>
</div>
<div class="container p-t-50 p-b-50">
    <div class="col-sm-6 text-center bordered-right">
        <img src="images/x3m/x3m-tech.png" alt="X3M technical details" class="img-responsive">
    </div>
    <div class="col-sm-6 p-l-40">
        <h2 class="text-white m-t-40">
          Technical details
        </h2>
        <p class="fs-15 text-white m-t-20">
            The measures indicated are the minimum and maximum range. <br> For the single product's details see the catalog
        </p>
        <table id="table-tech" class="m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-radius.png" /></td>
                <td>
                    SHEAVE <span class="bold">32-120 mm</span> <br>
                    LINE MAX <span class="bold">8-28 mm</span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                  MAX WORKING LOAD <span class="bold">1500-15000 Kg</span> <br>
                  BREAKING LOAD  <span class="bold">3000-30000 Kg</span> <br>
                  WEIGHT <span class="bold">58-1749 gr</span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>TOTAL LENGTH <span class="bold">76-271 mm</span></td>
            </tr>
        </table>
    </div>
</div>
<div class="product-carousel">
  <img src="images/x3m/x3m_gal_1.jpg" alt="X3M slideshow">
  <img src="images/x3m/x3m_gal_2.jpg" alt="X3M slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/x3m/x3m-footer.png" alt="" class="img-responsive">
    <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=26" target="_blank" class="btn btn-bordered m-t-50">SEE ALL X3M'S PRODUCTS</a>
  </div>
</div>
</div>
