<div class="row wish--20 text-center">

    <div class="col-xs-3 col-sm-1 p-l-0">
        <h5 class="text-white">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_IMAGE']?>
        </h5>
    </div>

    <div class="col-xs-4 col-sm-3 p-l-0">
        <h5 class="text-white">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_CODE']?>
        </h5>
    </div>

    <div class="col-sm-2 p-l-0 hidden-xs">
        <h5 class="text-white">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?>
        </h5>
    </div>

    <div class="col-sm-2 p-l-0 hidden-xs">
        <h5 class="text-white">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SIZES']?>
        </h5>
    </div>

    <div class="col-sm-2 p-l-0 hidden-xs">
        <h5 class="text-white">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRICE']?>
        </h5>
    </div>

    <div class="col-xs-4 col-sm-2 p-l-0">
        <h5 class="text-white">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_OPTIONS']?>
        </h5>
    </div>
</div>

<div class="cerca-whishlist-apparel-results scrollbar"></div>