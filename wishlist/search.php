<div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs nav-justified" role="tablist">
        <li role="presentation" class="active"><a href="#catalogue" aria-controls="catalogue" role="tab" data-toggle="tab">Catalogo</a></li>
        <li role="presentation"><a href="#apparel" aria-controls="apparel" role="tab" data-toggle="tab">Apparel</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="catalogue">
            <?php include('./wishlist/catalogue.php'); ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="apparel">
            <?php include('./wishlist/apparel.php'); ?>
        </div>    
    </div>

</div>