<div class="row logo-category">
    <div class="col-sm-12 col-md-12">
        <img src="<?=$public_url?>/images/category-logo-apparel.png" alt="apparel - ubi maior italia">
        <hr class="hidden-sm">
    </div>
</div><!-- / row -->

<div class="row text-category">
    <hr class="visible-sm">
    <p class="col-sm-12 col-md-12 specs-category">
        <?=$lang[$_COOKIE['ubi_lang']]['_APPAREL_INTRO']?>
    </p>

    <div class="col-sm-12 col-md-12 specs-category">
        <ul class="categoryTech unstyled-list inline-list hidden-sm text-center">
            
            
            <p><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SIZING_GUIDE']?></p>
            <a class="fancybox" href="../images/cat-apparel/anatomic-sizing-guide-big.jpg"><img src="../images/cat-apparel/anatomical-sizing-guide-sml.jpg" alt="" class="img-responsive"></a>
            <div class="spacer20"></div>            
        </ul>
    </div>
</div> <!-- / row -->

<!-- MOBILE SIDEBAR -->
 <div class="row visible-sm mobile-cat">    
    <div class="col-sm-6" style="border-right:1px solid #555;text-align:center;">
        <div class="categoryTech unstyled-list inline-list">                       
            <p><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SIZING_GUIDE']?></p>
            <a class="fancybox" href="../images/cat-apparel/anatomic-sizing-guide-big.jpggit"><img src="../images/cat-apparel/anatomical-sizing-guide-sml.jpg" alt="" class="img-responsive"></a>
            <div class="spacer20"></div>            
        </div>
    </div>
</div>
