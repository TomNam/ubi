<?php

$languages = $mysqli->query("SELECT id from languages WHERE slug='".$_COOKIE['ubi_lang']."'");
$language = $languages->fetch_object();

$query = "SELECT a.*, at.type as product_type, ad.content as description FROM apparel a LEFT JOIN apparel_type at ON at.id=a.id_type LEFT JOIN apparel_description ad ON a.id=ad.id_apparel WHERE ad.id_lang=".$language->id." AND a.id=".$_REQUEST['id'];

$prodotti = $mysqli->query($query);
if($prodotti->num_rows>0){
    while($prodotto = $prodotti->fetch_object()){
        ?>
        <div class="col-sm-6 product">.
            <div class="col-sm-4 productImage text-center">
                <img src="../img/apparel/<?=$prodotto->cover?>" style="margin:0 auto;width:auto !important;max-width:100%;max-height:250px !important;" alt="Cover Prodotto Apparel - Ubi Maior Italia">
            </div>
            <div class="col-sm-8">
                <table>
                    <tr>
                        <td></td>
                        <td class="productName">
                            <?=$prodotto->codice?>
                            <?php if($prodotto->is_stock){ ?>
                                <span class="label label-success">IN STOCK</span>
                            <?php }else{ ?>
                                <span class="label label-default">NON IN STOCK</span>
                            <?php  } ?>
                        </td>
                    </tr>
                    <tr height="20"><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td></td>
                        <td>                            
                            <ul class="unstyled-list">                                                               
                                <li style="display:inline-block;margin-right:5px;"><?=($_COOKIE['ubi_lang']) === 'it' ? 'Taglie disponibili' : 'Available sizes'?>:</li>
                                <?php 
                                if($prodotto->taglia){
                                    $taglie = explode(',',$prodotto->taglia);
                                    foreach ($taglie as $taglia) {
                                        ?>
                                        <li style="display:inline-block;">
                                            <span class="label label-inverse"><?=$taglia?></span>
                                        </li>
                                        <?php
                                    }  
                                }else{
                                    ?>
                                    <li style="display:inline-block;">
                                        <span class="label label-inverse"><?=($_COOKIE['ubi_lang']==='it') ? 'TAGLIA UNICA' : 'ONE SIZE'?></span>
                                    </li>
                                    <?php
                                }                                      
                                ?>
                            </ul>
                        </td>
                    </tr>
                    <tr height="20"><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td></td>
                        <td><?=$prodotto->description?></td>
                    </tr>     
                    <tr height="20"><td colspan="2">&nbsp;</td></tr>                                    
                    <tr>
                        <td class="price">
                            <i class="fa fa-euro"></i>
                        </td>
                        <td>
                            <strong><?=number_format((float)$prodotto->prezzo, 2, ',', '')?></strong>* 
                            <a href="javascript:void(0);" class="order add-to-whishlist" data-id="<?=$prodotto->id?>" data-codice="<?=$prodotto->codice?>" data-price="<?=$prodotto->prezzo?>" data-taglie="<?=$prodotto->taglia?>" data-cover="<?=$prodotto->cover?>" data-description="<?=$prodotto->description?>">
                                <i class="fa fa-star"></i> Wishlist
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br>*<?=$lang[$_COOKIE[ubi_lang]][_CATALOGUE_IVA]?></td>
                    </tr>
                    <tr height="60"><td colspan="2">&nbsp;</td></tr>                                    
                </table>
            </div>
            <div class="col-sm-12">
                <div class="row">
                    <?php 
                    $immagini = $mysqli->query("SELECT * FROM apparel_images WHERE id_apparel=".$prodotto->id." ORDER BY priority");
                    if($immagini->num_rows > 0){
                        while($immagine = $immagini->fetch_object()){
                            ?>
                            <div class="col-sm-2">
                                <a class="fancybox" rel="apparel<?=$prodotto->id?>" href="img/apparel/<?=$immagine->name?>">
                                    <img src="img/apparel/<?=$immagine->name?>" class="img-responsive apparel-secondary-img" alt="Prodotto <?=$prodotto->codice?> Apparel - Immagini Secondarie">
                                </a>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>  
        <?php     
    }
}else{
    ?>
    <div class="row m-t-50" style="min-height:400px;">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12 text-center">
            <h3>ERROR! No Product found</h3>
        </div>
    </div>
    <?php
}