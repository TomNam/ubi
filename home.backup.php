<div class="full-height slide-intro">
    <!--<div class="logo_fiera">
        <a href="https://registration.n200.com/survey/0qz5j2nzb247d?actioncode=NTWO003178GSM&partner-contact=25nlr71rvlp0v" target="_blank"><img src="images/mets.png" alt="Mets trade 2016"></a>
    </div>-->
    <div class="clearfix wrapper-img-home">
        <div class="col-md-6 col-xs-8 col-xs-push-2 col-md-push-0">
            <img src="images/disegni_home.png" alt="Ubi major design" class="img-home image-responsive-height first-image" />
        </div>
        <div class="col-md-6 col-xs-8 col-xs-push-2 col-md-push-0">
            <h1 class="second-image">
                <img src="images/logo_ubi_white.png" alt="Ubi major logo" class="img-home image-responsive-height"/><br>
                <span class="second-claim">SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS</span>
            </h1>            
        </div>
        <div class="subtitle col-xs-10 col-xs-push-1 text-center text-white">
            <h2 class="text-white">We equip your passion</h2>
        </div>
    </div>
    <div class="text-center p-t-50 xs-p-t-0">
        <img src="images/new_zealand_logo.png" alt="New Zealand Emirates Team">
    </div>
    <div class="mouse-wrapper">
        <div class="mouse">
            <div class="mouse-scroll"></div>
        </div>
    </div>
</div>
<div id="nz-section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4 col-sm-push-1 text-center">
                <h2 class="m-b-20"><img src="images/emirates-writing.png" width="100%"> <b>TEAM NEW ZEALAND</b></h2>
                <p class="m-b-50">
                    For the year 2017 UBI MAIOR ITALIA&#174; has been chosen to be official supplier of the Emirates Team New Zealand
                </p>
                <p class="bold-ita">
                    "We chose UBI MAIOR ITALIA&#174; for the high quality of their products as well as their flexibility in creating custom parts"
                </p>
                <p class="fs-13">
                    <em>MAX SIRENA, Emirates Team New Zealand</em>
                </p>
            </div>
        </div>
    </div>
</div>
<div id="who-we-are-section" class="text-white">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-6 col-md-offset-6">
                <h2 class="text-red p-b-25">UBI MAIOR ITALIA&#174;</h2>
                <ul class="unstyled-list feature-list">
                    <li><span class="bold">50 years</span> of experience</li>
                    <li>Functional and constructive <span class="bold">quality</span></li>
                    <li><span class="bold">Craft</span> knowledge, <span class="bold">modern</span> techniques</li>
                    <li><span class="bold">Performance, reliability and durability</span></li>
                </ul>
                <!--<a href="#" class="btn btn-bordered m-t-50">Chi siamo</a>-->
                <div class="row p-t-50">
                    <div class="col-sm-8">
                        <img src="images/home-tech-small.png" class="img-responsive" alt="Ubi major craft">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="featured-section">
    <div class="container">
        <div class="row text-center">
            <div class="col-md-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag"></div>
                    <img src="images/featured-x3m.png" class="" alt="Featured product X3M">
                    <h3 class="text-white">X3M</h3>
                    <p class="m-b-20">X3M FLIGHT blocks are designed to support extremely heavy weights, and boast a highly beneficial load /dimension/ weight ratio. </p>
                    <a href="index.php?p=x3m" class="btn btn-bordered m-b-20">
                        <i class=" m-r-10 fa fa-plus-circle"></i> Read More
                    </a>
                    <a href="https://dl.dropboxusercontent.com/u/3158723/DataSheet-X3MSerie.pdf" target="_blank" class="btn btn-bordered m-b-20">
                        <i class=" m-r-10 fa fa-arrows-v"></i> Tech spec
                    </a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag"></div>
                    <img src="images/featured-jiber.png" class="" alt="Featured product Jiber">
                    <h3 class="text-white">Jiber-TX</h3>
                    <p class="m-b-20">JIBER-TX is a structural jib furler suitable for composite and textile forestay. </p>
                    <a href="index.php?p=jiber-tx" class="btn btn-bordered m-b-20">
                        <i class=" m-r-10 fa fa-plus-circle"></i> Read More
                    </a>
                   <!--<a href="#" class="btn btn-bordered m-b-20">
                        <i class=" m-r-10 fa fa-arrows-v"></i> Tech spec
                    </a>-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="col-sm-12 featured-product padding-20">
                    <div class="feature-flag"></div>
                    <img src="images/featured-fr.png" class="" alt="Featured product FR">
                    <h3 class="text-white">FR250RWm</h3>
                    <p class="m-b-20">FR AVVOLGITORI are extremely reliable and functional for managing bow sails, both code zero and gennaker. </p>
                    <a href="index.php?p=fr250rw" class="btn btn-bordered m-b-20">
                        <i class=" m-r-10 fa fa-plus-circle pull-left"></i> <span class="pull-right">Read More</span>
                    </a>
                   <a href="https://dl.dropboxusercontent.com/u/3158723/DataSheet-FR250mSerie.pdf" target="_blank" class="btn btn-bordered m-b-20">
                        <i class=" m-r-10 fa fa-arrows-v"></i> Tech spec
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!---------------- SEZIONE CATEGORIE ------------------>
<div id="category-section" class="m-t-50  clearfix">
    <div class="product-box col-md-6 padding-40 clearfix">
        <div class="col-sm-4 ">
            <img src="images/category-yc.png" class="img-responsive m-t-50" alt="">
        </div>
        <div class="col-sm-8 text-right text-white">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="text-white m-b-30">YC Yacht Club</h2>
            <p class="fs-15 m-b-30">
                Designed for the new generation
                of increasingly performant cruisers,
                the blocks in the YACHT CLUB series
                follow a new lay-out, which foresees the use of
                shackles to be fixed, and are guaranteed to meet the
                highest strength, versatility and design criteria.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=26" target="_blank" class="btn btn-bordered btn-white yc-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
        <div class="col-sm-4 ">
            <img src="images/category-rt.png" class="img-responsive m-t-50" alt="">
        </div>
        <div class="col-sm-8 text-right">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30">RT Regata</h2>
            <p class="fs-15 m-b-30">
                The RT line turns 30 years of evolution in this field into a
                truly ALL ROUND range of components
                designed for any kind of boat.
                RT blocks can bear very heavy weights and ensure
                the smoothest running mechanism thanks to
                TORLON (HIGH LOAD line) or DELRIN (LOW LOAD line) roller cage.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=12" target="_blank" class="btn btn-bordered rt-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
        <div class="col-sm-8">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30">X3M Flight </h2>
            <p class="fs-15 m-b-30">
                X3M FLIGHT blocks are designed to
                support extremely heavy weights, and boast a highly
                beneficial load /dimension/ weight ratio.
                These blocks combine technology, functionality and design,
                and are designed to be used exclusively
                with textile fixings (included).
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=36" target="_blank" class="btn btn-bordered x3m-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
        <div class="col-sm-4 ">
            <img src="images/category-x3m.png" class="img-responsive m-t-50" alt="">
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 clearfix">
        <div class="col-sm-8">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30 text-white">Jiber</h2>
            <p class="fs-15 m-b-30 text-white">
                JIBER is a structural jib or genoa furler that
                works without the usual aluminum extruded foils.
                The rod forestay is linked to the drum and swivel, 
                and rotates to transmit the torque and furl the sail.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=8" target="_blank" class="btn btn-bordered jib-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
        <div class="col-sm-4 ">
            <img src="images/category-jiber.png" class="img-responsive m-t-50" alt="">
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 clearfix">
        <div class="col-sm-4 ">
            <img src="images/category-fr.png" class="img-responsive m-t-50" alt="">
        </div>
        <div class="col-sm-8 text-right">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30 text-white">FR Avvolgitori</h2>
            <p class="fs-15 m-b-30 text-white">
                FR AVVOLGITORI are extremely reliable
                and functional for managing bow sails,
                both code zero and gennaker.
                This complete line ranges from products suited to
                dinghy and smaller vessels, right up to larger units and
                custom designed products.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=30" target="_blank" class="btn btn-bordered fr-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
        <div class="col-sm-4 ">
            <img src="images/category-accessories.png" class="img-responsive m-t-50" alt="">
        </div>
        <div class="col-sm-8 text-right">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30">Accessories</h2>
            <p class="fs-15 m-b-30">
                UBI MAIOR ITALIA&#174; offers a complete range of rigging equipment and accessories.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=48" target="_blank" class="btn btn-bordered acc-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
        <div class="col-sm-4 ">
            <img src="images/category-sailmakers.png" class="img-responsive m-t-50" alt="">
        </div>
        <div class="col-sm-8 text-right">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30">Sailmakers</h2>
            <p class="fs-15 m-b-30">
                UBI MAIOR ITALIA&#174; produces a series of technical accessories for sailmakers.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=58" target="_blank" class="btn btn-bordered sail-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
    </div>

    <div class="product-box col-md-6 padding-40 clearfix">
        <div class="col-sm-4 ">
            <img src="images/category-custom.png" class="img-responsive m-t-50" alt="">
        </div>
        <div class="col-sm-8 text-right">
            <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
            <h2 class="m-b-30 text-white">Custom</h2>
            <p class="fs-15 m-b-30 text-white">
                Thanks to the company's in house production facilities,
                UBI MAIOR ITALIA&#174; is able to realize custom projects of
                all types, easily and quickly.
            </p>
            <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=62" target="_blank" class="btn btn-bordered cust-but"><i class="fa fa-plus m-r-10"></i> Learn more</a>
            <a href="#" target="_blank" class="btn btn-bordered">
                <i class=" m-r-20 fa fa-arrows-v"></i> Tech spec
            </a>
        </div>
    </div>
    <div style="clear:both"></div>
</div>
<!---------------- SEZIONE CAROUSEL ------------------>
<div id="carousel-section">
  <div class="product-carousel">
    <img src="images/home-gal-1.jpg" alt="X3M slideshow">
    <img src="images/home-gal-2.jpg" alt="X3M slideshow">
    <img src="images/home-gal-3.jpg" alt="X3M slideshow">
    <img src="images/home-gal-4.jpg" alt="X3M slideshow">
    <img src="images/home-gal-5.jpg" alt="X3M slideshow">
  </div>
</div>
