<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_TITLE']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_INTRO']?>
            </h5>
            <ul class="anchor-brands">
                <li><a href="#carbonautica"><img src="images/cat-brands/brands-carbonautica.jpg" alt="UbiMaior Italia - Dealer Carbonautica" class="image-responsive-height"></a></li>
                <li><a href="#nubeway"><img src="images/cat-brands/brands-nubeway.jpg" alt="UbiMaior Italia - Dealer Nubeway" class="image-responsive-height"></a></li>
            </ul>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/category-otherbrands.png" alt="UbiMaior Italia - Sailing Brands" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-brands">
    <!--<div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <ul class="m-b-50 anchor-list">
            <li>
                <img src="images/cat-accessories/ring-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#rings"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/shackles-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#shackles"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/halyard-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#halyard"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/snatchblock-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#snatchblocks"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/crossover-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#crossover"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/padeye-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#padeye"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/organisers-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#organisers"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS']?></a>
            </li>
        </ul>       

        <a class="btn btn-lrg btn-bordered acc-but" href="index.php?p=catalog&idl=5">
            <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>                

    </div>--><!-- / category-intro-text-->
</div>

<!-- mobile category-intro-text-->
<!--<div class="category-intro-text visible-xs-inline-block">
    <ul class="m-b-50 row anchor-list">
        <li class="col-xs-6">
            <img src="images/cat-accessories/ring-anchor.png" alt="Ubimaior Italia - Accessories Rings"><br>
            <a href="#rings"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/shackles-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#shackles"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/halyard-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#halyard"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/snatchblock-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#snatchblocks"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/crossover-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#crossover"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/padeye-anchor.png" alt="Ubimaior Italia - Accessories Rings"><br>
            <a href="#padeye"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/organisers-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#organisers"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS']?></a>
        </li>
    </ul>
    <a class="btn btn-lrg btn-bordered acc-but" href="index.php?p=catalog&idl=5">
        <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div>--><!-- / mobile category-intro-text-->


<!-- CARBONAUTICA -->
<div class="container carbonautica-container">
    <a id="carbonautica"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-12">
            <h1 class="text-white m-b-30 m-t-50"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_CARBONAUTICA_TITLE']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_CARBONAUTICA_INTRO']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="brands-txt"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_CARBONAUTICA_TXT1']?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="images/cat-brands/carbonautica-carbon1.jpg">
                <img src="images/cat-brands/carbonautica-carbon1.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="images/cat-brands/carbonautica-carbon2.jpg">
                <img src="images/cat-brands/carbonautica-carbon2.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>    
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="images/cat-brands/carbonautica-carbon3.jpg">
                <img src="images/cat-brands/carbonautica-carbon3.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>    
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_CARBONAUTICA_TXT2']?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="images/cat-brands/carbonautica-fiber1.jpg">
                <img src="images/cat-brands/carbonautica-fiber1.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a>     
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="images/cat-brands/carbonautica-fiber2.jpg">
                <img src="images/cat-brands/carbonautica-fiber2.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a> 
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group" href="images/cat-brands/carbonautica-fiber3.jpg">
                <img src="images/cat-brands/carbonautica-fiber3.jpg" width="75%" alt="Ubi Maior Italia - Carbonautica Dealer">
            </a> 
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_CARBONAUTICA_TXT3']?></div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="http://www.carbonautica.com/" target="blank" class="btn btn-bordered brands-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_BUT']?></a>
        </div>
    </div>
</div>

<div class="darker-divider"></div>


<!-- NUBEWAY -->
<div class="container carbonautica-container">
    <a id="nubeway"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-12">
            <h1 class="text-white m-b-40 m-t-60"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_NUBEWAY_TITLE']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_NUBEWAY_INTRO']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="brands-txt"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_NUBEWAY_TXT1']?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="images/cat-brands/nubeway1.jpg">
                <img src="images/cat-brands/nubeway1.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>     
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="images/cat-brands/nubeway2.jpg">
                <img src="images/cat-brands/nubeway2.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="images/cat-brands/nubeway3.jpg">
                <img src="images/cat-brands/nubeway3.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_NUBEWAY_TXT2']?></div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="images/cat-brands/nubeway4.jpg">
                <img src="images/cat-brands/nubeway4.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="images/cat-brands/nubeway5.jpg">
                <img src="images/cat-brands/nubeway5.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="col-sm-4 text-center brands-imgs">
            <a class="fancybox" rel="group1" href="images/cat-brands/nubeway6.jpg">
                <img src="images/cat-brands/nubeway6.jpg" width="75%" alt="Ubi Maior Italia - Nubeway dealer">
            </a>
        </div>
        <div class="spacer20"></div>
        <div class="brands-txt"><?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_NUBEWAY_TXT3']?></div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="http://www.nubeway.com/it/" target="blank" class="btn btn-bordered brands-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_BRANDS_BUT']?></a>
        </div>
    </div>
</div>

<div class="darker-divider"></div>

<div class="product-carousel">
  <img src="images/cat-brands/ubimaioritalia-other-brand-image2.jpg" alt="Accessories - slideshow">
  <img src="images/cat-brands/ubimaioritalia-other-brand-image1.jpg" alt="Accessories - slideshow">
  <img src="images/cat-brands/ubimaioritalia-other-brand-image3.jpg" alt="Accessories - slideshow">
</div>

