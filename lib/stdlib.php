<?php 
$env = getenv('HTTP_UBI_ENV');

/* WEBSITE LINK */
$urls = [ 
  'dev' => [
      'base_url' => 'http://ubimaior.test/admin',
      'public_url' => 'http://ubimaior.test'      
  ],
  'wip' => [
    'base_url' => 'http://ubimaioritalia.com/wip/admin',
    'public_url' => 'http://ubimaioritalia.com/wip'
  ],
  'staging' => [
    'base_url' => 'http://ubi.zululand.co.uk/admin',
    'public_url' => 'http://ubi.zululand.co.uk'
  ],
  'prod' => [
    'base_url' => 'http://ubimaioritalia.com/admin',
    'public_url' => 'http://ubimaioritalia.com'
  ]
];

$base_url = $urls[$env]['base_url'];
$public_url = $urls[$env]['public_url'];

function debug_array($array){
  echo "<pre>";
  print_r($array);
  echo "</pre>";
}


/* REDIRECT FUNCTION */
function redirect($url){
  header("HTTP/1.1 200 OK");
  header ("Location: ".$url);
}  
?>