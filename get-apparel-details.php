<?php error_reporting(1); include("./lib/dbconn.php"); include('./lib/stdlib.php'); include("./lib/language.php"); $id = $_REQUEST['id'];
$languages = $mysqli->query("SELECT id from languages WHERE slug='".$_COOKIE['ubi_lang']."'");
$language = $languages->fetch_object();

$query = "SELECT a.*, at.type as product_type, ad.content as description FROM apparel a LEFT JOIN apparel_type at ON at.id=a.id_type LEFT JOIN apparel_description ad ON a.id=ad.id_apparel WHERE ad.id_lang=".$language->id." AND a.id=".$_REQUEST['id'];

$prodotti = $mysqli->query($query);
if($prodotti->num_rows>0){
    $prodotto = $prodotti->fetch_object();

    ?>    
    <div class="row" style="clear:both;">
        <div class="col-sm-12 product" style="min-height:auto !important;width:auto !important;">
            <div class="col-sm-4 productImage text-center">
                <img src="../img/apparel/<?=$prodotto->cover?>" style="margin:0 auto;width:auto !important;max-width:100%;max-height:250px !important;" alt="Cover Prodotto Apparel - Ubi Maior Italia">
            </div>
            <div class="col-sm-8">
                <table>
                    <tr>
                        <td></td>
                        <td class="productName">
                            <?=$prodotto->codice?>
                            <?php if($prodotto->is_stock){ ?>
                                <span class="label label-success">IN STOCK</span>
                            <?php }else{ ?>
                                <span class="label label-default">NON IN STOCK</span>
                            <?php  } ?>
                        </td>
                    </tr>
                    <tr height="20"><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td></td>
                        <td>                            
                            <ul class="unstyled-list">                                                               
                                <li style="display:inline-block;margin-right:5px;"><?=($_COOKIE['ubi_lang']) === 'it' ? 'Taglie disponibili' : 'Available sizes'?>:</li>
                                <?php 
                                if($prodotto->taglia){
                                    $taglie = explode(',',$prodotto->taglia);
                                    foreach ($taglie as $taglia) {
                                        ?>
                                        <li style="display:inline-block;">
                                            <span class="label label-inverse"><?=$taglia?></span>
                                        </li>
                                        <?php
                                    }
                                }else{
                                    ?>
                                    <li style="display:inline-block;">
                                        <span class="label label-inverse"><?=($_COOKIE['ubi_lang']==='it') ? 'TAGLIA UNICA' : 'ONE SIZE'?></span>
                                    </li>
                                    <?php
                                }                                        
                                ?>
                            </ul>
                        </td>
                    </tr>
                    <tr height="20"><td colspan="2">&nbsp;</td></tr>
                    <tr>
                        <td></td>
                        <td><?=$prodotto->description?></td>
                    </tr>     
                    <tr height="20"><td colspan="2">&nbsp;</td></tr>                                    
                    <tr>
                        <td>
                            <i class="fa fa-euro"></i>
                        </td>
                        <td>
                            <strong><?=number_format((float)$prodotto->prezzo, 2, ',', '')?></strong>*                             
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2"><br>*<?=$lang[$_COOKIE[ubi_lang]][_CATALOGUE_IVA]?></td>
                    </tr>
                </table>
            </div>            
        </div>  
    </div>  
    <?php
}else{
    echo 'Prodotto non trovato';
}
