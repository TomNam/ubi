<div id="who-page" class="clearfix container-fluid">
    <div class="row">
        <div class="who-intro col-xs-12 clearfix">
            <div class="row">
                <div class="col-xs-12 col-sm-5 who-intro-left"></div>
                <div class="col-xs-12 col-sm-7 who-intro-right">
                    <h2 class="text-red">UBI MAIOR ITALIA&#174;</h2>
                    <h3 class="text-grey">SAIL BLOCKS – DECK HARDWARE – CUSTOM PROJECTS</h3> 
                    <h4 class="text-white">
                        <?=$lang[$_COOKIE['ubi_lang']]['_CHISIAMO_INTRO']?> 
                    </h4>
                </div>
            </div>
        </div>

        <div class="who-who col-xs-12 xs-m-t-20 m-t-50 m-b-50 clearfix">
            <div class="row">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-lg-6 col-lg-offset-5">
                    <h3 class="m-b-30 text-grey">DESIGN, PRODUCTION, APPLICATION </h3>
                    <p class="m-b-30 text-white">
                        <?=$lang[$_COOKIE['ubi_lang']]['_CHISIAMO_TEXT1']?>
                    </p>
                </div>
            </div>
        </div>

        <div class="who-quote col-xs-12 clearfix">
            <div class="row">
                <div class="col-sm-5 quote-text">
                    <h4 class="m-b-50 text-white">
                        <?=$lang[$_COOKIE['ubi_lang']]['_CHISIAMO_TEXT2']?>
                    </h4>
                </div>
                <ul class="col-sm-6 col-sm-offset-1 nine-steps">
                    <a class="fancybox" rel="group" href="images/chisiamo-1.jpg">
                        <li><img src="images/chisiamo-1.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-2.jpg">
                        <li><img src="images/chisiamo-2.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-3.jpg">
                        <li><img src="images/chisiamo-3.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-4.jpg">
                        <li><img src="images/chisiamo-4.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-5.jpg">
                        <li><img src="images/chisiamo-5.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-6.jpg">
                        <li><img src="images/chisiamo-6.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-7.jpg">
                        <li><img src="images/chisiamo-7.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-8.jpg">
                        <li><img src="images/chisiamo-8.jpg" width="100%" alt=""></li>
                    </a>
                    <a class="fancybox" rel="group" href="images/chisiamo-9.jpg">
                        <li><img src="images/chisiamo-9.jpg" width="100%" alt=""></li>
                    </a>   
                </ul>
            </div>
        </div>

        <div class="who-people col-xs-12 clearfix text-center">
            <div class="row who-content">
                <h3 class="m-b-30 text-grey"><?=$lang[$_COOKIE['ubi_lang']]['_CHISIAMO_TEAM']?></h3>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/gianni_dini.jpg" width="100%" >
                    <div class="people-text">
                        <h5>GIANNI DINI</h5>
                        <p>
                            CEO - Product Manager<br>
                            +39 0558364421<br><br>
                            <span class="text-red">gianni.dini@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/raffaele_dirusso.jpg" width="100%" >
                    <div class="people-text">
                        <h5>RAFFAELE DI RUSSO</h5>
                        <p>
                            Corporate Brand Manager <br>
                            +39 0558364421<br><br>
                            <span class="text-red">raffaele.dirusso@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/vieri_abolaffio.jpg" width="100%" >
                    <div class="people-text">
                        <h5>VIERI ABOLAFFIO</h5>
                        <p>
                            Project Manager <br>
                            +39 0558364421<br><br>
                            <span class="text-red">ufficiotecnico@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/fabio_desimoni.jpg" width="100%" >
                    <div class="people-text">
                        <h5>FABIO DE SIMONI</h5>
                        <p>
                            Sales Manager <br>
                            +39 340 7783482<br><br>
                            <span class="text-red">fabio.desimoni@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/nicola_stedile.jpg" width="100%" >
                    <div class="people-text">
                        <h5>NICOLA STEDILE</h5>
                        <p>
                            Sales Italy, Adriatic Area <br>
                            +39 366 719 6775<br><br>
                            <span class="text-red">nicola.stedile@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <!--<div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/francesco_gravina.jpg" width="100%" >
                    <div class="people-text">
                        <h5>FRANCESCO GRAVINA</h5>
                        <p>
                            Technical Office <br>
                            +39 0558364421<br><br>
                            <span class="text-red">ufficiotecnico.stedile@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>-->
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/patrizio_solli.jpg" width="100%" >
                    <div class="people-text">
                        <h5>PATRIZIO SOLLI</h5>
                        <p>
                            Delivery-Warehouse  <br>
                            +39 0558364421 <br> <br>
                            <span class="text-red">magazzino@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/silvia_sestini.jpg" width="100%" >
                    <div class="people-text">
                        <h5>SILVIA SESTINI</h5>
                        <p>
                            <?=$lang[$_COOKIE['ubi_lang']]['_CHISIAMO_ADMIN']?><br>
                            +39 0558364421 <br> <br>
                            <span class="text-red">amministrazione@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-4 col-lg-4 people">
                    <img src="images/cinzia_carotti.jpg" width="100%" >
                    <div class="people-text">
                        <h5>CINZIA CAROTTI</h5>
                        <p>
                            <?=$lang[$_COOKIE['ubi_lang']]['_CHISIAMO_ADMIN']?><br>
                            +39 0558364421 <br> <br>
                            <span class="text-red">amministrazione@ubimaioritalia.com</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="who-all col-xs-12 m-t-50 clearfix"></div>
    
    </div>
</div>
