<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">REGATA</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_RT_INTRO']?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSER']?> </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="110" class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CHEEKS']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_RT_CHEEKS_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_RT_SHEAVE_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_ROLLERS']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_RT_ROLLERS_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_BALLS']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_RT_BALLS_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHACKLE']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_RT_SHACKLE_DESC']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_OTHER']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_RT_OTHER_DESC']?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/category-rt.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-regata">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_RT_IMG_TXT']?>
        </p>            
        <a class="btn btn-lrg btn-bordered rt-but" href="index.php?p=catalog&idl=3">
            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>            

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text-->
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_RT_IMG_TXT']?>
    </p>            
    <a class="btn btn-lrg btn-bordered rt-but" href="index.php?p=catalog&idl=3">
        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / mobile category-intro-text-->
<!---------------------- category options ---------------------->
<div class="container-fluid p-b-50"> 
    <div class="col-sm-4 p-l-40 m-t-40 bordered-right text-center">
        <h2 class="text-white m-t-40">RT REGATA</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=$lang[$_COOKIE['ubi_lang']]['_RT_TECH1']?>
        </p>
        <img src="images/cat-regata/rt-regata.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?> <span class="bold">60-150 mm</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?> <span class="bold"> 12-25 mm</span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?> <span class="bold">1400-6800 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?>  <span class="bold">2800-13600 Kg</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=3" class="btn btn-bordered rt-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

    <div class="col-sm-4 p-l-40 m-t-40 bordered-right text-center">
        <h2 class="text-white m-t-40">RT ULTRA</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=$lang[$_COOKIE['ubi_lang']]['_RT_TECH2']?>
        </p>
        <img src="images/cat-regata/rt-ultra.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?> <span class="bold">55-120 mm</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?> <span class="bold"> 14-28 mm</span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?> <span class="bold">1800-5000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?>  <span class="bold">3600-10000 Kg</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=12" class="btn btn-bordered rt-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

    <div class="col-sm-4 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-40">RT EVE</h2>
        <p class="fs-15 text-white m-t-20 lines-tags">
            <?=$lang[$_COOKIE['ubi_lang']]['_RT_TECH3']?>
        </p>
        <img src="images/cat-regata/rt-eve.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?> <span class="bold">55-150 mm</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?> <span class="bold"> 12-25 mm</span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?> <span class="bold">3000-13000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?>  <span class="bold">6000-26000 Kg</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=13" class="btn btn-bordered rt-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

</div><!---------------------- / category options ---------------------->

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal"><!-- regata drawing -->
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/regata-photo-drawing.jpg">
                    <img src="images/cat-regata/regata-photo-drawing.jpg" alt="Regata" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/regata-drawing.jpg">
                    <img src="images/cat-regata/regata-drawing.png" alt="Regata" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal"><!-- ultra drawing -->
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/ultra-drawing.jpg">
                    <img src="images/cat-regata/ultra-drawing.png" alt="Regata" class="img-responsive"> 
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/ultra-photo-drawing.jpg">
                    <img src="images/cat-regata/ultra-photo-drawing.jpg" alt="Regata" class="img-responsive">
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal p-t-50"><!-- eve drawing -->
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/eve-photo-drawing.jpg">
                    <img src="images/cat-regata/eve-photo-drawing.jpg" alt="Jiber-TX" class="img-responsive">
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/eve-drawing.jpg">
                    <img src="images/cat-regata/eve-drawing.png" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="spacer10"></div>

        <div class="row equal p-t-50"><!-- xbearing drawing -->
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-regata/xbearing-drawing.jpg">
                    <img src="images/cat-regata/xbearing-drawing.png" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
            <div class="col-xs-12 col-sm-6 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-regata/xbearing-photo-drawing.jpg">
                    <img src="images/cat-regata/xbearing-photo-drawing.jpg" alt="Jiber-TX" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="rt-esploso"></div>
</div>

<div class="product-carousel">
  <img src="images/cat-regata/rt-slider1.jpg" alt="Regata - slideshow">
  <img src="images/cat-regata/rt-slider2.jpg" alt="Regata - slideshow">
  <img src="images/cat-regata/rt-slider3.jpg" alt="Regata - slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-regata/regata-footer.png" alt="Ubi Maior Italia - Regata Line" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=12" target="_blank" class="btn btn-bordered rt-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_RT_CATALOGUE']?></a>
  </div>
</div>
</div>


