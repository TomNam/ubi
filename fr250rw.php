<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">FR250RWm</h1>
            <h5 class="text-white hint-text">
            FR AVVOLGITORI are extremely reliable and functional for managing bow sails, both code zero and gennaker. FR250RWm model is last born in UBIMAIOR factory.
      </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i>Look closer </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td class="all-caps text-white">DRUM AND HEAD</td>
                        <td>ALUMINIUM alloy 6082T6, machined from a single piece and anodized</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">CARTER</td>
                        <td>Machined from a single piece of BLACK DELRIN, EASY LOCK system, for the easy installation of the loop</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">BEARINGS</td>
                        <td>Cylindrical roller thrust bearings, X-Bearing sistem by UBI MAI1R ITALIA&#174; on free tack</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">OTHER</td>
                        <td>AISI 316 and 17-4Ph STEEL hub and screws. Hub adjustment and locking by socket set in 12 positions</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/fr250rwm/fr250rw-first.png" alt="FR250RWm" class="image-responsive-height">
        </div>
    </div>
</div>
<div class="bg-white clearfix fr250rw-ft features-img-right">
    <div class="container-fluid clearfix  p-b-50">
        <div class="row equal p-t-50">
            <div class="col-md-5 col-sm-8 text-center">
                <h2 class="padding-20">
                  Features
                </h2>
                <p class="fs-15 m-b-50">
                    The drum and head are machined from a single piece of aluminum 6082T6 and contain cylindrical roller thrust bearings featuring the best characteristics for this use. Indeed, they move exceptionally well (which is fundamental for the swivel) and boast optimal load distribution.
                </p>
                <img src="images/fr250rwm/fr250rw-tech.png" alt="FR250RWm" class="img-responsive">
                <p class="fs-15 m-t-50">
                    The drum flue geometry guarantees a perfect grip when winding, while also allowing for easy desingagement when opened so that the sail can be opened from the cockpit simply by activating the sheet line. This big model of FR Avvolgitori has the famous lock ReWind and the X-Bearing system on free tack.
                </p>
                <a href="#" class="btn btn-gray btn-bordered m-t-30 all-caps">Technical Specifications</a>

            </div>
            <div class="col-sm-6 clearfix"></div>
        </div>
    </div>
</div>
<div class="container-fluid p-t-50 p-b-50">
    <div class="col-sm-6 text-center bordered-right">
        <img src="images/fr250rwm/fr250rw-tech2.png" alt="fr250rwm technical details" class="img-responsive">
    </div>
    <div class="col-sm-6 p-l-40">
        <h2 class="text-white m-t-40">
          Technical details
        </h2>
        <p class="fs-15 text-white m-t-20">
            The measures indicated are the minimum and maximum range. <br> For the single product's details see the catalog
        </p>
        <table id="table-tech" class="m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-radius.png" /></td>
                <td>
                    &#8960; ROLLER DRUM <span class="bold">250mm </span> <br>
                    FURLING LINE <span class="bold">10mm </span> <br>
                    &#8960; PIN <span class="bold">16mm </span> 
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                  MAX WORKING LOAD <span class="bold">10000 Kg</span> <br>
                  BREAKING LOAD  <span class="bold">20000 Kg</span> <br>
                  WEIGHTHEAD <span class="bold">900 gr</span> <br>
                  WEIGHT DRUM <span class="bold">3550 gr</span>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="product-carousel">
  <img src="images/fr250rwm/fr250rw_gal_1.jpg" alt="FR250RWm slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-8 col-xs-push-2">
    <img src="images/fr250rwm/fr250rw-footer.png" alt="" class="img-responsive">
    <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=36" target="_blank" class="btn btn-bordered m-t-50">SEE ALL OTHER FR'S PRODUCTS</a>
  </div>
</div>
</div>
