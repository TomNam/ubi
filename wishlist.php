
<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
	<div class="row">
		<div class="col-sm-12 ">
			<h4 class="text-white pull-right wish-to-close" id="closeLeftPush">
				<?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSE']?> <i class="fa fa-times text-red"></i>
			</h4>
		</div>
	</div>
	<div class="row wish--20">
		<div class="col-sm-4 col-md-3">
			<h2 class="text-white text-left bold"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH']?></h2>
		</div>
		<div class="col-sm-8 col-md-9">
			<input class="whishlist-search-trigger" placeholder="<?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH_PLACEHOLDER']?>" type="text">
		</div>
	</div>
	<div class="row m-t-50">
		<h4 style="margin-left:10px;" class="text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH_RES']?>: <span class="whishlist-search-keyword"></span></h4>
		<hr>
	</div>
	
	<?php include('./wishlist/search.php');?>	

</nav>
<div class="wish-container">
	<div class="wish-content">
		<div class="row">
			<div class="col-sm-10 wish-u-were-right">
				<div class="row">
					<div class="col-xs-12 col-md-2 search-left">
						<p><?=$lang[$_COOKIE['ubi_lang']]['_WISH_MORE_ITEMS']?></p>
						<button id="showLeftPush" class="jib-bg">
							<span class="fa fa-search-plus"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH']?>
						</button>
					</div><!--/ search-left-->
					<div class="col-xs-12 col-md-10 wish-u-were-right-cont">
						<div class="spacer10 hidden-xs"></div>
						<div class="alert alert-info" role="alert">
							<span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span>
							<span class="sr-only">Attention:</span>
							<strong><?=$lang[$_COOKIE['ubi_lang']]['_WISH_ATTENTION']?></strong><br />
							<?=$lang[$_COOKIE['ubi_lang']]['_WISH_ATTENTION_TXT']?></strong>
						</div>
						<div class="row table-wish table-top-line">
							<div class="col-xs-2 col-sm-1 title"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_IMAGE']?></strong></div>
							<div class="col-xs-2 col-sm-2 title"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CODE']?></strong></div>
							<div class="col-sm-3 title hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_DESC']?></strong> </div>
							<div class="col-xs-4 col-sm-2 title"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRICE']?></div>
							<div class="col-xs-2 col-sm-1 title">QTY</div>
							<div class="col-xs-2 col-sm-4 title"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_OPTIONS']?></div>
						</div>
						<div class="whislist-container"></div>
						<div class="col-xs-12 padding-20 order-form" style="display:none;">
							<h4><?=$lang[$_COOKIE['ubi_lang']]['_WISH_FILL_FORM']?></h4>
							<hr>

							<div class="alert alert-danger alert-error-form" role="alert" style="display:none;">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_ERROR']?>:</span>
								<span class="error-text"></span>
							</div>
							<div class="alert alert-success alert-success-form" role="alert" style="display:none;">
								<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
								<span class="sr-only"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SUCCESS']?>:</span>
								<span class="success-text"></span>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="text-red" for=""><?=$lang[$_COOKIE['ubi_lang']]['_ALL_NAME']?></label>
										<input type="text" name="name" class="order-name form-control" placeholder="insert your name here" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="text-red" for=""><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SURNAME']?></label>
										<input type="text" name="surname" class="order-surname form-control" placeholder="insert your surname here" required>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label class="text-red" for="">Email</label>
										<input type="email" name="email" class="order-email form-control" placeholder="insert your email here" required>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label class="text-red" for=""><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PHONE']?></label>
										<input type="text" name="phone" class="order-phone form-control" placeholder="insert your phone number here" required>
									</div>
								</div>
							</div>
							<div class="text-center m-t-20">
								<button class="btn btn-bordered jib-bg submit-order" style="cursor:pointer;"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEND']?> <i class="fa fa-spin fa-cog spinner-order" style="display:none;"></i></button>
							</div>
						</div><!--/ order-form-->
						<div class="col-xs-12 total-bar" style="display:none;">
							<div class="row whishlist-total-bg padding-30">								
								<div class="total-amount col-xs-12 col-sm-8"><h4 class="text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TOTAL']?> <span class="whishlish-total-amount m-l-20"></span></h4></div>
								<div class="total-place-ord col-xs-12 col-sm-4"><button class="text-white place-order" style="width:170px;padding:10px 5px;border:1px solid #fff;background:none;margin:0 auto;font-size:18px;"><i class="fa fa-paper-plane"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEND']?></button></div>
							</div>							
						</div>
					</div><!--/ wish-u-were-right-cont-->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="active-lang" data-lang="<?=$_COOKIE['ubi_lang']?>"></div>
