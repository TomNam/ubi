<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">SAILMAKERS</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_SAIL_INTRO']?>
            </h5>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/category-sailmakers.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-sailmakers">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_SAIL_IMG_TXT']?>
        </p>            
        <!--<a class="btn btn-lrg btn-bordered sail-but" href="index.php?p=catalog&idl=4">
            <i class="m-r-10 fa fa-cog"></i> Products
        </a>-->            

    </div><!-- / category-intro-text-->
</div>
<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text -->
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_SAIL_IMG_TXT']?>
    </p>            
    <a class="btn btn-lrg btn-bordered sail-but" href="index.php?p=catalog&idl=4">
        <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / mobile category-intro-text-->

<div class="container-fluid">
    <div class="sailmakers-full"></div>
</div>

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-sailmakers/sailmakers-drawing1.jpg">
                    <img src="images/cat-sailmakers/sailmakers-drawing1.jpg" alt="Ubimaior Italia - Sailmakers accessories" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-sailmakers/sailmakers-photo-drawing3.jpg">
                    <img src="images/cat-sailmakers/sailmakers-photo-drawing3.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
    
    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-sailmakers/sailmakers-photo-drawing4.jpg">
                    <img src="images/cat-sailmakers/sailmakers-photo-drawing4.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive"> 
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-sailmakers/sailmakers-drawing2.jpg">
                    <img src="images/cat-sailmakers/sailmakers-drawing2.jpg" alt="Ubimaior Italia - Sailmakers accessories" class="img-responsive"> 
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid clearfix">
        <div class="row equal">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-sailmakers/sailmakers-photo-drawing2.jpg">
                    <img src="images/cat-sailmakers/sailmakers-photo-drawing2.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive"> 
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-sailmakers/sailmakers-photo-drawing1.jpg">
                    <img src="images/cat-sailmakers/sailmakers-photo-drawing1.jpg" alt="Ubimaior Italia - Yatch Club" class="img-responsive">
                </a>
            </div>
        </div>
    </div>
</div>

<div class="product-carousel">
  <img src="images/cat-sailmakers/sailmakers-gallery-1.jpg" alt="Yacht Club - slideshow">
  <img src="images/cat-sailmakers/sailmakers-gallery-2.jpg" alt="Yacht Club - slideshow">
  <img src="images/cat-sailmakers/sailmakers-gallery-3.jpg" alt="Yacht Club - slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-sailmakers/sailmakers-footer.png" alt="Ubi Maior Italia - Regata Line" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=58" target="_blank" class="btn btn-bordered sail-but m-t-50"> <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_SAIL_CATALOGUE']?></a>
  </div>
</div>
</div>


