<div class="overlay p-t-50" id="searchOverlay" style="overflow: hidden">
    <div class="container search-cont-sml text-white m-t-50">
        <div class="row">
          <div class="col-sm-12 ">
            <h4 class="text-white pull-right" id="closeSearch" style="cursor:pointer;"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSE']?> <i class="fa fa-times text-red"></i></h4>
          </div>
        </div>
        <div class="row">
            <div class="col-xs-3 col-sm-2">
                <h2 class="search-title-sml text-white text-right bold"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH']?></h2>
            </div>
            <div class="col-xs-9 col-sm-10">
                <input class="inputSearch" type="search" placeholder="<?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH_PLACEHOLDER']?>" autofocus>
            </div>
        </div>
        <div class="row m-t-50 research-for-box">
            <h3 class="research-for text-white"> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEARCH_RES']?>: <span class="searchResults"></span></h3>
            <hr>
        </div>
        <div class="row" style="max-height: 65vh;overflow-y: auto;">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li role="presentation" class="active"><a href="#search-catalogue" aria-controls="search-catalogue" role="tab" data-toggle="tab">Catalogo</a></li>
                <li role="presentation"><a href="#search-apparel" aria-controls="search-apparel" role="tab" data-toggle="tab">Apparel</a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="search-catalogue">
                    <?php include('./blocks/search/catalogue.php'); ?>
                </div>
                <div role="tabpanel" class="tab-pane" id="search-apparel">
                    <?php include('./blocks/search/apparel.php'); ?>
                </div>    
            </div>
        
        </div>
    </div>
</div>
