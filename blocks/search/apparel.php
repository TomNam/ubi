<table class="table">
    <thead>
        <tr>
            <th class="text-center search-img-block"> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_IMAGE']?></th>
            <th class="text-center search-code-block"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CODE']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SIZES']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRICE']?></th>
        </tr>
    </thead>
    <tbody class="prodotti-apparel-results scrollbar">

    </tbody>
</table>