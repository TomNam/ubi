<table class="table">
    <thead>
        <tr>
            <th class="text-center search-img-block"> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_IMAGE']?></th>
            <th class="text-center search-code-block"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CODE']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TYPE']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_WEIGHT']?></th>
            <th class="text-center hidden-xs"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?></th>
        </tr>
    </thead>
    <tbody class="prodotti-catalogue-results scrollbar">

    </tbody>
</table>