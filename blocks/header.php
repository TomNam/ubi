<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UBI MAIOR Italia | Sail Blocks – Deck Hardware – Custom Projects</title>

    <link rel="icon" type="image/png" href="/images/favicon.png">

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome Css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Slick Css -->
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/slick-theme.css">
    <!-- Toast -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css">
    <!-- General Css -->
    <link rel="stylesheet" href="css/general.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/home.css">
    <link rel="stylesheet" href="css/product-page.css">
    <link rel="stylesheet" href="/css/jquery.instastream.css">

    <?php if(!$_REQUEST["p"]): ?>
    <link rel="stylesheet" href="css/index.css">
    <!-- Resource style -->
    <?php endif; ?>

    <?php if($_REQUEST["p"]=="product-highlight"): ?>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.7/slick.css" />
    <?php endif; ?>

    <?php if($_REQUEST["p"]=="category-intro-jiber"||"categories"): ?>
    <link rel="stylesheet" type="text/css" href="css/categories.css" />
    <?php endif; ?>

    <?php if($_REQUEST["p"]=="wishlist"): ?>
    <link rel="stylesheet" type="text/css" href="css/default.css" />
	<link rel="stylesheet" type="text/css" href="css/wishlist.css" />
    <?php endif; ?>


    <link rel="stylesheet" href="fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

    <link href='https://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/jquery.cookie.js"></script>   
    <script type="text/javascript" src="js/language.js"></script>   

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
    ga('create', 'UA-88158703-1', 'auto');
    ga('send', 'pageview');
    
    </script>
</head>

<body class="cbp-spmenu-push scrollbar">
    <!-- The overlay -->
    <div id="nav" class="overlay">
        <div class="overlay-inner-cont">
            <!-- Overlay content -->
            <div class="overlay-content row">
                <div class="col-xs-5 col-sm-6 col-md-3 first-menu-col">
                    <div class="row">
                        <ul class="menu-col col-xs-12">
                            <li class="menu-col-tit"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_PAGES_TITLE']?></li>
                            <li><a href="index.php">Home</a></li>
                            <li><a href="index.php?p=chisiamo"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_ABOUT']?></a></li>
                            <li><a href="index.php?p=categories"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_FULL_RANGE']?></a></li>
                            <li><a href="index.php?p=catalog&idl=4"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_ONLINE_CATALOGUE']?></a></li>
                            <li><a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf" target="_blank"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_PDF_CATALOGUE']?></a></li>
                            <li><a href="index.php?p=reseller"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_DEALERS']?></a></li>
                            <li><a href="index.php?p=wishlist">Wishlist</a></li>
                            <li><a href="index.php?p=contact"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_CONTACT']?></a></li>
                            <?php if($_COOKIE["ubi_lang"]=='it'): ?>
                                <li><a class="set-lang" data-lang="en" href="javascript:void(0)"><img src="images/en-flag.png" alt="english flag" width="25" /></a></li>
                            <?php else: ?>
                                <li><a class="set-lang" data-lang="it" href="javascript:void(0)"><img src="images/it-flag.png" alt="italian flag" width="25" /></a></li>
                            <?php endif; ?>  
                            <li><a href="pdf/WLG-<?=$_COOKIE['ubi_lang']?>.pdf" target="_blank"><span class="fa fa-file-pdf-o" style="font-size:13px;"></span> <i style="font-size:15px;"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_WARRANTY']?></i></a></li>                            
                        </ul>

                        <!-- Other menu
                            <ul class="menu-col col-xs-12 col-sm-5 col-lg-5">
                            <li class="menu-col-tit">TOP LINES</li>
                            <li><a href="index.php?p=x3m">X3M Flight</a></li>
                            <li><a href="index.php?p=jiber-tx">Jiber-TX</a></li>
                            <li><a href="index.php?p=fr250rw">FR250RWm</a></li>
                        </ul>-->
                    </div>
                </div>
                <ul class="col-xs-6 col-sm-4 col-md-4 menu-col">
                    <li class="menu-col-tit"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_PRODUCTS_TITLE']?></li>
                    <li><a href="index.php?p=category-intro-yc">YC Yatch Club</a></li>
                    <li><a href="index.php?p=category-intro-regata">RT Regata</a></li>
                    <li><a href="index.php?p=category-intro-x3m">X3M Flight</a></li>
                    <li><a href="index.php?p=category-intro-jiber">JB Jiber</a></li>
                    <li><a href="index.php?p=category-intro-fr">FR Avvolgitori</a></li>
                    <li><a href="index.php?p=category-intro-accessories"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_ACCESSORIES']?></a></li>
                    <li><a href="index.php?p=category-intro-sailmakers">Sailmakers</a></li>
                    <li><a href="index.php?p=category-intro-custom">Custom</a></li>
                    <li><a href="index.php?p=apparel">Apparel</a></li>
                    <li><a href="index.php?p=other-brands"><?=$lang[$_COOKIE['ubi_lang']]['_MENU_OTHER_BRANDS']?></a></li>
                </ul>
            </div>
        </div>
    </div>

    <!-- Use any element to open/show the overlay navigation menu -->
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a href="index.php"><img src="images/logo_ubi_white.png" alt="Ubi Major" class="logo"></a>
            </div>
            <!-- <div id="navbar" class="navbar-collapse collapse"> -->

            <ul class="nav navbar-nav navbar-right">
                <li class="search">
                    <i class="fa fa-search"></i>
                </li>
                <li class="top-wish">
                    <a href="index.php?p=wishlist">
                        <i class="fa fa-shopping-cart"></i>
                    </a>
                </li>
                <li class="menu-icon">
                    <div class="navIcon">
                        <span></span>
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </li>
            </ul>
            <!-- </div> -->
        </div>
    </nav>
    <div class="container ">
        <div class="row" style="position:fixed">
            <div class="col-xs-9 col-md-11">

            </div>


        </div>
    </div>

<?php include('blocks/search.php') ?>
