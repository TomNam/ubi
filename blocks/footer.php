    <div id="footer">
        <div class="container-fluid newsletter-container p-t-50 p-b-50 text-center">
            <div class="row">
                <div class="col-xs col-sm-12 text-center">
                    <a href="http://eepurl.com/cBk9df" target="_blank">
                        <h3><i class="fa fa-envelope"></i> <?=$lang[$_COOKIE['ubi_lang']]['_FOOTER_NEWSLETTER']?></h3>
                    </a>
                </div>
            </div>
        </div>
        <div class="container p-t-50 p-b-50 text-center">
            <div class="row">
                <div class="col-sm-4 col-sm-push-4 ">
                    <img src="images/ubi-footer.png" class="img-responsive" alt="">
                </div>
            </div>
            <div class="row regione-toscana">
                <a class="fancybox" href="images/regione-toscana-big.jpg">
                    <ul>
                        <li><img src="images/regione-toscana-POR-CreO14_20.png" width="100" style="margin:50px 0 0;" alt="regione toscana fondo"></li>
                        <li><p>Progetto co-finanziato dal <br> POR FESR Toscana 2014-2020</p></li>
                    </ul>
                </a>
            </div>
            <div class="row">
                <div class="col-sm-12 m-t-50">
                    <a href="https://www.google.it/maps/place/UBIMAIOR+ITALIA/@43.8210758,11.3903583,17z/data=!3m1!4b1!4m5!3m4!1s0x132baa7cc10779d1:0x9e7a47a1c54439e7!8m2!3d43.821072!4d11.392547" target="_blank">UBI MAIOR ITALIA <sup>&reg;</sup> by DINI s.r.l., Meccanica di precisione <br> via di Serravalle 35 / 37 / 39 - 50065 - Molino del Piano - Pontassieve - Firenze &nbsp; - &nbsp;<span class="fa fa-map-o"></span></a><br> Tel. 055 8364421 - Fax 055 8364614 - info@ubimaioritalia.com
                </div>
            </div>
        </div>
    </div>
    <input type="hidden" value="<?=$_GET[" p "]?>" />

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
    <script type="text/javascript" src="js/underscore.js"></script>

    <?php if($_REQUEST["p"]=="wishlist"): ?>
        <script src="js/modernizr.custom.js"></script>
        <script src="js/classie.js"></script>
        <script src="js/wishlist.js"></script>
        <script src="js/jquery.instastream.js"></script>
    <?php elseif($_REQUEST["p"]=='category-intro-accessories'): ?>
    <script>
        function scrollNav() {
    $('.nav a').click(function(){
        //Toggle Class
        $(".active").removeClass("active");
        $(this).closest('li').addClass("active");
        var theClass = $(this).attr("class");
        $('.'+theClass).parent('li').addClass('active');
        //Animate
        $('html, body').stop().animate({
            scrollTop: $( $(this).attr('href') ).offset().top - 160
        }, 400);
        return false;
    });
    $('.scrollTop a').scrollTop();
    }
    scrollNav();
    </script>
    <?php elseif($_REQUEST["p"]=='catalog'): ?>
        <script type="text/javascript" src="js/catalog.js"></script>
    <?php elseif($_REQUEST["p"]=='apparel'): ?>
        <script type="text/javascript" src="js/apparel.js"></script>
    <?php endif; ?>


    <script src="js/slick.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/search.js"></script>
</body>
</html>
