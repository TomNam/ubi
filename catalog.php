<?php include("./lib/dbconn.php");

if(!$_REQUEST['idl'] && !$_REQUEST["codiceProdotto"]){
    $class_siderbar = '';
    $class_content = 'col-md-12';
}else{
    $class_siderbar = 'col-md-3';
    $class_content = 'col-md-9';
}

?>
<div class="container-fluid p-t-80">
    <div class="row">

        <?php if($class_siderbar!==''): ?>
            <div class="col-sm-12 col-md-3 category-left">
                <?php include('catalog/sidebar.php'); ?>
            </div><!-- / category left -->
        <?php endif; ?>

        <div class="col-sm-12 <?=$class_content?> products-container">
          <!-- Quick links to other lines -->
          <div class="row text-center">
            <div class="dropup-cont">
                <div class="dropup quick-links-catalog-mobile">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        Other lines
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        <li><a href="index.php?p=catalog&idl=4">Yatch Club</a></li>
                        <li><a href="index.php?p=catalog&idl=3">Regata</a></li>
                        <li><a href="index.php?p=catalog&idl=2">X3M</a></li>
                        <li><a href="index.php?p=catalog&idl=8"><li>Jiber</a></li>
                        <li><a href="index.php?p=catalog&idl=1"><?=($_COOKIE['ubi_lang']=='it') ? 'Avvolgitori' : 'Furler'?></a></li>
                        <li><a href="index.php?p=catalog&idl=5">Accessories</a></li>
                        <li><a href="index.php?p=apparel">Apparel</a></li>
                        <!-- <li><a href="index.php?p=category-intro-sailmakers">Sailmakers</a></li>
                        <li><a href="index.php?p=category-intro-custom">Custom</a></li> -->
                    </ul>
                </div>
            </div>

            <div class="text-center">
                <ul class="quick-links-catalog">
                    <a href="index.php?p=catalog&idl=4"><li>Yatch Club</li></a>
                    <a href="index.php?p=catalog&idl=3"><li>Regata</li></a>
                    <a href="index.php?p=catalog&idl=2"><li>X3M</li></a>
                    <a href="index.php?p=catalog&idl=8"><li>Jiber</li></a>
                    <a href="index.php?p=catalog&idl=1"><li><?=($_COOKIE['ubi_lang']=='it') ? 'Avvolgitori' : 'Furler'?></li></a>
                    <a href="index.php?p=catalog&idl=5"><li>Accessories</li></a>
                    <a href="index.php?p=apparel"><li>Apparel</li></a>
                    <!-- <a href="index.php?p=category-intro-sailmakers"><li>Sailmakers</li></a>
                    <a href="index.php?p=category-intro-custom"><li>Custom</li></a> -->
                </ul>
            </div>
          </div>
          <!-- / Quick links to other lines -->

          <div class="row" style="clear:both;">
            <?php
                $productCode = $_REQUEST["codiceProdotto"];

                if($_REQUEST['idl']){
                    $query = "SELECT c.*, l.Sigla as codice_linea, t.Sigla as codice_tipologia FROM catalogo c LEFT JOIN linee l ON l.id=c.IdLInea LEFT JOIN tipologie t ON t.Id=c.IdTipologia LEFT JOIN nomi_voci n ON n.IdTipologia = c.IdTipologia AND n.lingua = 3 WHERE c.IdLinea = '".$_REQUEST['idl']."'";
                    $prodotti = $mysqli->query($query);
                    if($prodotti->num_rows>0){
                        include('catalog/filter_bar.php');
                    }
                }else{
                    ?><div class="col-xs-12"><h4 class="text-center">Product Details</h4></div><?php
                }

                if($productCode){
                    include('catalog/details.php');
                }else if($_REQUEST['idl']){
                    include('catalog/grid.php');
                }else{
                    ?>
                    <div class="row m-t-50" style="min-height:400px;">
                        <div class="col-sm-6 col-sm-offset-3">
                            <h2>OPS! Something went wrong here</h2>
                            <h4>You should be watching at a product details or a Line's product list. Use our Search page and find what you are looking for!</h4>
                            <button class="btn btn-bordered jib-bg search">Search</button>
                        </div>
                    </div>
                    <?php
                }
            ?>
        </div>
    </div>
</div>
</div>
