<div id="category-section category-page" class="m-t-50 row clearfix">
    <!---------------- SEZIONE CATEGORIE ------------------>
    <div id="category-section" class="m-t-50  clearfix">
        <div class="product-box col-md-6 padding-40 clearfix">
            <div class="col-sm-4 ">
                <img src="images/category-yc.png" class="img-responsive m-t-50" alt="UbiMaior Italia - Yacht Club Cruisers">
            </div>
            <div class="col-sm-8 text-right text-white">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="text-white m-b-30">YC Yacht Club</h2>
                <p class="fs-15 m-b-30">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_YC']?>
                </p>
                <a href="index.php?p=category-intro-yc" class="btn btn-bordered btn-white yc-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="index.php?p=catalog&idl=4" class="btn btn-bordered yc-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                </a>
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
            <div class="col-sm-4 ">
                <img src="images/category-rt-light.png" class="img-responsive m-t-50" alt="UbiMaior Italia - Rt regata blocks">
            </div>
            <div class="col-sm-8 text-right">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30">RT Regata</h2>
                <p class="fs-15 m-b-30">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_RT']?>
                </p>
                <a href="index.php?p=category-intro-regata" class="btn btn-bordered rt-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="index.php?p=catalog&idl=3" class="btn btn-bordered rt-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                </a>
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
            <div class="col-sm-8">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30">X3M Flight </h2>
                <p class="fs-15 m-b-30">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_X3M']?>
                </p>
                <a href="index.php?p=category-intro-x3m" class="btn btn-bordered x3m-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="index.php?p=catalog&idl=2" class="btn btn-bordered x3m-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                </a>
            </div>
            <div class="col-sm-4 ">
                <img src="images/category-x3m-light.png" class="img-responsive m-t-50" alt="UbiMaior Italia - Jiber - Genoa Furler">
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 clearfix">
            <div class="col-sm-8">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30 text-white">JB Jiber</h2>
                <p class="fs-15 m-b-30 text-white">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_JB']?>
                </p>
                <a href="index.php?p=category-intro-jiber" class="btn btn-bordered jib-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="index.php?p=catalog&idl=8" class="btn btn-bordered jib-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                </a>
            </div>
            <div class="col-sm-4 ">
                <img src="images/category-jiber.png" class="img-responsive m-t-50" alt="UbiMaior Italia - Jiber - Genoa Furler">
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 clearfix">
            <div class="col-sm-4 ">
                <img src="images/category-fr.png" class="img-responsive m-t-50" alt="UbiMaior Italia - bow sails">
            </div>
            <div class="col-sm-8 text-right">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30 text-white">FR Avvolgitori</h2>
                <p class="fs-15 m-b-30 text-white">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_FR']?>
                </p>
                <a href="index.php?p=category-intro-fr" class="btn btn-bordered fr-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="index.php?p=catalog&idl=1" class="btn btn-bordered fr-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                </a>
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
            <div class="col-sm-4 ">
                <img src="images/category-accessories-light.png" class="img-responsive m-t-50" alt="UbiMaior Italia - rigging equipment and accessories">
            </div>
            <div class="col-sm-8 text-right">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_ACCESSORIES']?></h2>
                <p class="fs-15 m-b-30">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_ACC']?>
                </p>
                <a href="index.php?p=category-intro-accessories" class="btn btn-bordered acc-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="index.php?p=catalog&idl=5" class="btn btn-bordered acc-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
                </a>
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 bg-light-gray clearfix">
            <div class="col-sm-4 ">
                <img src="images/category-sailmakers-light.png" class="img-responsive m-t-50" alt="UbiMaior Italia - technical accessories for sailmakers">
            </div>
            <div class="col-sm-8 text-right">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30">Sailmakers</h2>
                <p class="fs-15 m-b-30">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_SAIL']?>
                </p>
                <a href="index.php?p=category-intro-sailmakers" class="btn btn-bordered sail-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=58" class="btn btn-bordered sail-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_CATALOGUE']?>
                </a>
            </div>
        </div>

        <div class="product-box col-md-6 padding-40 clearfix">
            <div class="col-sm-4 ">
                <img src="images/category-custom.png" class="img-responsive m-t-50" alt="UbiMaior Italia - Custom sail projects">
            </div>
            <div class="col-sm-8 text-right">
                <!--<h6 class="all-caps bold muted m-b-20 text-white">LOREM IPSUM</h6>-->
                <h2 class="m-b-30 text-white">Custom</h2>
                <p class="fs-15 m-b-30 text-white">
                    <?=$lang[$_COOKIE['ubi_lang']]['_HOME_CUST']?>
                </p>
                <a href="index.php?p=category-intro-custom" class="btn btn-bordered cust-but"><i class="fa fa-plus m-r-10"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LEARNMORE']?></a>
                <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=62" class="btn btn-bordered cust-but">
                    <i class=" m-r-20 fa fa-anchor"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_CATALOGUE']?>
                </a>
            </div>
        </div>
        <div style="clear:both"></div>
    </div>

</div>
