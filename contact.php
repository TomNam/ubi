<div id="contact" class="clearfix container-fluid">
    <div class="row">
        <div class="who-intro col-xs-12 clearfix">
            <div class="row">
                <div class="col-xs-12 col-sm-7 who-contact-left">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2878.676178420288!2d11.390358315337004!3d43.82107584957712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132baa7cc10779d1%3A0x9e7a47a1c54439e7!2sUBIMAIOR+ITALIA!5e0!3m2!1sit!2sit!4v1501759696075" width="100%" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
                <div class="col-xs-12 col-sm-5 who-contact-right">
                    <h2 class="text-red">UBI MAIOR ITALIA&#174;</h2>
                    <h5 class="text-white where">
                        Via di Serravalle 35/37/39 - 50065 - <br> 
                        Molino del Piano - Pontassieve - Firenze <br>
                        Tel. 055 8364421 - Fax 055 8364614 <br><br>
                        <a href="mailto:info@ubimaioritalia.com">info@ubimaioritalia.com</a>
                    </h5>
                </div>
            </div>
        </div>
    </div>    
</div>
