<?php
function generate_qf($linea,$size,$load,$type){

  $filters = "WHERE c.IdLinea=".$linea."";

  switch ($linea) {

    case '1':
      if($type && $type!='ALL'){
            if($type=='ACCESSORIES'){
                $filters.= " AND c.Val06=''";
            }else{
                $filters.= " AND c.Val06='".strtolower($type)."'";
            }
        }

        if($size && $size!='ALL'){
            if($size=='200' || $size=='250' || $size=='250H'){
                if($type=='ACCESSORIES'){
                    $filters.= " AND (c.Val03='".$size."' || c.Val04='".$size."')";
                }else{
                    $filters.= " AND c.Val02='".$size."'";
                }
            }else{
                if($type=='ACCESSORIES'){

                  $filters.= " AND (c.Val03='".$size."' || c.Val04 LIKE '%".$size."%')";
                }else{
                    $filters.= " AND c.Val02='".$size."'";
                }
            }
        }
      break;

    case '2':
        if($size && $size!='ALL'){
            $filters.= " AND c.Val01=".$size;
        }
        break;

    case '3':
        if($load && $load!='ALL'){
            $filters.= " AND (c.Val05='".$load."' OR c.Val06='".$load."')";
        }
        if($load!='ALL'){
            if(!$size){
                $filters.= " AND c.Val01=60";
            }else{
                if($size>80 && $load == 'L')
                    $filters.= " AND c.Val01=60";
                else
                    $filters.= " AND c.Val01=".$size;
            }
        }
        break;

    case '4':
        if($size && $size!='ALL'){
            $filters.= " AND c.Val01=".$size;
        }
        break;

    case '5':
    switch ($type) {
        case 'rings':
            $filters.= " AND (c.IdTipologia=4 OR c.IdTipologia=5  OR c.IdTipologia=45)";
            break;
        case 'shackles':
            $filters.= " AND (c.IdTipologia=6 OR c.IdTipologia=7)";
            break;
        case 'halyard_blocks':
            $filters.= " AND c.IdTipologia=24";
            break;
        case 'snatchblocks':
            $filters.= " AND c.IdTipologia=8";
            break;
        case 'crossover':
            $filters.= " AND c.IdTipologia=9";
            break;
        case 'padeye':
            $filters.= " AND (c.IdTipologia=11 OR c.IdTipologia=25 OR c.IdTipologia=35)";
            break;
        case 'organisers':
            $filters.= " AND (c.IdTipologia=10 OR c.IdTipologia=47)";
            break;
    }
      break;

    case '6':
      # code...
      break;

    case '7':
      # code...
      break;

    case '8':

        if($type && $type!='ALL'){
            if($type=='JB')
                $filters.= " AND c.IdTipologia=26";
            else
                $filters.= " AND c.IdTipologia=27";
        }

        if($size && $size!='ALL'){
            $filters.= " AND c.Val06  LIKE '%".$size."%'";
        }

        break;

    case '9':
      # code...
      break;

    case '10':
      # code...
      break;

    case '11':

      break;

    case '12':
      if($type){
        if($type=='TB')
            $filters.= " AND c.IdTipologia=37";
        else{
          $filters.= " AND c.IdTipologia!=37";
            if($size && $size!='ALL') $filters.= " AND c.Val01=".$size;
        }
      }else{
        $filters.= " AND c.IdTipologia!=37";
        if($size && $size!='ALL') $filters.= " AND c.Val01=".$size;
      }
      break;

    case '13':
        if($type){
            if($type=='TB')
                $filters.= " AND c.IdTipologia=37";
            else{
                $filters.= " AND c.IdTipologia!=37";
                if($size && $size!='ALL') $filters.= " AND c.Val01=".$size;
            }
        }else{
            $filters.= " AND c.IdTipologia!=37";
            if($size && $size!='ALL') $filters.= " AND c.Val01=".$size;
        }
      break;
  }

  return $filters;

}
