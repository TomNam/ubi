<?php
if($_REQUEST["codiceProdotto"]){
    $query = "SELECT l.*, t.Tipologia as tipologia, t.TipologiaEn as tipologia_en FROM catalogo c LEFT JOIN linee l ON l.id=c.IdLInea LEFT JOIN tipologie t ON t.id=c.IdTipologia WHERE c.codice = '".$_REQUEST["codiceProdotto"]."'";
    $linee = $mysqli->query($query);
    $linea = $linee->fetch_object();

    $arr_tech_drw_accessories = [
      "Ring" => "rings",
      "High Load Shackle" => "shackles",
      "Snap Shackle" => "shackles",
      "Halyard Shackle" => "shackles",
      "Halyard Block" => "halyard_blocks",
      "Snatch Block" => "snatchblocks",
      "Cross over" => "crossover",
      "Soft pad eye" => "padeye",
      "Flush Deck Pad Eye" => "padeye",
      "Hard Pad Eye" => "padeye",
      "Organiser Fairlead" => "organizers",
      "Organiser" => "organizers"
    ];

    $f_type = $arr_tech_drw_accessories[$linea->tipologia];

}else if($_REQUEST["idl"]){
    $query = "SELECT * FROM linee WHERE id = '".$_REQUEST["idl"]."'";
    $linee = $mysqli->query($query);
    $linea = $linee->fetch_object();

    $f_type = $_REQUEST["f_type"];
}

$descrizione_linea = 'Descrizione_'.$_COOKIE['ubi_lang'];
?>
<div class="row logo-category">
    <div class="col-sm-12 col-md-12">
        <img src="images/<?=$linea->image?>" alt="">
        <hr class="hidden-sm">
    </div>
</div><!-- / row -->

<div class="row text-category">
    <hr class="visible-sm">
    <p class="col-sm-12 col-md-12 specs-category">
    <?=$linea->$descrizione_linea?>
    </p>

    <div class="col-sm-12 col-md-12 specs-category">
        <ul class="categoryTech unstyled-list inline-list hidden-sm text-center">
            <hr>
                <li><a href="<?=$linea->tech_specs_link?>" target="_blank"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TECH_SPEC']?></a></li>
            <hr>
            <?php if(($_REQUEST["idl"]==5 || $linea->id==5 ) && ($f_type!="" && $f_type!='snatchblocks')): ?>
            <p><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TECH_DRAW']?></p>
            <a class="fancybox" href="../images/cat-accessories/accessories-drawings-<?=$f_type?>-big.jpg">
                <img src="../images/cat-accessories/accessories-drawings-<?=$f_type?>-sml.jpg" alt="" class="img-responsive">
            </a>
            <?php endif; ?>
            <?php if($_REQUEST["idl"]==1): ?>
            <p><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SIZING_GUIDE']?></p>
                <a class="fancybox" href="../images/sizing-guide-big.png">
                    <img src="../images/sizing-guide-sml.png" alt="" class="img-responsive">
                </a>
                <div class="spacer20"></div>
            <?php endif; ?>
        </ul>
    </div>
</div> <!-- / row -->

<!-- MOBILE SIDEBAR -->
 <div class="row visible-sm mobile-cat">
    <div class="col-sm-6" style="border-right:1px solid #555;text-align:center;">
        <div class="categoryTech unstyled-list inline-list">
            <a href="<?=$linea->tech_specs_link?>" target="_blank"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TECH_SPEC']?></a>
        </div>
    </div>
    <div class="col-sm-6" style="border-right:1px solid #555;text-align:center;">
        <div class="categoryTech unstyled-list inline-list">
            <?php if($_REQUEST["idl"]==5 && ($_REQUEST["f_type"]!="" && $_REQUEST["f_type"]!='snatchblocks')): ?>
                <p><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TECH_DRAW']?></p>
                <a class="fancybox" href="../images/cat-accessories/accessories-drawings-<?=$_REQUEST["f_type"]?>-big.jpg">
                    <img src="../images/cat-accessories/accessories-drawings-<?=$_REQUEST["f_type"]?>-sml.jpg" alt="" class="img-responsive">
                </a>
                <div class="spacer20"></div>
            <?php endif; ?>
            <?php if($_REQUEST["idl"]==1): ?>
                <p><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SIZING_GUIDE']?></p>
                <a class="fancybox" href="../images/sizing-guide-big.png">
                    <img src="../images/sizing-guide-sml.png" alt="" class="img-responsive">
                </a>
                <div class="spacer20"></div>
            <?php endif; ?>
        </div>
    </div>
</div>
