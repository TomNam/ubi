<?php
include('filters_generator.php');
$query_filters = generate_qf($_REQUEST['idl'],$_REQUEST['f_size'],$_REQUEST['f_load'],$_REQUEST['f_type']);

$query = "SELECT DISTINCT c.*, l.Sigla as codice_linea, t.Sigla as codice_tipologia FROM catalogo c LEFT JOIN linee l ON l.id=c.IdLInea LEFT JOIN tipologie t ON t.Id=c.IdTipologia LEFT JOIN nomi_voci n ON n.IdTipologia = c.IdTipologia AND n.lingua = 3 ".$query_filters." ORDER BY c.Ordinale";

// echo $query."<br />";

$prodotti = $mysqli->query($query);
if($prodotti->num_rows>0){
    while($prodotto = $prodotti->fetch_object()){

        $productCode = $prodotto->Codice;
        $nomivoci_language = ($_COOKIE['ubi_lang']=='it') ? 1 : 2;
        $nomi_voci = $mysqli->query("SELECT NPrezzo,Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE Lingua=$nomivoci_language AND IdTipologia='".$prodotto->IdTipologia."'");
        $nome_voce = $nomi_voci->fetch_object();

        if($prodotto->codice_tipologia=='2:1') $codice_tipologia = '21';
        elseif($prodotto->codice_tipologia=='3:1') $codice_tipologia = '31';
        elseif($prodotto->codice_tipologia=='2:1LF') $codice_tipologia = '21LF';        
        else $codice_tipologia = $prodotto->codice_tipologia;

        $img_url = $prodotto->image;

        if($prodotto->NPrezzo=='0'){
            $prezzo = '<td class="price"><i class="fa fa-euro"></i></td>'.'<td><strong>'.number_format((float)$prodotto->Prezzo, 2, ',', '').'</strong>* <a href="javascript:void(0);" class="order add-to-whishlist"  data-id="'.$prodotto->IdArticolo.'" data-codice="'.$prodotto->Codice.'"  data-img-path="'.$img_url.'" data-cod-linea="'.$prodotto->codice_linea.'" data-cod-tipologia="'.$prodotto->codice_tipologia.'" data-show-price="'.$show_product_price.'" data-price="'.$prodotto->Prezzo.'" data-description="'.$prodotto->DescrizioneInglese.'" data-weight="'.$prodotto->Peso.'" data-load="'.$prodotto->MWL.'"><i class="fa fa-star"></i> Wishlist</a></td>';
        }else{
            $prezzo = '<td colspan="2"><a href="javascript:void(0);" class="order add-to-whishlist"  data-id="'.$prodotto->IdArticolo.'" data-codice="'.$prodotto->Codice.'"  data-img-path="'.$img_url.'" data-cod-linea="'.$prodotto->codice_linea.'" data-cod-tipologia="'.$prodotto->codice_tipologia.'" data-show-price="'.$show_product_price.'" data-price="'.$prodotto->Prezzo.'" data-description="'.$prodotto->DescrizioneInglese.'" data-weight="'.$prodotto->Peso.'" data-load="'.$prodotto->MWL.'"><i class="fa fa-star"></i> Wishlist</a></td>';
        }

        /*GESTIONE PRODUCT UNIT*/
        $valori_unit = $mysqli->query("SELECT Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE Lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_unit = $valori_unit->fetch_object();
        /*GESTIONE PRODUCT UNIT*/

        echo '<div class="col-sm-6 product">'.
                '<div class="col-sm-4 productImage text-center">'.
                    '<img src="'.$img_url.'" style="margin:0 auto;width:auto !important;max-width:100%;max-height:250px !important;" alt="">'.
                '</div>'.
                '<div class="col-sm-8">'.
                    '<table>'.
                        '<tr>'.
                            '<td></td>'.
                                '<td class="productName">'.$prodotto->Codice.'</td>'.
                            '</tr>'.
                        '<tr>'.
                            '<td>'.
                                '<i class="fa fa-info-circle"></i>'.
                            '</td>'.
                            '<td>'.
                                '<ul class="unstyled-list">';
                                    for($i=1;$i<7;$i++){
                                        $varnameVoce = 'Voce0'.$i;
                                        $varnameVal = 'Val0'.$i;
                                        if($prodotto->$varnameVal){
                                            echo '<li>'.strtolower($nome_voce->$varnameVoce).': <span class="bold text-info">'.$prodotto->$varnameVal.''.$valore_unit->$varnameVoce.'</span></li>';
                                        }
                                    }
                                    if($prodotto->MWL)
                                      echo '<li>mwl: <span class="bold text-info">'.$prodotto->MWL.' Kg</span></li>';
                                    if($prodotto->Peso){
                                      $peso_label = (($_COOKIE['ubi_lang']=='it') ? 'peso' : 'weight');
                                      echo '<li>'.$peso_label.': <span class="bold text-info">'.$prodotto->Peso.'g</span></li>';
                                    }
                                echo '</ul>'.
                            '</td>'.
                        '</tr>'.
                        '<tr>'.
                            '<td></td>';
                          if($_COOKIE['ubi_lang']=='en'){
                            echo '<td>Description: <span class="bold text-info">'.$prodotto->DescrizioneInglese.'</span></td>';
                          }else{
                            echo '<td>Descrizione: <span class="bold text-info">'.$prodotto->Descrizione.'</span></td>';
                          }

                        echo '</tr>'.
                        '<tr>'.
                            $prezzo.
                        '</tr>'.
                        '<tr>'.
                            '<td colspan="2"><br>*'.$lang[$_COOKIE['ubi_lang']]['_CATALOGUE_IVA'].'</td>'.
                        '</tr>'.
                    '</table>'.
                '</div>'.
            '</div>';
        }
}else{
    // 0 righe trovate; stampare messaggio "no items found"
    ?>
    <div class="row m-t-50" style="min-height:400px;">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12 text-center">
            <h3>No Products found for this category</h3>
        </div>
    </div>
    <?php
}
?>
