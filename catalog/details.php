<?php
$query = "SELECT c.*, l.Sigla as codice_linea, t.Sigla as codice_tipologia FROM catalogo c LEFT JOIN linee l ON l.id=c.IdLInea LEFT JOIN tipologie t ON t.Id=c.IdTipologia LEFT JOIN nomi_voci n ON n.IdTipologia = c.IdTipologia AND n.lingua = 3 WHERE c.Codice = '".$productCode."'";
$prodotti = $mysqli->query($query);
if($prodotti->num_rows>0){
    $prodotto = $prodotti->fetch_object();


    $nomivoci_language = ($_COOKIE['ubi_lang']=='it') ? 1 : 2;
    $nomi_voci = $mysqli->query("SELECT NPrezzo,Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE lingua=$nomivoci_language AND IdTipologia='".$prodotto->IdTipologia."'");
    $nome_voce = $nomi_voci->fetch_object();

    if($prodotto->codice_tipologia=='2:1') $codice_tipologia = '21';
    elseif($prodotto->codice_tipologia=='2:1LF') $codice_tipologia = '21LF';
    elseif($prodotto->codice_tipologia=='3:1') $codice_tipologia = '31';    
    else $codice_tipologia = $prodotto->codice_tipologia;

    if($prodotto->Val06){
        $prodotto->voce06label = $nome_voce->Voce06;
    }

    if($prodotto->Val07){
        if($nome_voce->Voce07=='WEIGHT' || $nome_voce->Voce07=='BL'){
            $valori_peso07 = $mysqli->query("SELECT Voce07 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
            $valore_peso07 = $valori_peso07->fetch_object();
        }
        $prodotto->voce07label = $nome_voce->Voce07;
        $prodotto->voce07unit = $valore_peso07->Voce07;
    }

    if($prodotto->Val08){
        if($nome_voce->Voce08=='WEIGHT'){
            $valori_peso08 = $mysqli->query("SELECT Voce08 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
            $valore_peso08 = $valori_peso08->fetch_object();
        }
        $prodotto->voce08label = $nome_voce->Voce08;
        $prodotto->voce08unit = $valore_peso08->Voce08;
    }


    $img_url = $prodotto->image;

    if($prodotto->NPrezzo==0 || $prodotto->NPrezzo=='0'){
      $show_product_price = "true";
    }else{
      $show_product_price = "false";
    }

    $wishlist_snippet = '<a href="javascript:void(0);" class="order add-to-whishlist" data-id="'.$prodotto->IdArticolo.'" data-codice="'.$prodotto->Codice.'"  data-img-path="'.$img_url.'" data-cod-linea="'.$prodotto->codice_linea.'" data-cod-tipologia="'.$prodotto->codice_tipologia.'" data-show-price="'.$show_product_price.'" data-price="'.$prodotto->Prezzo.'" data-description="'.$prodotto->DescrizioneInglese.'" data-weight="'.$prodotto->Peso.'" data-load="'.$prodotto->MWL.'"><i class="fa fa-star"></i> Wishlist</a>';


    if($prodotto->NPrezzo==0 || $prodotto->NPrezzo=='0'){
        $prezzo = '<td class="price"><i class="fa fa-euro"></i></td>'.'<td><strong>'.number_format((float)$prodotto->Prezzo, 2, ',', '').'</strong>* '.$wishlist_snippet.'</td>';
    }else{
        $prezzo = '<td colspan="2">'.$wishlist_snippet.'</td>';
    }

    /* GESTIONE MAX WORKING LOAD DEI PRODOTTI */
    if($nome_voce->Voce01=='MWL'){
        $valori_load = $mysqli->query("SELECT Voce01 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_load = $valori_load->fetch_object();
        $prodotto->mwl = $prodotto->Val01.$valore_load->Voce01;
    }
    if($nome_voce->Voce02=='MWL'){
        $valori_load = $mysqli->query("SELECT Voce02 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_load = $valori_load->fetch_object();
        $prodotto->mwl = $prodotto->Val02.$valore_load->Voce02;
    }
    if($nome_voce->Voce03=='MWL'){
        $valori_load = $mysqli->query("SELECT Voce03 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_load = $valori_load->fetch_object();
        $prodotto->mwl = $prodotto->Val03.$valore_load->Voce03;
    }
    if($nome_voce->Voce04=='MWL'){
        $valori_load = $mysqli->query("SELECT Voce04 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_load = $valori_load->fetch_object();
        $prodotto->mwl = $prodotto->Val04.$valore_load->Voce04;
    }
    if($nome_voce->Voce05=='MWL'){
        $valori_load = $mysqli->query("SELECT Voce05 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
        $valore_load = $valori_load->fetch_object();
        $prodotto->mwl = $prodotto->Val05.$valore_load->Voce05;
    }
    /* GESTIONE MAX WORKING LOAD DEI PRODOTTI */

    /*GESTIONE PRODUCT UNIT*/
    $valori_unit = $mysqli->query("SELECT Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
    $valore_unit = $valori_unit->fetch_object();
    /*GESTIONE PRODUCT UNIT*/

    //print_r($prodotto);
    echo '<div class="col-sm-6 product">'.
        '<div class="col-sm-4 productImage">'.
            '<img src="'.$img_url.'" alt="">'.
        '</div>'.
        '<div class="col-sm-8">'.
            '<table>'.
                '<tr>'.
                    '<td></td>'.
                    '<td class="productName">'.$prodotto->Codice.'</td>'.
                    '</tr>'.
                '<tr>'.
                            '<td>'.
                                '<i class="fa fa-info-circle"></i>'.
                            '</td>'.
                            '<td>'.
                                '<ul class="unstyled-list">';
                                    if($prodotto->Val01)
                                        echo '<li>'.strtolower($nome_voce->Voce01).': <span class="bold text-info">'.$prodotto->Val01.$valore_unit->Voce01.'</span></li>';
                                    if($prodotto->Val02)
                                        echo '<li>'.strtolower($nome_voce->Voce02).': <span class="bold text-info">'.$prodotto->Val02.$valore_unit->Voce02.'</span></li>';
                                    if($prodotto->Val03)
                                        echo '<li>'.strtolower($nome_voce->Voce03).': <span class="bold text-info">'.$prodotto->Val03.$valore_unit->Voce03.'</span></li>';
                                    if($prodotto->Val04)
                                        echo '<li>'.strtolower($nome_voce->Voce04).': <span class="bold text-info">'.$prodotto->Val04.$valore_unit->Voce04.'</span></li>';
                                    if($prodotto->Val05)
                                        echo '<li>'.strtolower($nome_voce->Voce05).': <span class="bold text-info">'.$prodotto->Val05.$valore_unit->Voce05.'</span></li>';
                                    if($prodotto->Val06)
                                        echo '<li>'.strtolower($nome_voce->Voce06).': <span class="bold text-info">'.$prodotto->Val06.$valore_unit->Voce06.'</span></li>';
                                    if($prodotto->Val07)
                                        echo '<li>'.strtolower($nome_voce->Voce07).': <span class="bold text-info">'.$prodotto->Val07.$valore_unit->Voce07.'</span></li>';
                                    if($prodotto->Val08)
                                        echo '<li>'.strtolower($nome_voce->Voce08).': <span class="bold text-info">'.$prodotto->Val08.$valore_unit->Voce08.'</span></li>';
                                    if($prodotto->MWL){
                                        echo '<li>mwl: <span class="bold text-info">'.$prodotto->MWL.' Kg</span></li>';
                                    }
                                    if($prodotto->Peso){
                                      $peso_label = (($_COOKIE['ubi_lang']=='it') ? 'peso' : 'weight');
                                      echo '<li>'.$peso_label.': <span class="bold text-info">'.$prodotto->Peso.'g</span></li>';
                                    }
                                echo '</ul>'.
                            '</td>'.
                        '</tr>'.
                '<tr>'.
                    '<td></td>';
                    if($_COOKIE['ubi_lang']=='en'){
                      echo '<td>Description: <span class="bold text-info">'.$prodotto->DescrizioneInglese.'</span></td>';
                    }else{
                      echo '<td>Description: <span class="bold text-info">'.$prodotto->Descrizione.'</span></td>';
                    }
                echo '</tr>'.
                '<tr>'.
                    $prezzo.
                '</tr>'.
                '<tr>'.
                    '<td colspan="2"><br>*'.$lang[$_COOKIE['ubi_lang']]['_CATALOGUE_IVA'].'</td>'.
                '</tr>'.
            '</table>'.
        '</div>'.
    '</div>';
}else{
    // 0 righe trovate; stampare messaggio "no items found"
    echo 'Product not found';
}
?>
