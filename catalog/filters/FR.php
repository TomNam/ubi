<?php
$type_arr = ['ALL', 'CLASSIC','M', 'RW', 'RWM','ACCESSORIES'];
if(!$_REQUEST['f_type']) $tf = 'ALL';
else $tf = $_REQUEST['f_type'];

$size_arr = ['ALL','87','100','125','150','200','250','250H'];
if(!$_REQUEST['f_size']) $sf = 'ALL';
else $sf = $_REQUEST['f_size'];
?>

<?php if($_COOKIE['ubi_lang']=='it'){
  $product_list_title = "Lista Prodotti";
  $select_size_label = "<small>Seleziona <strong>Dimensione</strong></small>";
  $select_type_label = "<small>Seleziona <strong>Tipologia</strong></small>";
}else{
  $product_list_title = "Products List";
  $select_size_label = "<small>Select <strong>Size</strong></small>";
  $select_type_label = "<small>Select <strong>Tipology</strong></small>";
}
?>

<!-- inizio filtro -->
<div class="col-xs-12"><h4 class="text-center"><?=$product_list_title?></h4></div>
<div class="categoryFilter clearfix">
    <div class="col-sm-5 col-lg-4">
        <?=$select_size_label?>
        <ul class="list-inline">
            <?php
            foreach ($size_arr as $key => $value) {
                if($value==$sf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_type=<?=$tf?>&f_size=<?=$value?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
    <div class="col-sm-3 col-lg-4">
        <?=$select_type_label?>
        <ul class="list-inline">
            <?php foreach($type_arr as $key => $value){
                if($value==$tf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_type=<?=$value?>&f_size=<?=$_REQUEST['f_size']?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
</div>
<!-- fine filtro -->
