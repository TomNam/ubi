<!-- inizio filtro -->
<?php if($_COOKIE['ubi_lang']=='it'){
  $product_list_title = "Lista Prodotti";
  $select_type_label = "<small>Seleziona <strong>Tipologia</strong></small>";
}else{
  $product_list_title = "Products List";
  $select_type_label = "<small>Select <strong>Tipology</strong></small>";
}
?>
<div class="col-xs-12"><h4 class="text-center"><?=$product_list_title?></h4></div>
<div class="categoryFilter clearfix">

    <div class="col-sm-12">
        <?=$select_type_label?>
        <ul class="list-inline">

            <?php
            $requested_type = $_REQUEST['f_type'];
            if($_COOKIE['ubi_lang']=='it')
              $arr_filters = ['' => 'all','rings' => 'anello','shackles' => 'grillo','halyard_blocks' => 'grillo drizza','snatchblocks' => 'pastecca','crossover' => 'crossover','padeye' => 'padeye','organisers' => 'organiser'];
            else
              $arr_filters = ['' => 'all','rings' => 'rings','shackles' => 'shackles','halyard_blocks' => 'halyard blocks','snatchblocks' => 'snatchblocks','crossover' => 'crossover','padeye' => 'padeye','organisers' => 'organisers'];
            foreach($arr_filters as $key => $value){
                if($requested_type==$key){
                    ?><li class="selected-filter"><a class="filter-links" href="index.php?p=catalog&idl=5&f_type=<?=$key?>"><?=strtoupper($value)?></a></li><?php
                }else{
                    ?><li><a class="filter-links" href="index.php?p=catalog&idl=5&f_type=<?=$key?>"><?=strtoupper($value)?></a></li><?php
                }
            }
            ?>

        </ul>
    </div>
</div>
<!-- fine filtro -->
