<?php
// Load filter manager
$type_arr = ['RTV' => 'RTV', 'TB' => 'TB'];
if(!$_REQUEST["f_type"]) $tf = 'RTV';
else $tf = $_REQUEST["f_type"];

// Puleggia Size filter manager
$size_arr = ['ALL',55,77,100,125,150];
if(!$_REQUEST["f_size"] || $_REQUEST["f_size"]=='ALL') $sf = 'ALL';
else $sf = $_REQUEST["f_size"];
?>
<?php if($_COOKIE['ubi_lang']=='it'){
  $product_list_title = "Lista Prodotti";
  $select_type_label = "<small>Seleziona <strong>Tipologia</strong></small>";
  $select_size_label = "<small>Seleziona <strong>Dimensione</strong></small>";
  $select_category_label = "<small>Seleziona <strong>Categoria</strong></small>";
}else{
  $product_list_title = "Products List";
  $select_type_label = "<small>Select <strong>Tipology</strong></small>";
  $select_size_label = "<small>Select <strong>Size</strong></small>";
  $select_category_label = "<small>Select <strong>Category</strong></small>";
}
?>
<!-- inizio filtro -->
<div class="col-xs-12"><h4 class="text-center"><?=$product_list_title?></h4></div>
<div class="categoryFilter clearfix">
    <?php if($_REQUEST['idl']=='3' || $_REQUEST['idl']=='12' || $_REQUEST['idl']=='13' ): ?>
        <div class="col-sm-4">
            <?=$select_category_label?>
            <ul class="list-inline">
                <li><a class="filter-links" href="index.php?p=catalog&idl=3">CLASSIC</a></li>
                <li><a class="filter-links" href="index.php?p=catalog&idl=12">ULTRA</a></li>
                <li class="selected-filter"><a class="filter-links" href="index.php?p=catalog&idl=13">EVE</a></li>

            </ul>
        </div>
    <?php endif; ?>
    <?php if(!$_REQUEST['f_type'] || $_REQUEST['f_type']=='RTV'):?>
    <div class="col-sm-5 col-lg-4">
        <?=$select_size_label?>
        <ul class="list-inline">
            <?php
            foreach ($size_arr as $key => $value) {
                if($value==$sf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_type=<?=$tf?>&f_size=<?=$value?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
    <?php endif; ?>
    <div class="col-sm-3 col-lg-4">
        <?=$select_type_label?>
        <ul class="list-inline">
            <?php foreach($type_arr as $key => $value){
                if($key==$tf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_type=<?=$key?>&f_size=<?=$_REQUEST['f_size']?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
</div>
<!-- fine filtro -->
