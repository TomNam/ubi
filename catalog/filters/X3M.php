<?php
$size_arr = ['ALL',32,40,46,50,55,60,70,90,110,120];
if(!$_REQUEST['f_size'] || $_REQUEST['f_size']=='ALL') $sf = 'ALL';
else $sf = $_REQUEST['f_size'];
?>

<?php if($_COOKIE['ubi_lang']=='it'){
  $product_list_title = "Lista Prodotti";
  $select_size_label = "<small>Seleziona <strong>Dimensione</strong></small>";
}else{
  $product_list_title = "Products List";
  $select_size_label = "<small>Select <strong>Size</strong></small>";
}
?>
<!-- inizio filtro -->
<div class="col-xs-12"><h4 class="text-center"><?=$product_list_title?></h4></div>
<div class="categoryFilter clearfix">
    <div class="col-sm-12 col-lg-12">
        <?=$select_size_label?>
        <ul class="list-inline">
            <?php foreach($size_arr as $key => $value){
                if($value==$sf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_size=<?=$value?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
</div>
<!-- fine filtro -->
