<?php
// Load filter manager
$load_arr = ['ALL' => 'ALL','L' => 'LOW', 'H' => 'HIGH'];
if(!$_REQUEST["f_load"]) $lf = 'ALL';
else $lf = $_REQUEST["f_load"];

// Puleggia Size filter manager
if($lf!='ALL'){
    if($lf=='L') $size_arr = [60,80];
    else $size_arr = [60,80, 100, 125, 150];
}

if($lf!='ALL'){
    if(!$_REQUEST['f_size']) $sf = 60;
    else{
        if($_REQUEST['f_size']>80 && $lf == 'L')
            $sf = 60;
        else
            $sf = $_REQUEST['f_size'];
    }
}
?>
<?php if($_COOKIE['ubi_lang']=='it'){
  $product_list_title = "Lista Prodotti";
  $select_type_label = "<small>Seleziona <strong>Tipologia</strong></small>";
  $select_size_label = "<small>Seleziona <strong>Dimensione</strong></small>";
  $select_load_label = "<small>Seleziona <strong>Carico</strong></small>";
}else{
  $product_list_title = "Products List";
  $select_type_label = "<small>Select <strong>Tipology</strong></small>";
  $select_size_label = "<small>Select <strong>Size</strong></small>";
  $select_load_label = "<small>Select <strong>Load</strong></small>";
}
?>
<!-- inizio filtro -->
<div class="col-xs-12"><h4 class="text-center"><?=$product_list_title?></h4></div>
<div class="categoryFilter clearfix">
    <?php if($_REQUEST['idl']=='3' || $_REQUEST['idl']=='12' || $_REQUEST['idl']=='13' ): ?>
        <div class="col-sm-4">
            <?=$select_type_label?>
            <ul class="list-inline">
                <li class="selected-filter"><a class="filter-links" href="index.php?p=catalog&idl=3">CLASSIC</a></li>
                <li><a class="filter-links" href="index.php?p=catalog&idl=12">ULTRA</a></li>
                <li><a class="filter-links" href="index.php?p=catalog&idl=13">EVE</a></li>

            </ul>
        </div>
    <?php endif; ?>
    <?php if($lf!='ALL'): ?>
    <div class="col-sm-5 col-lg-4">
        <?=$select_size_label?>
        <ul class="list-inline">
            <?php
            foreach ($size_arr as $key => $value) {
                if($value==$sf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_load=<?=$lf?>&f_size=<?=$value?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
    <?php endif; ?>
    <div class="col-sm-3 col-lg-4">
        <?=$select_load_label?>
        <ul class="list-inline">
            <?php foreach($load_arr as $key => $value){
                if($key==$lf) $class_selected = 'class="catalog-filter-link selected-filter"';
                else $class_selected = 'class="catalog-filter-link"';
                ?><li <?=$class_selected?>><a href="index.php?p=catalog&idl=<?=$_REQUEST['idl']?>&f_load=<?=$key?>&f_size=<?=$_REQUEST['f_size']?>"><?=$value?></a></li><?php
            }
            ?>
        </ul>
    </div>
</div>
<!-- fine filtro -->
