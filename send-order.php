<?php error_reporting(1); include("./lib/dbconn.php"); include("./lib/stdlib.php"); include('./mailer/class.phpmailer.php'); include ('./lib/PHPExcel/Classes/PHPExcel/IOFactory.php');

$nome = $_POST["name"];
$cognome = $_POST["surname"];
$email = $_POST["email"];
$phone = $_POST["phone"];

$prodotti = $_POST["products"];

$mail = new PHPMailer();
$mail->SetFrom('no-reply@ubimaioritalia.com',"Ubi Maior Italia");
$mail->addReplyTo('no-reply@ubimaioritalia.com',"Ubi Maior Italia");

$env = getenv('HTTP_UBI_ENV');
switch ($env) {
    case 'dev':
        $mail->AddAddress("simo.namaqua@gmail.com");
        break;

    case 'staging':
        $mail->AddAddress("simo.namaqua@gmail.com");
        $mail->AddAddress("tommaso.stigler@gmail.com");
        $mail->AddAddress("info@ubimaioritalia.com");
        break;

    case 'prod':
    $mail->AddAddress("info@ubimaioritalia.com");
        break;
    
    default:
        $mail->AddAddress("info@ubimaioritalia.com");
        break;
}

/** EXCEL CREATOR **/
$total = 0;
$data = '"CODE","DESCRIPTION","WEIGHT","MWL","UNIT PRICE","QTY","PRICExQTY"' . "\n";
foreach($prodotti as $prodotto){

    if($prodotto['type']!=='apparel'){
        if($prodotto['codice_tipologia']=='2:1') $codice_tipologia = '21';
        elseif($prodotto['codice_tipologia']=='3:1') $codice_tipologia = '31';
        elseif($prodotto['codice_tipologia']=='2:1LF') $codice_tipologia = '21LF';        
        else $codice_tipologia = $prodotto['codice_tipologia'];    

        if($prodotto["mostra_prezzo"]==="true" || $prodotto["mostra_prezzo"]===true){
            $prezzo_totale_prodotto = '"€ '.$prodotto["prezzo"].'"';
            $prezzo_qty_unit_price = '"€ '.($prodotto["prezzo"]*$prodotto["qty"]).'"';
            $total += ($prodotto["prezzo"]*$prodotto["qty"]);
        }else{
            $prezzo_totale_prodotto = '"n.d."';
            $prezzo_qty_unit_price = '"n.d."';
        }
    }else{
        $codice_tipologia = 'n.d.';
        $prezzo_totale_prodotto = '"€ '.$prodotto["prezzo"].'"';
        $prezzo_qty_unit_price = '"€ '.($prodotto["prezzo"]*$prodotto["qty"]).'"';
        $total += ($prodotto["prezzo"]*$prodotto["qty"]);
    }

    $data .= '"'.$prodotto['codice'].'","'.$prodotto["description"].'","'.$prodotto["weight"].'","'.$prodotto["load"].'",'.$prezzo_totale_prodotto.',"'.$prodotto["qty"].'",'. $prezzo_qty_unit_price . "\n";                
}

$data .= '"","","","","","","",""' . "\n";
$data .= '"","","","","","","","TOTAL","€ '.$total.'"' . "\n";
$filename = 'ubimaior_order_'.time();
$order_name = 'orders/'.$filename.'.csv';
$f = fopen( $order_name, 'wb');
fwrite($f , $data );
fclose($f);

$objReader = PHPExcel_IOFactory::createReader('CSV');
$objReader->setInputEncoding('UTF-8');
$objPHPExcel = $objReader->load($order_name);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$xls_filepath = './xls/'.$filename.'.xls';
$objWriter->save($xls_filepath);

/** EXCEL CREATOR **/


$content='
<table border="0" style="width:100%;height:100%;background: #202027;">
    <tr valign="top" height="80">
		<td> 
			<table style="position:relative;width:700px;height:100px;margin:0 auto;">	
                <tr>
                    <td style="width:100%;text-align:center;">
                        <img src="'.$public_url.'/images/logo_ubi_white.png" alt="Ubi Maior Italia - Logo" width="200" />            
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<tr valign="top" align="center" style="min-height:500px;height:auto;">
		<td> 
			<table style="position:relative;width:700px;min-height:500px;height:auto;margin:0 auto;background:#f4f4f4;font-family:helvetica, sans-serif;color:#5f5f5f;">												
				<tr>
					<td>
                        <p style="padding:0 10px;font-size:15px;"><b><i>Ciao Admin,</i></b> <br /><br />
                            Un visitatore ha effettuato un nuovo ordine dal sito www.ubimaioritalia.com.<br /><br />
                            DETTAGLI DEL CLIENTE:<br /><br />
                            Nome: <b>'.$nome.'</b><br />
                            Cognome: <b>'.$cognome.'</b><br />
                            Email: <b>'.$email.'</b><br />
                            Telefono:  <b>'.$phone.'</b>
                        </p>
                    </td>
                </tr>
                <tr valign="top">					
                    <td align="center">
                        <p style="padding:0 10px;font-size:15px;">DETTAGLI DELL\'ORDINE: </p>
                        <div style="width:95%;margin:0 15px 15px;background: #212727; color: #fff; height: 20px;font-weight:bold;padding:5px;">
                            <div style="width: 16.66666667%;float:left;display:inline;">IMAGE</div>
                            <div style="width: 16.66666667%;float:left;display:inline;">CODE</div>
                            <div style="width: 33.33333333%;float:left;display:inline;">DESCRIPTION</div>
                            <div style="width: 16.66666667%;float:left;display:inline;">PRICE</div>
                            <div style="width: 16.66666667%;float:left;display:inline;">QTY</div>
                        </div>';

						$total = 0;
						foreach($prodotti as $prodotto){

                            if($prodotto['type']!=='apparel'){
                                $prodotto_weight = '';

                                if($prodotto['codice_tipologia']=='2:1') $codice_tipologia = '21';
                                elseif($prodotto['codice_tipologia']=='3:1') $codice_tipologia = '31';
                                elseif($prodotto['codice_tipologia']=='2:1LF') $codice_tipologia = '21LF';                                
                                else $codice_tipologia = $prodotto['codice_tipologia'];			
                                
                                $img_url = $prodotto['product_image'];

                                if($prodotto["weight"] && $prodotto["weight"]!='n.d.')
                                    $prodotto_weight .= '<br />Weigth: '.$prodotto["weight"];
                                if($prodotto["load"] && $prodotto["load"]!='n.d.')
                                    $prodotto_weight .= '<br />MWL: '.$prodotto["load"];
                                

                                $content.='
                                <div style="width:95%;margin:0 15px 15px;display:flex;align-items: center;font-size: 14px;color: #555;border-top: 1px solid #eee;border-bottom: 1px solid #eee; margin-bottom: 10px;">
                                    <div style="width: 16.66666667%;float:left;"><img src="'.$public_url.'/'.$img_url.'" height="70" alt="Cover Product - Ubi Maior"></div>
                                    <div style="width: 16.66666667%;float:left;">'.$prodotto['codice'].'</div>
                                    <div style="width: 33.33333333%;float:left;">'.$prodotto["description"].$prodotto_weight.$prodotto_load.'</div>';
                                    if($prodotto["mostra_prezzo"]==="true" || $prodotto["mostra_prezzo"]===true){
                                        $content .= '<div style="width: 16.66666667%;float:left;">&#8364; '.($prodotto["prezzo"]*$prodotto["qty"]).'<br /><small>('.$prodotto["prezzo"].'x'.$prodotto["qty"].')</small></div>';
                                    }else{
                                        $content .= '<div style="width: 16.66666667%;float:left;">n.d.</div>';
                                    }
                                    $content .= '<div style="width: 16.66666667%;float:left;">'.$prodotto["qty"].'</div>
                                </div>                        
                                ';

                            }else{
                                $content.='
                                <div style="width:95%;margin:0 15px 15px;display:flex;align-items: center;font-size: 14px;color: #555;border-top: 1px solid #eee;border-bottom: 1px solid #eee; margin-bottom: 10px;">
                                    <div style="width: 16.66666667%;float:left;"><img src="'.$public_url.'/img/apparel/'.$img_url.'" height="70" alt="Cover Product Apparel - Ubi Maior"></div>
                                    <div style="width: 16.66666667%;float:left;">'.$prodotto['codice'].'</div>
                                    <div style="width: 33.33333333%;float:left;">'.$prodotto["description"].'</div>                                   
                                    <div style="width: 16.66666667%;float:left;">&#8364; '.($prodotto["prezzo"]*$prodotto["qty"]).'<br /><small>('.$prodotto["prezzo"].'x'.$prodotto["qty"].')</small></div>
                                    <div style="width: 16.66666667%;float:left;">'.$prodotto["qty"].'</div>
                                </div>';
                            }
                            
                            if($prodotto['type']!=='apparel' && ($prodotto["mostra_prezzo"]==="true" || $prodotto["mostra_prezzo"]===true)){
                                $total += ($prodotto["prezzo"]*$prodotto["qty"]);
                            }else if($prodotto['type']==='apparel'){
                                $total += ($prodotto["prezzo"]*$prodotto["qty"]);
                            }
						}						                        
                    
					$content.='
					</td>												
                </tr>
				<tr style="width:100%;height:20px;clear:both;"><td>&nbsp;</td></tr>
                <tr valign="top">					
                    <td align="center">
                        <div style="font-family:helvetica, sans-serif;width:98%;background: #ee1d23;color:#fff;padding:10px 0;">
                            TOTALE: &#8364; '.$total.'
                        </div>
                    </td>												
                </tr>
				<tr style="width:100%;height:20px;clear:both;"><td>&nbsp;</td></tr>
			</table>  
		</td>   
	</tr>   		
    <tr valign="top">
		<td> 
			<table style="position:relative;width:700px;height:100px;margin:0 auto;">	
                <tr>
					<td style="width:100%;text-align:center;">						         
						<img src="'.$public_url.'/images/ubi-footer.png" alt="Ubi Maior Italia" width="300" />						
						<p style="font-family:helvetica, sans-serif;font-size:11px;text-align:center;color:#fff;text-decoration:none;">
                            UBI MAIOR ITALIA &#174; by DINI s.r.l., Meccanica di precisione <br />
                            via di Serravalle 35/37/39 - 50065 - Molino del Piano - Pontassieve - Firenze <br />
                            Tel. 055 8364421 - Fax 055 8364614 - info@ubimaioritalia.com
                        </p>
						</ul>
					</td>
				</tr>
            </table>
        </td>
    </tr>
</table>';

$mail->Subject = "Nuovo Ordine - Ubi Maior Italia";   
$mail->addAttachment($xls_filepath);
$mail->MsgHTML($content);

$mail2 = new PHPMailer();
$mail2->SetFrom('no-reply@ubimaioritalia.com',"Ubi Maior Italia");
$mail2->addReplyTo('no-reply@ubimaioritalia.com',"Ubi Maior Italia");
$mail2->AddAddress($email);

$content2='
<table border="0" style="width:100%;height:100%;background: #202027;">
    <tr valign="top" height="80">
		<td> 
			<table style="position:relative;width:700px;height:100px;margin:0 auto;">	
                <tr>
                    <td style="width:100%;text-align:center;">
                        <img src="'.$public_url.'/images/logo_ubi_white.png" alt="Ubi Maior Italia - Logo" width="200" />            
                    </td>
                </tr>
            </table>
        </td>
    </tr>
	<tr valign="top" align="center" style="min-height:500px;height:auto;">
		<td> 
			<table style="position:relative;width:700px;min-height:500px;height:auto;margin:0 auto;background:#f4f4f4;font-family:helvetica, sans-serif;color:#5f5f5f;">												
				<tr>
					<td>
                        <p style="padding:0 10px;font-size:15px;"><b><i>Dear '.$nome.' '.$cognome.',</i></b> <br /><br />
                            Thank you for sending your order to us.<br />
                            A member of our staff will come back to you as soon as possible.<br /><br />
                            Kind regards,<br />
                            Ubi Maior Italia Staff
                        </p>
                    </td>
                </tr>
                <tr valign="top">					
                    <td align="center">
                        <p style="padding:0 10px;font-size:15px;">DETTAGLI DELL\'ORDINE: </p>
                        <div style="width:95%;margin:0 15px 15px;background: #212727; color: #fff; height: 20px;font-weight:bold;padding:5px;">
                            <div style="width: 16.66666667%;float:left;display:inline;">IMAGE</div>
                            <div style="width: 16.66666667%;float:left;display:inline;">CODE</div>
                            <div style="width: 33.33333333%;float:left;display:inline;">DESCRIPTION</div>
                            <div style="width: 16.66666667%;float:left;display:inline;">PRICE</div>
                            <div style="width: 16.66666667%;float:left;display:inline;">QTY</div>
                        </div>';

						$total = 0;
						foreach($prodotti as $prodotto){

                            if($prodotto['type']!=='apparel'){
                                $prodotto_weight = '';

                                if($prodotto['codice_tipologia']=='2:1') $codice_tipologia = '21';
                                elseif($prodotto['codice_tipologia']=='3:1') $codice_tipologia = '31';
                                elseif($prodotto['codice_tipologia']=='2:1LF') $codice_tipologia = '21LF';                                
                                else $codice_tipologia = $prodotto['codice_tipologia'];			
                                
                                $img_url = $prodotto['product_image'];

                                if($prodotto["weight"] && $prodotto["weight"]!='n.d.')
                                    $prodotto_weight .= '<br />Weigth: '.$prodotto["weight"];
                                if($prodotto["load"] && $prodotto["load"]!='n.d.')
                                    $prodotto_weight .= '<br />MWL: '.$prodotto["load"];
                                

                                $content2.='
                                <div style="width:95%;margin:0 15px 15px;display:flex;align-items: center;font-size: 14px;color: #555;border-top: 1px solid #eee;border-bottom: 1px solid #eee; margin-bottom: 10px;">
                                    <div style="width: 16.66666667%;float:left;"><img src="'.$public_url.'/'.$img_url.'" height="70" alt="Cover Product - Ubi Maior"></div>
                                    <div style="width: 16.66666667%;float:left;">'.$prodotto['codice'].'</div>
                                    <div style="width: 33.33333333%;float:left;">'.$prodotto["description"].$prodotto_weight.$prodotto_load.'</div>';
                                    if($prodotto["mostra_prezzo"]==="true" || $prodotto["mostra_prezzo"]===true){
                                        $content2 .= '<div style="width: 16.66666667%;float:left;">&#8364; '.($prodotto["prezzo"]*$prodotto["qty"]).'<br /><small>('.$prodotto["prezzo"].'x'.$prodotto["qty"].')</small></div>';
                                    }else{
                                        $content2 .= '<div style="width: 16.66666667%;float:left;">n.d.</div>';
                                    }
                                    $content2 .= '<div style="width: 16.66666667%;float:left;">'.$prodotto["qty"].'</div>
                                </div>                        
                                ';

                            }else{
                                $content2.='
                                <div style="width:95%;margin:0 15px 15px;display:flex;align-items: center;font-size: 14px;color: #555;border-top: 1px solid #eee;border-bottom: 1px solid #eee; margin-bottom: 10px;">
                                    <div style="width: 16.66666667%;float:left;"><img src="'.$public_url.'/img/apparel/'.$img_url.'" height="70" alt="Cover Product Apparel - Ubi Maior"></div>
                                    <div style="width: 16.66666667%;float:left;">'.$prodotto['codice'].'</div>
                                    <div style="width: 33.33333333%;float:left;">'.$prodotto["description"].'</div>                                   
                                    <div style="width: 16.66666667%;float:left;">&#8364; '.($prodotto["prezzo"]*$prodotto["qty"]).'<br /><small>('.$prodotto["prezzo"].'x'.$prodotto["qty"].')</small></div>
                                    <div style="width: 16.66666667%;float:left;">'.$prodotto["qty"].'</div>
                                </div>';
                            }
                            
                            if($prodotto['type']!=='apparel' && ($prodotto["mostra_prezzo"]==="true" || $prodotto["mostra_prezzo"]===true)){
                                $total += ($prodotto["prezzo"]*$prodotto["qty"]);
                            }else if($prodotto['type']==='apparel'){
                                $total += ($prodotto["prezzo"]*$prodotto["qty"]);
                            }
						}						                        
                    
					$content2.='
					</td>												
                </tr>
				<tr style="width:100%;height:20px;clear:both;"><td>&nbsp;</td></tr>
                <tr valign="top">					
                    <td align="center">
                        <div style="font-family:helvetica, sans-serif;width:98%;background: #ee1d23;color:#fff;padding:10px 0;">
                            TOTAL: &#8364; '.$total.'
                        </div>
                    </td>												
                </tr>
				<tr style="width:100%;height:20px;clear:both;"><td>&nbsp;</td></tr>
			</table>  
		</td>   
	</tr>   		
    <tr valign="top">
		<td> 
			<table style="position:relative;width:700px;height:100px;margin:0 auto;">	
                <tr>
					<td style="width:100%;text-align:center;">						         
						<img src="'.$public_url.'/images/ubi-footer.png" alt="Ubi Maior Italia" width="300" />						
						<p style="font-family:helvetica, sans-serif;font-size:11px;text-align:center;color:#fff;text-decoration:none;">
                            UBI MAIOR ITALIA &#174; by DINI s.r.l., Meccanica di precisione <br />
                            via di Serravalle 35/37/39 - 50065 - Molino del Piano - Pontassieve - Firenze <br />
                            Tel. 055 8364421 - Fax 055 8364614 - info@ubimaioritalia.com
                        </p>
						</ul>
					</td>
				</tr>
            </table>
        </td>
    </tr>
</table>';

$mail2->Subject = "Your Order Summary - Ubi Maior Italia";   
$mail2->addAttachment($xls_filepath);
$mail2->MsgHTML($content2);

//------------------------------------------------------------------------------ 
         
if(!$mail->Send() || !$mail2->Send())
	echo json_encode(array("response"=>false));
else
	echo json_encode(array("response"=>true));