// menu

$(window).on('scroll touchmove', function() {
  $('nav').toggleClass('navScrolled', $(document).scrollTop() > 0);
});

$(document).ready(function() {
  // Open menu
  $('.navIcon').on('click', function() {
    $(this).toggleClass('open');
    $('#searchOverlay').fadeOut(500);
    $('#nav').fadeToggle(500);
    $('body').toggleClass('disableScroll'); //disabilita lo scroll sotto l'overlay. Applicato al body
  });

  // Open search

  $('.search').on('click', function() {
    $('#searchOverlay').fadeToggle(500);
    $('body').toggleClass('disableScroll');
    $('.inputSearch').focus();
  });

  $('#closeSearch').on('click', function() {
    $('#searchOverlay').fadeToggle(500);
    $('body').toggleClass('disableScroll');
  });

  // animation first slide
  $('.first-image').animate(
    {
      marginRight: '0%',
      opacity: 1
    },
    2000,
    function() {
      $('.subtitle h2').animate(
        {
          opacity: 0.5
        },
        500,
        function() {
          $('.mouse-wrapper').fadeIn(1500);
        }
      );
    }
  );
  $('.second-image').animate(
    {
      marginLeft: '0%',
      opacity: 1
    },
    2000
  );

  // $('.vela').delay(3000).fadeOut(500);
});

// PRODUCT Pages

var showNextRow = function() {
  var nextInvisibleRow = $('#table-closer tr')
    .not(':visible')
    .filter(':first');
  if (nextInvisibleRow.length > 0) {
    nextInvisibleRow.delay(100).fadeIn(500, showNextRow);
  }
};

$('.closer').on('click', function() {
  if ($(this).hasClass('open')) {
    $(this).removeClass('open');
    $('.table-container').fadeOut(500, function() {
      $('h1').animate(
        {
          marginTop: 60
        },
        800,
        function() {
          $('#table-closer tr').hide();
        }
      );
    });
  } else {
    $(this).addClass('open');
    $('h1').animate(
      {
        marginTop: -20
      },
      800,
      function() {
        $('.table-container').slideDown(500, function() {
          showNextRow();
        });
      }
    );
  }
});

// Carousel

$(document).ready(function() {
  $('.product-carousel').slick({
    // autoplay: true,
    autoplaySpeed: 2000
  });
});

// Fancybox

$(document).ready(function() {
  $('.fancybox').fancybox();
});

$(document).ready(function() {
  $('.various').fancybox({
    maxWidth: 800,
    maxHeight: 600,
    fitToView: false,
    width: '70%',
    height: '70%',
    autoSize: false,
    closeClick: false,
    openEffect: 'none',
    closeEffect: 'none'
  });
});
