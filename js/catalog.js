$(document).ready(function() {
  $(document).on('click', '.add-to-whishlist', function() {
    var products = [],
      product_code = $(this).data('codice'),
      product_id = $(this).data('id'),
      product_image = $(this).data('img-path'),
      product_cod_linea = $(this).data('cod-linea'),
      product_cod_tipologia = $(this).data('cod-tipologia'),
      product_price = $(this).data('price'),
      product_show_price = $(this).data('show-price'),
      product_description = $(this).data('description');
    product_weight = $(this).data('weight');
    product_load = $(this).data('load');
    product_load_value = $(this).data('load-value');

    var product = {
      id: product_id,
      product_image: product_image,
      codice: product_code,
      codice_linea: product_cod_linea,
      codice_tipologia: product_cod_tipologia,
      prezzo: product_price,
      mostra_prezzo: product_show_price,
      description: product_description,
      weight: product_weight,
      load: product_load,
      load_value: product_load_value,
      qty: 1
    };

    if (!localStorage.getItem('ubi_whishlist')) {
      products.push(product);
      localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    } else {
      products = JSON.parse(localStorage.ubi_whishlist);
      if (!_.findWhere(products, { codice: product_code })) {
        products.push(product);
        toastr.success('Product added to Wishlist', 'Success!');
      } else {
        _.each(products, function(p) {
          if (p.codice == product_code) {
            p.qty = p.qty + 1;
          }
        });
        toastr.success('Wishlist product qty updated', 'Success!');
      }
      localStorage.removeItem('ubi_whishlist');
      localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    }
  });
});
