var refreshTime = 600000; // every 10 minutes in milliseconds
window.setInterval( function() {
    $.ajax({
        cache: false,
        type: "GET",
        url: "./importer/refreshSession.php"
    }).done(function(){
    	console.log('Session refreshed');
    });
}, refreshTime );