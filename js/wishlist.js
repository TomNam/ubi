function checkemail(email) {
  var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
  if (filter.test(email)) return true;
  else {
    return false;
  }
}

function renderCatalogue(p){

  var weight = 'n.d.',
    load = 'n.d.';

  if (p.codice_tipologia == '2:1')
    codiceTipologia = p.codice_tipologia = '21';
  else if (p.codice_tipologia == '3:1')
    codiceTipologia = p.codice_tipologia = '31';
  else if (p.codice_tipologia == '2:1LF')
    codiceTipologia = p.codice_tipologia = '21LF';      
  else codiceTipologia = p.codice_tipologia;

  if (p.weight && p.weight != 'n.d.') {
    weight = '<br /><span class="title">Weight:</span> ' + p.weight;
  }
  if (p.load && p.weight != 'n.d.') {
    load = '<br /><span class="title">MWL:</span> ' + p.load;
  }

  var modalID = new Date().valueOf();

  if (p.mostra_prezzo) {
    price_div = '<div class="col-xs-4 col-sm-2 title">€ ' + p.total_item + '<br /><small><i>( € ' + Math.round(parseFloat(p.prezzo)) + 'x' + p.qty + ' )</i></small></div>';
  } else {
    price_div = '<div class="col-xs-4 col-sm-2 title">n.d.</div>';
  }

  var html =
  '<div class="row wishlist-line table-wish">' +
    '<div class="col-xs-2 col-sm-1">' +
      '<img src="' + p.product_image + '" height="55" alt="Cover Product - Ubi Maior" />' +
    '</div>' +
    '<div class="col-xs-2 col-sm-2 title">' + p.codice + '</div>' +
    '<div class="hidden-xs col-sm-3">' + '<span class="title">Description:</span> <br> ' + p.description + weight + load + '</div>' +
    price_div +
    '<div class="col-xs-3 col-sm-1">' +
      '<div class="btn btn-small item-number title">' + p.qty + '</div>' +
      '<ul class="add-remove-item">' +
        '<li class="add-qty-whishlist" data-codice="' + p.codice +'"><p>+</p></li>' +
        '<li class="subtract-qty-whishlist" data-codice="' + p.codice + '"><p>_</p></li>' +
      '</ul>' +
    '</div>' +
    '<div class="col-xs-3 col-sm-4">' +
      '<div class="btn-group" role="group">' +
        '<button data-modal="' + modalID + '" data-type="catalogue" data-codice="' + p.codice + '" class="btn btn-sml open-details"><i class="fa fa-search-plus"></i> <span class="hidden-xs">Details</span></button> ' +
        '<button href="javascript:void(0)" class="btn btn-sml remove-whishlist" data-codice="' + p.codice + '"><i class="fa fa-trash"></i> <span class="hidden-xs">Remove</span></button>' +
      '</div>' +
    '</div>' +
  '</div>' +
  '<div class="modal fade" id="' + modalID + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
    '<div class="modal-dialog modal-lg" role="document">' +
      '<div class="modal-content">' +
        '<div class="modal-header">' +
          '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
          '<h4 class="modal-title" id="myModalLabel">Product Details</h4>' +
        '</div>' +
        '<div class="modal-body"></div>' +
        '<div class="modal-footer">' +
          '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>';

  $('.whislist-container').append(html);
}

function renderApparel(p){  

  var modalID = new Date().valueOf();

  var html =
  '<div class="row wishlist-line table-wish">' +
    '<div class="col-xs-2 col-sm-1">' +
      '<img src="img/apparel/' + p.product_image + '" class="img-responsive" alt="Cover Product - Ubi Maior" />' +
    '</div>' +
    '<div class="col-xs-2 col-sm-2 title">' + p.codice + '</div>' +
    '<div class="hidden-xs col-sm-3">' + '<span class="title">Description:</span> <br> ' + p.description + '</div>' +
    '<div class="col-xs-4 col-sm-2 title">€ ' + p.total_item + '<br /><small><i>( € ' + Math.round(parseFloat(p.prezzo)) + 'x' + p.qty + ' )</i></small></div>' +
    '<div class="col-xs-3 col-sm-1">' +
      '<div class="btn btn-small item-number title">' + p.qty + '</div>' +
      '<ul class="add-remove-item">' +
        '<li class="add-qty-whishlist" data-codice="' + p.codice +'"><p>+</p></li>' +
        '<li class="subtract-qty-whishlist" data-codice="' + p.codice + '"><p>_</p></li>' +
      '</ul>' +
    '</div>' +
    '<div class="col-xs-3 col-sm-4">' +
      '<div class="btn-group" role="group">' +
        '<button data-modal="' + modalID + '" data-type="apparel" data-id="' + p.id + '" class="btn btn-sml open-apparel-details"><i class="fa fa-search-plus"></i> <span class="hidden-xs">Details</span></button> ' +
        '<button href="javascript:void(0)" class="btn btn-sml remove-whishlist" data-codice="' + p.codice + '"><i class="fa fa-trash"></i> <span class="hidden-xs">Remove</span></button>' +
      '</div>' +
    '</div>' +
  '</div>' +
  '<div class="modal fade" id="' + modalID + '" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">' +
    '<div class="modal-dialog modal-lg" role="document">' +
      '<div class="modal-content">' +
        '<div class="modal-header">' +
          '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
          '<h4 class="modal-title" id="myModalLabel">Product Details</h4>' +
        '</div>' +
        '<div class="modal-body"></div>' +
        '<div class="modal-footer">' +
          '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '</div>' +
      '</div>' +
    '</div>' +
  '</div>';

  $('.whislist-container').append(html);
}

function getWishlist() {
  $('.whislist-container').empty();
  $('.total-bar').hide();
  $('.whishlish-total-amount').empty();
  if (localStorage.ubi_whishlist) {
    var products = JSON.parse(localStorage.ubi_whishlist);
    var total = 0;

    if (products.length > 0) {
      _.each(products, function(p) {
        p.total_item = Math.round(parseFloat(p.prezzo)) * parseInt(p.qty);        
        if(p.type=='apparel'){
          renderApparel(p);
        }else{
          renderCatalogue(p);
        }
        total += p.total_item;
      });

      $('.total-bar').show();
      $('.whishlish-total-amount').append('€ ' + total);
    } else {
      var empty_wish = $('.active-lang').attr('data-lang') == 'en' ? 'No products added to Wishlist' : 'Non hai aggiunto prodotti in Whishlist';
      var html = '<div class="row wishlist-line table-wish"><div class="col-sm-12">' + empty_wish + '</div></div>';
      $('.whislist-container').append(html);
    }
  } else {
    var empty_wish = $('.active-lang').attr('data-lang') == 'en' ? 'No products added to Wishlist' : 'Non hai aggiunto prodotti in Whishlist';
    var html = '<div class="row table-wish"><div class="col-sm-12"><p>' + empty_wish + '</p></div></div>';
    $('.whislist-container').append(html);
  }
}

function renderSearchCatalogue(prodotti, keyword){
  var html = '';
  if (prodotti.length > 0) {
    for (var i = 0; i < prodotti.length; i++) {
      var img_cover,
        prodotto = prodotti[i],
        codiceTipologia,
        weight = 'n.d.',
        load = 'n.d.',
        load_value = 'n.d.',
        img_url = prodotto.image;

      if (prodotto.codice_tipologia == '2:1') codiceTipologia = '21';
      else if (prodotto.codice_tipologia == '3:1') codiceTipologia = '31';
      else if (prodotto.codice_tipologia == '2:1LF') codiceTipologia = '21LF';
      else codiceTipologia = prodotto.codice_tipologia;

      weight = prodotto.Peso;

      if (prodotto.MWL) load = prodotto.MWL;

      html = html.concat(
      '<div class="row text-center result-cont">' +
        '<div class="col-xs-3 col-sm-1 p-l-0">' +
          '<img src="' + img_url + '" height="55" alt="Cover Product - Ubi Maior" />' +
        '</div>' +
        '<div class="col-xs-4 col-sm-2 p-l-0 p-t-15 ">' + prodotto.Codice + '</div>' +
        '<div class="hidden-xs col-sm-2 p-l-0 p-t-15">' + prodotto.Linea + '</div>' +
        '<div class="hidden-xs col-sm-2 p-l-0 p-t-15">' + prodotto.Tipologia + '</div>' +
        '<div class="hidden-xs col-sm-1 p-l-0 p-t-15">' + weight + '</div>' +
        '<div class="hidden-xs col-sm-1 p-l-0 p-t-15">' + load + '</div>' +
        '<div class="col-xs-4 col-sm-2 p-l-0">' +
          '<a class="btn btn-bordered jib-but add-to-whishlist"' + 
            'data-show-price="' + prodotto.show_price + '"' + 
            'data-img-path="' + img_url + '"' +
            'data-id="' + prodotto.IdArticolo + '"'+ 
            'data-codice="' + prodotto.Codice + '"'+
            'data-cod-linea="' + prodotto.codice_linea + '"'+
            'data-cod-tipologia="' + prodotto.codice_tipologia + '"'+
            'data-price="' + prodotto.Prezzo + '"'+
            'data-description="' + prodotto.DescrizioneInglese +'"'+
            'data-weight="' + weight + '"'+
            'data-load="' + load + '"'+
            'data-load-value="' + load_value +'">' +
              '<span class="fa fa-plus"></span> Add' +
          '</a>' +
        '</div>' +
      '</div>');
    }
  } else {
    html = html.concat(
    '<div class="row">' +
      '<div class="col-sm-12">No results for ' + keyword + '</div>' +
    '</div>');
  }
  $('.cerca-whishlist-catalogue-results').append(html);
}

function renderSearchApparel(prodotti, keyword){
  var html = '';
  if (prodotti.length > 0) {
    for (var i = 0; i < prodotti.length; i++) {
      var prodotto = prodotti[i];
      html = html.concat(
      '<div class="row text-center result-cont">' +
        '<div class="col-xs-3 col-sm-1 p-l-0">' +
          '<img src="img/apparel/' + prodotto.cover + '" height="55" alt="Cover Product Apparel - Ubi Maior" />' +
        '</div>' +
        '<div class="col-xs-4 col-sm-3 p-l-0 p-t-15 ">' + prodotto.codice + '</div>' +
        '<div class="hidden-xs col-sm-2 p-l-0 p-t-15">' + prodotto.product_type + '</div>' +
        '<div class="hidden-xs col-sm-2 p-l-0 p-t-15">' + prodotto.taglia + '</div>' +
        '<div class="hidden-xs col-sm-2 p-l-0 p-t-15">€ ' + prodotto.prezzo + '</div>' +        
        '<div class="col-xs-4 col-sm-2 p-l-0">' +
          '<a class="btn btn-bordered jib-but add-to-whishlist"' + 
            'data-type="apparel"' +             
            'data-img="' + prodotto.cover + '"' +
            'data-id="' + prodotto.id + '"'+ 
            'data-codice="' + prodotto.codice + '"'+            
            'data-taglia="' + prodotto.taglia + '"'+
            'data-price="' + prodotto.prezzo + '"'+
            'data-description="' + prodotto.description +'">' +
              '<span class="fa fa-plus"></span> Add' +
          '</a>' +
        '</div>' +
      '</div>');
    }
  } else {
    html = html.concat(
    '<div class="row">' +
      '<div class="col-sm-12">No results for ' + keyword + '</div>' +
    '</div>');
  }
  $('.cerca-whishlist-apparel-results').append(html);
}

function addItemToWishlist (product){
  var products = [];
    
    if (!localStorage.getItem('ubi_whishlist')) {
      products.push(product);
      localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    } else {
      products = JSON.parse(localStorage.ubi_whishlist);
      if (!_.findWhere(products, { codice: product.codice })) {
        products.push(product);
        toastr.success('Product added to Wishlist', 'Success!', {
          showDuration: '300',
          hideDuration: '300',
          timeOut: '1000'
        });
      } else {
        _.each(products, function(p) {
          if (p.codice == product.codice) {
            p.qty = p.qty + 1;
          }
        });
        toastr.success('Wishlist product qty updated', 'Success!', {
          showDuration: '300',
          hideDuration: '300',
          timeOut: '1000'
        });
      }
      localStorage.removeItem('ubi_whishlist');
      localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    }
    getWishlist();
}



$(document).ready(function() {
  $(document).on('click touch', '.open-details', function() {
    var codice = $(this).attr('data-codice'),
      modalId = $(this).attr('data-modal');

    $.ajax({
      url: 'get-product-details.php',
      type: 'post',
      crossDomain: true,
      data: { codice: codice },
      dataType: 'html'
    }).done(function(res) {
      $('#' + modalId + ' .modal-body')
        .empty()
        .html(res);
      $('#' + modalId).modal('show');
    });
  });

  $(document).on('click touch', '.open-apparel-details', function() {
    var id = $(this).attr('data-id'),
      modalId = $(this).attr('data-modal');

    $.ajax({
      url: 'get-apparel-details.php',
      type: 'post',
      crossDomain: true,
      data: { id: id },
      dataType: 'html'
    }).done(function(res) {
      $('#' + modalId + ' .modal-body')
        .empty()
        .html(res);
      $('#' + modalId).modal('show');
    });
  });

  var menuLeft = document.getElementById('cbp-spmenu-s1'),
    showLeftPush = document.getElementById('showLeftPush'),
    closeLeftPush = document.getElementById('closeLeftPush'),
    body = document.body;

  showLeftPush.onclick = function() {
    classie.toggle(this, 'active');
    classie.toggle(body, 'cbp-spmenu-push-toright');
    classie.toggle(menuLeft, 'cbp-spmenu-open');
    $('.whishlist-search-trigger').focus();

    $('.search-left').hide();
  };

  closeLeftPush.onclick = function() {
    classie.toggle(showLeftPush, 'active');
    classie.toggle(body, 'cbp-spmenu-push-toright');
    classie.toggle(menuLeft, 'cbp-spmenu-open');
    $('.search-left').show();
  };

  getWishlist();

  var keyword = '';
  $(document).on('keyup','.whishlist-search-trigger',_.debounce(function() {
      keyword = $(this).val();
      if (keyword && keyword !== '' && keyword.length > 2) {
        $.ajax({
          url: 'search.php',
          type: 'POST',
          data: { keyword: keyword }
        }).done(function(res) {
          res = JSON.parse(res);          
          $('.whishlist-search-keyword').text(keyword);
          $('.cerca-whishlist-catalogue-results').empty();
          $('.cerca-whishlist-apparel-results').empty();

          renderSearchCatalogue(res.catalogo, keyword);
          renderSearchApparel(res.apparel, keyword);

        });
      } else {
        $('.cerca-whishlist-catalogue-results').empty();
        $('.cerca-whishlist-apparel-results').empty();
      }
    }, 500)
  );

  $(document).on('click touch', '.add-to-whishlist', function() {

    var type = $(this).attr('data-type');
    console.log(type);
    if(type=='apparel'){
      console.log('apparel-type');
      var product_code = $(this).data('codice'),
      product_id = $(this).data('id'),
      product_image = $(this).data('img'),      
      product_taglia = $(this).data('taglia'),
      product_price = $(this).data('price'),      
      product_description = $(this).data('description');

      var product = {
        id: product_id,
        product_image: product_image,
        codice: product_code,        
        prezzo: product_price,
        size: product_taglia,
        description: product_description,
        qty: 1,
        type: 'apparel'
      };

    }else{

      var product_code = $(this).data('codice'),
      product_id = $(this).data('id'),
      product_image = $(this).data('img-path'),
      product_cod_linea = $(this).data('cod-linea'),
      product_cod_tipologia = $(this).data('cod-tipologia'),
      product_price = $(this).data('price'),
      product_show_price = $(this).data('show-price'),
      product_description = $(this).data('description');
      product_weight = $(this).data('weight');
      product_load = $(this).data('load');
      product_load_value = $(this).data('load-value');

      var product = {
        id: product_id,
        product_image: product_image,
        codice: product_code,
        codice_linea: product_cod_linea,
        codice_tipologia: product_cod_tipologia,
        prezzo: product_price,
        mostra_prezzo: product_show_price,
        description: product_description,
        weight: product_weight,
        load: product_load,
        load_value: product_load_value,
        qty: 1
      };
    }

    addItemToWishlist(product);
    
  });

  $(document).on('click touch', '.remove-whishlist', function() {
    var codice = $(this).data('codice');
    var products = JSON.parse(localStorage.ubi_whishlist);
    var i = 0;
    _.each(products, function(p) {
      if (!p || p.codice == codice) {
        products.splice(i, 1);
      }
      i++;
    });
    localStorage.removeItem('ubi_whishlist');
    localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    toastr.success(
      'Wishlist product removed successfully from list',
      'Success!',
      {
        showDuration: '300',
        hideDuration: '300',
        timeOut: '1000'
      }
    );
    getWishlist();
  });

  $(document).on('click touch', '.add-qty-whishlist', function() {
    var codice = $(this).data('codice');
    var products = JSON.parse(localStorage.ubi_whishlist);
    _.each(products, function(p) {
      if (p.codice == codice) {
        p.qty = p.qty + 1;
      }
    });
    localStorage.removeItem('ubi_whishlist');
    localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    toastr.success('Wishlist product qty updated successfully', 'Success!', {
      showDuration: '300',
      hideDuration: '300',
      timeOut: '1000'
    });
    getWishlist();
  });

  $(document).on('click touch', '.subtract-qty-whishlist', function() {
    var codice = $(this).data('codice');
    var products = JSON.parse(localStorage.ubi_whishlist);
    var i = 0;
    _.each(products, function(p) {
      if (p && p.codice == codice) {
        if (p.qty - 1 == 0) {
          products.splice(i, 1);
          console.log(p, i, products);
          toastr.success(
            'Wishlist product removed successfully from list',
            'Success!',
            {
              showDuration: '300',
              hideDuration: '300',
              timeOut: '1000'
            }
          );
        } else {
          p.qty = p.qty - 1;
          toastr.success(
            'Wishlist product qty updated successfully',
            'Success!',
            {
              showDuration: '300',
              hideDuration: '300',
              timeOut: '1000'
            }
          );
        }
      }
      i++;
    });
    localStorage.removeItem('ubi_whishlist');
    localStorage.setItem('ubi_whishlist', JSON.stringify(products));
    getWishlist();
  });

  $(document).on('click', '.place-order', function() {
    $(this).toggleClass('open');
    if ($(this).hasClass('open')) {
      $(this)
        .empty()
        .html('<i class="fa fa-times"></i> Close');
    } else {
      $(this)
        .empty()
        .html('<i class="fa fa-paper-plane"></i> Send List');
    }
    $('.order-form').slideToggle();
  });

  $(document).on('click touch', '.submit-order', function() {
    $('.alert-error-form').hide();
    var fields = ['.order-name', '.order-surname', '.order-email', '.order-phone'],error = false;
    _.each(fields, function(f) {
      if (!$(f).val()) error = true;
    });

    if (error) {
      $('.alert-error-form .error-text').empty().html('Please <strong>fill all form inputs</strong> in order to send your order. Thank you.');
      $('.alert-error-form').show();
      setTimeout(function() {
        $('.alert-error-form').hide();
      }, 3500);
    } else {
      if (!checkemail($('.order-email').val())) {
        $('.alert-error-form .error-text').empty().html('Please enter a <strong>valid email</strong> in order to send your order. Thank you.');
        $('.alert-error-form').show();
        setTimeout(function() {
          $('.alert-error-form').hide();
        }, 3500);
      } else {
        $('.spinner-order').show();
        var products = JSON.parse(localStorage.ubi_whishlist);
        var order = {
          name: $('.order-name').val(),
          surname: $('.order-surname').val(),
          email: $('.order-email').val(),
          phone: $('.order-phone').val(),
          products: products
        };
        $.ajax({
          url: 'send-order.php',
          type: 'POST',
          data: order
        }).done(function(res) {
          var result = JSON.parse(res);
          if (result.response) {
            $('.alert-success-form .success-text').empty().html('Success! <strong>Your order has been sent successfully!</strong> A member of our staff will come back to you shortly. Thank you for writing us.');
            $('.alert-success-form').show();
            $('.order-name').val('');
            $('.order-surname').val('');
            $('.order-email').val('');
            $('.order-phone').val('');
            setTimeout(function() {
              $('.place-order').toggleClass('open');
              if ($('.place-order').hasClass('open')) {
                $('.place-order').empty().html('<i class="fa fa-times"></i> Close');
              } else {
                $('.place-order').empty().html('<i class="fa fa-paper-plane"></i> Send List');
              }
              $('.alert-success-form').hide();
              $('.order-form').slideToggle();
            }, 6000);
          } else {
            $('.alert-error-form .error-text').empty().html('Ops! <strong>An error occurred</strong> while sending your order. Please try again or contact us directly. Thank you.');
            $('.alert-error-form').show();
            setTimeout(function() {
              $('.alert-error-form').hide();
            }, 6000);
          }
          $('.spinner-order').hide();
        });
      }
    }
  });
});
