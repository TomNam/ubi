$(document).ready(function() {
    $(document).on('click', '.add-to-whishlist', function() {
      var products = [],
        product_code = $(this).data('codice'),
        product_id = $(this).data('id'),
        product_image = $(this).data('cover'),        
        product_size = $(this).data('taglia'),
        product_price = $(this).data('price'),        
        product_description = $(this).data('description');      
  
      var product = {
        id: product_id,
        product_image: product_image,
        codice: product_code,
        prezzo: product_price,
        description: product_description,
        size: product_size,
        type:'apparel',
        qty: 1        
      };
  
      if (!localStorage.getItem('ubi_whishlist')) {
        products.push(product);
        localStorage.setItem('ubi_whishlist', JSON.stringify(products));
      } else {
        products = JSON.parse(localStorage.ubi_whishlist);
        if (!_.findWhere(products, { codice: product_code, type: 'apparel' })) {
          products.push(product);
          toastr.success('Product added to Wishlist', 'Success!');
        } else {
          _.each(products, function(p) {
            if (p.codice == product_code) {
              p.qty = p.qty + 1;
            }
          });
          toastr.success('Wishlist product qty updated', 'Success!');
        }
        localStorage.removeItem('ubi_whishlist');
        localStorage.setItem('ubi_whishlist', JSON.stringify(products));
      }
    });
  });
  