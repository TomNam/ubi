function renderCatalogueItems(prodotti, keyword){
    var html ='';
    if(prodotti.length > 0){
        for(var i=0; i <prodotti.length; i++){
            var img_cover,
                prodotto = prodotti[i],
                codiceTipologia,
                weight = 'n.d.',
                load = 'n.d.',
                img_url = prodotto.image;
                
            if(prodotto.codice_tipologia=="2:1")   codiceTipologia = '21';
            else if(prodotto.codice_tipologia=="3:1")   codiceTipologia = '31';
            else if(prodotto.codice_tipologia=="2:1LF")   codiceTipologia = '21LF';            
            else codiceTipologia = prodotto.codice_tipologia;           

            weight = prodotto.Peso+' g';
            load = prodotto.MWL+' kg';                                                      

            html = html.concat(
                '<tr align="center" style="cursor:pointer;" onclick="window.location.href = \'index.php?p=catalog&codiceProdotto=' + prodotto.Codice + '\'">' +
                    '<td class="search-img-block"><img src="' + img_url + '" height="70" alt="Cover Product - Ubi Maior" onerror="this.src=\'img/image-not-found.png\'" /></td>' +
                    '<td class="search-code-block" valign="middle"><p>' + prodotto.Codice + '</p></td>' +
                    '<td class="hidden-xs" valign="middle">' + prodotto.Linea + '</td>' +
                    '<td class="hidden-xs" valign="middle">' + prodotto.Tipologia + '</td>' +
                    '<td class="hidden-xs" valign="middle">' + weight + '</td>' +
                    '<td class="hidden-xs" valign="middle">' + load + '</td>' +
                '</tr>'
            );
        }
    }else{
    html = html.concat(
        '<tr>' +
            '<td colspan="6">No results for ' + keyword + '</td>' +
        '</tr>'
        );
    }
    $(".prodotti-catalogue-results").append(html);
}

function renderApparelItems(prodotti, keyword){
    var html ='';
    if(prodotti.length > 0){
        for(var i=0; i <prodotti.length; i++){
            var prodotto = prodotti[i];
                
            html = html.concat(
                '<tr align="center" style="cursor:pointer;" onclick="window.location.href = \'index.php?p=apparel&id=' + prodotto.id + '\'">' +
                    '<td class="search-img-block"><img src="img/apparel/' + prodotto.cover + '" height="70" alt="Cover Product Apparel - Ubi Maior" onerror="this.src=\'img/image-not-found.png\'" /></td>' +
                    '<td class="search-code-block" valign="middle"><p>' + prodotto.codice + '</p></td>' +
                    '<td class="hidden-xs" valign="middle">' + prodotto.product_type + '</td>' +
                    '<td class="hidden-xs" valign="middle">' + prodotto.taglia + '</td>' +
                    '<td class="hidden-xs" valign="middle">€ ' + prodotto.prezzo + '</td>' +                    
                '</tr>'
            );
        }
    }else{
    html = html.concat(
        '<tr>' +
            '<td colspan="5">No results for ' + keyword + '</td>' +
        '</tr>'
        );
    }
    $(".prodotti-apparel-results").append(html);
}

$(document).ready(function(){
    var keyword = '';
    $(document).on('keyup','.inputSearch',_.debounce(function(){
        keyword = $(this).val();
        if(keyword && keyword !== '' && keyword.length>2){
            $.ajax({
                url: 'search.php',
                type: 'POST',
                data: { keyword: keyword }
            }).done(
                function(res){
                    res = JSON.parse(res);
                    console.log(res);                    
                    $(".searchResults").text(keyword);
                    $(".prodotti-catalogue-results").empty();
                    $(".prodotti-apparel-results").empty();
                    
                    renderCatalogueItems(res.catalogo,keyword);
                    renderApparelItems(res.apparel,keyword);

                }
            );
        }else{
            $(".prodotti-catalogue-results").empty();
            $(".prodotti-apparel-results").empty();
        }
    },500));
});