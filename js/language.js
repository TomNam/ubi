if(!Cookies.get('ubi_lang')){
    Cookies.set('ubi_lang', 'en', { expires: 365*10, domain: false });
    window.location.reload();
}

$(document).ready(function(){
    $(document).on('click','.set-lang',function(){
        var lang = $(this).data('lang');
        Cookies.remove('ubi_lang');
        Cookies.set('ubi_lang', lang, { expires: 365*10 });
        window.location.reload();
    });
});