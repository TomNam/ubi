<!-- rings -->
<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_TITLE']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_INTRO']?>
            </h5>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/category-accessories.png" alt="UbiMaior Italia - Sailing Accessories" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-acc">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <ul class="m-b-50 anchor-list">
            <li>
                <img src="images/cat-accessories/ring-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#rings"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/shackles-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#shackles"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/halyard-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#halyard"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/snatchblock-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#snatchblocks"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/crossover-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#crossover"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/padeye-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#padeye"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE']?></a>
            </li>
            <li>
                <img src="images/cat-accessories/organisers-anchor.png" alt="Ubimaior Italia - Accessories Rings">
                <a href="#organisers"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS']?></a>
            </li>
        </ul>            

        <a class="btn btn-lrg btn-bordered acc-but" href="index.php?p=catalog&idl=5">
            <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>            

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text-->
    <ul class="m-b-50 row anchor-list">
        <li class="col-xs-6">
            <img src="images/cat-accessories/ring-anchor.png" alt="Ubimaior Italia - Accessories Rings"><br>
            <a href="#rings"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/shackles-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#shackles"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/halyard-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#halyard"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/snatchblock-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#snatchblocks"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/crossover-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#crossover"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/padeye-anchor.png" alt="Ubimaior Italia - Accessories Rings"><br>
            <a href="#padeye"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE']?></a>
        </li>
        <li class="col-xs-6">
            <img src="images/cat-accessories/organisers-anchor.png" alt="Ubimaior Italia - Accessories Rings">
            <a href="#organisers"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS']?></a>
        </li>
    </ul>
    <a class="btn btn-lrg btn-bordered acc-but" href="index.php?p=catalog&idl=5">
        <i class="m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / mobile category-intro-text-->

<div class="container">
    <a id="rings"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-10">
            <h1 class="text-white m-b-30 m-t-50"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/ring1.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS_IMG1']?> (LS)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/ring2.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS_IMG1']?> (AB)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/ring3.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS_IMG3']?> (AN)</i></p>
        </div>
        <div class="col-sm-4 col-sm-offset-2 text-center">
            <img src="images/cat-accessories/ring4.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_RINGS_IMG4']?> (AB)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/ring-drawing.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?></i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=rings" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ rings -->

<div class="darker-divider"></div>

<!--/ shackle -->
<a id="shackles"></a>
<div class="container-fluid lighter-bg">
    <div class="row xs-p-r-0 xs-p-l-0 p-r-80 p-l-80 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-50">
        <div class="col-sm-10 col-sm-offset-2">
            <h1 class="text-white m-b-40 m-t-60"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-3 col-sm-offset-1 text-center">
            <img src="images/cat-accessories/shackle1.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Shackles">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES_IMG1']?> (Gd)</i></p>
        </div>
        <div class="col-sm-3 text-center">
            <img src="images/cat-accessories/shackle2.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Shackles">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SHACKLES_IMG2']?> (Ghr)</i></p>
        </div>
        <div class="col-sm-3 text-center">
            <img src="images/cat-accessories/shackle-drawing.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Shackles">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?></i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=shackles" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ shackle -->

<div class="darker-divider"></div>

<!-- Halyard block -->
<div class="container">
    <a id="halyard"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-10">
            <h1 class="text-white m-b-30 m-t-50"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-4 col-sm-offset-2 text-center">
            <img src="images/cat-accessories/halyard1.jpg" width="90%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_HALYARD_IMG1']?> (Pd)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/halyard-drawing.jpg" width="90%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?></i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=halyard_blocks" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ Halyard block -->

<div class="darker-divider"></div>

<!-- snatch blocks -->
<a id="snatchblocks"></a>
<div class="container-fluid lighter-bg">
    <div class="row xs-p-r-0 xs-p-l-0 p-r-80 p-l-80 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-50">
        <div class="col-sm-10 col-sm-offset-2">
            <h1 class="text-white m-b-40 m-t-60"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-4 col-sm-offset-2 text-center">
            <img src="images/cat-accessories/snatchblock2.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH_IMG1']?> (PA)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/snatchblock1.jpg" width="75%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH_IMG2']?> (PA) </i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=snatchbklocks" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ snatch blocks -->

<div class="darker-divider"></div>

<!-- Crossover -->
<div class="container">
    <a id="crossover"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-10">
            <h1 class="text-white m-b-30 m-t-50"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-4 col-sm-offset-2 text-center">
            <img src="images/cat-accessories/crossover1.jpg" width="90%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_CROSSOVER_IMG1']?> (CO)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/crossover-drawing.jpg" width="90%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?></i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=crossover" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ Crossover -->

<div class="darker-divider"></div>

<!-- Padeye -->
<a id="padeye"></a>
<div class="container-fluid lighter-bg">
    <div class="row xs-p-r-0 xs-p-l-0 p-r-80 p-l-80 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-50">
        <div class="col-sm-10 col-sm-offset-2">
            <h1 class="text-white m-b-40 m-t-60"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/padeye1.jpg" width="70%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE_IMG1']?> (HPD)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/padeye2.jpg" width="70%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE_IMG2']?> (SPD)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/padeye3.jpg" width="70%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_PADEYE_IMG3']?> (FPD)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/padeye-drawing1.jpg" width="70%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?> HPD</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/padeye-drawing2.jpg" width="70%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?> SPD</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/padeye-drawing3.jpg" width="70%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?> FPD</i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=padeye" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ padeye -->

<div class="darker-divider"></div>

<!-- Organisers -->
<div class="container">
    <a id="organisers"></a>
    <div class="row p-t-70 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 m-t-70">
        <div class="col-sm-10">
            <h1 class="text-white m-b-30 m-t-50"><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS']?></h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_ACC_SNATCH_ORGANISERS_TXT']?>
            </h5>
        </div>
    </div>
    <div class="container-fluid p-t-50 p-b-70"> 
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/organiser1.jpg" width="80%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS_IMG1']?> (ORM)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/organiser2.jpg" width="80%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS_IMG2']?> (ORF)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/organiser3.jpg" width="80%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS_IMG3']?> (ORF)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/organiser-drawing1.jpg" width="80%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?> <?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS_IMG1']?> (ORM)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/organiser-drawing2.jpg" width="80%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?> <?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS_IMG2']?> (ORF)</i></p>
        </div>
        <div class="col-sm-4 text-center">
            <img src="images/cat-accessories/organiser-drawing3.jpg" width="80%" alt="Ubi Maior Italia - Accessories - Rings">
            <p class="fs-15 text-white m-t-20"><i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_SCHEME']?> <?=$lang[$_COOKIE['ubi_lang']]['_ACC_ORGANISERS_IMG3']?> (ORF)</i></p>
        </div>
        <div class="col-sm-12 text-center m-t-60">
            <a href="index.php?p=catalog&idl=5&f_type=organisers" class="btn btn-bordered acc-but"><span class="fa fa-cog"></span> &nbsp; <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SEE_CATALOGUE']?></a>
        </div>
    </div>
</div>
<!--/ Organisers -->

<div class="darker-divider"></div>

<div class="product-carousel">
  <img src="images/cat-accessories/accessories-slider1.jpg" alt="Accessories - slideshow">
  <img src="images/cat-accessories/accessories-slider2.jpg" alt="Accessories - slideshow">
  <img src="images/cat-accessories/accessories-slider3.jpg" alt="Accessories - slideshow">
</div>

