<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-xs-12 text-center">
            <h3 class="importer-title">Ubi Maior Italia - CSV Importer Tool</h3>

            <?php
            if($_GET["import"] && $_GET["import"]=="ok"){
            ?>
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                <div class="alert alert-success alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <strong>Successo!</strong> Import Avvenuto Correttamente
                </div>
                </div>
            </div>
            <?php
            }
            ?>

            <?php
            if($_GET["import"] && $_GET["import"]=="ko"){
            ?>
            <div class="row">
                <div class="col-xs-12 col-sm-12 text-center">
                <div class="alert alert-danger alert-dismissible" role="alert">
                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <strong>Ops!</strong> Errore durante l'Import. Si prega di riprovare o di controllare i dati esportati.
                </div>
                </div>
            </div>
            <?php
            }
            ?>

            <div class="well text-left">
                <form action="import_csv.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Carica CSV Nomi Voci</label>
                        <input type="file" name="nomi_voci" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Carica CSV Righe Catalogo</label>
                        <input type="file" name="catalogo" class="form-control"/>
                    </div>
                    <button type="submit" class="btn btn-default btn-sml">Invia</button>
                </form>
            </div>
        </div>
    </div>
</div>