function openDescription(evt, description) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(description).style.display = "block";
    evt.currentTarget.className += " active";
}

$(function(){
    $( "#sortable" ).sortable({
        handle: ".handle",
        create: function(event, ui){
            var sortable = jQuery(this).sortable("toArray", {attribute: 'value'});
            jQuery("#value_sortable").attr('value',sortable);
        },
        update: function(event, ui){
            var sortable = jQuery(this).sortable("toArray", {attribute: 'value'});
            jQuery("#value_sortable").attr('value',sortable);
        }
    });
    $( "#sortable" ).disableSelection();

    $(document).on('click','.slider',function(){
        if($(".stock-slider").is(":checked")){
            $(".stock-slider").attr("checked",false).attr("value",0);
        }else{
            $(".stock-slider").attr("checked",true).attr("value",1);
        }
    });

});
