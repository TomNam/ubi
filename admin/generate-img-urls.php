<?php error_reporting(0);?>
<div class="container">
  <div class="row">
    <div class="col-sm-6 col-sm-offset-3 col-xs-12 text-center">
      <h3 class="importer-title">Ubi Maior Italia<br />Generatore Nomi Immagini Catalogo </h3>
      <div class="well text-left">
        <?php
        $prodotti = $mysqli->query("SELECT c.*,l.Linea, l.Sigla as codice_linea,t.Tipologia,t.Sigla as codice_tipologia FROM catalogo c LEFT JOIN linee l ON l.id=c.IdLInea LEFT JOIN tipologie t ON t.Id=c.IdTipologia");
        while($prodotto = $prodotti->fetch_object()){

            $nomi_voci = $mysqli->query("SELECT Voce01,Voce02,Voce03,Voce04,Voce05,Voce06,Voce07,Voce08 FROM nomi_voci WHERE lingua=2 AND IdTipologia='".$prodotto->IdTipologia."'");
            $nome_voce = $nomi_voci->fetch_object();

            if($prodotto->codice_tipologia=='2:1') $codice_tipologia = '21';
            elseif($prodotto->codice_tipologia=='3:1') $codice_tipologia = '31';
            elseif($prodotto->codice_tipologia=='2:1LF') $codice_tipologia = '21LF';            
            else $codice_tipologia = $prodotto->codice_tipologia;


            if($prodotto->Val05){
                $prodotto->voce05label = $nome_voce->Voce05;
            }

            if($prodotto->Val06){
                $prodotto->voce06label = $nome_voce->Voce06;
            }

            if($prodotto->Val07){
                if($nome_voce->Voce07=='WEIGHT' || $nome_voce->Voce07=='BL'){
                    $valori_peso07 = $mysqli->query("SELECT Voce07 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
                    $valore_peso07 = $valori_peso07->fetch_object();
                }
                $prodotto->voce07label = $nome_voce->Voce07;
                $prodotto->voce07unit = $valore_peso07->Voce07;
            }

            if($prodotto->Val08){
                if($nome_voce->Voce08=='WEIGHT'){
                    $valori_peso08 = $mysqli->query("SELECT Voce08 FROM nomi_voci WHERE lingua=3 AND IdTipologia='".$prodotto->IdTipologia."'");
                    $valore_peso08 = $valori_peso08->fetch_object();
                }
                $prodotto->voce08label = $nome_voce->Voce08;
                $prodotto->voce08unit = $valore_peso08->Voce08;
            }

            // INIZIALIZZAZIONE DEL NOME DELL'IMMAGINE
            $img_url = 'img/image-not-found.png';
            $prodotto->parsed_code = $prodotto->Codice;
            //FR PRODUCTS CODE NORMALIZATION
            $prodotto->parsed_code = str_replace("FR2:1","FR21",$prodotto->parsed_code);            
            $prodotto->parsed_code = str_replace("FR3:1","FR31",$prodotto->parsed_code);            

            // CODICE PER CONTROLLARE SE ESISTE UN FILE PER IL CODICE PRODOTTO SPECIFICO
            if(is_dir('../img/covers/'.$prodotto->codice_linea)){
                if(file_exists('../img/covers/'.$prodotto->codice_linea.'/'.$prodotto->parsed_code.'.png')){
                    $img_url = 'img/covers/'.$prodotto->codice_linea.'/'.$prodotto->parsed_code.'.png';
                }else{
                    $img_url = 'img/covers/'.$prodotto->codice_linea.'-'.$codice_tipologia.'.png';
                }
            }            

            // CODICE PER GESTIRE ECCEZIONI PER SPECIFICHE LINEE PRODOTTO
            if($prodotto->codice_tipologia=='PA'){
                $img_url = 'img/covers/PA.png';            
            }elseif($prodotto->codice_linea=='RT' && $img_url=='img/covers/RT-BOZ.png' && ($prodotto->codice_tipologia=='BOZ' || $prodotto->codice_tipologia=='BOZ-S')){
                if($prodotto->voce05label=='LOAD' && $prodotto->Val05){
                    if(file_exists('../img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val05.'.png'))
                        $img_url = 'img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val05.'.png';
                }elseif($prodotto->voce06label=='LOAD' && $prodotto->Val06){
                    if(file_exists('../img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val06.'.png'))
                        $img_url = 'img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val06.'.png';
                }elseif($img_url == 'img/image-not-found.png' && $prodotto->voce07label=='LOAD' && $prodotto->Val07){
                    if(file_exists('../img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val07.'.png'))
                        $img_url = 'img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val07.'.png';
                }elseif($img_url == 'img/image-not-found.png' && $prodotto->voce08label=='LOAD' && $prodotto->Val08){
                    if(file_exists('../img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val08.'.png'))
                        $img_url = 'img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'-'.$prodotto->Val08.'.png';
                }elseif($img_url == 'img/image-not-found.png'){
                    if(file_exists('../img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'.png'))
                        $img_url = 'img/covers/'.$prodotto->codice_linea.'-'.$prodotto->codice_tipologia.'.png';
                }
            }

            $query = "UPDATE catalogo SET image='$img_url' WHERE Codice='$prodotto->Codice'";
            if(!$mysqli->query($query)){
                die($mysqli->errno." - ".$mysqli->error.'<br />'.$query);
            }else{
                echo "<p>Prodotto Code: ".$prodotto->Codice.' - Immagine: '.$img_url.'</p>';
            }
        }
        ?>
      </div>
    </div>
  </div>
</div>
