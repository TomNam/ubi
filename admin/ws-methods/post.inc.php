<?php
/*
*   UBI MAIOR - WEB SERVICE
*   GET METHODS HANDLER
*   Author: Simone Landi <s.landi@namaqua.cc>
*   Created on 20/05/2018
*/

switch ($_REQUEST["a"]) {

	case 'save-priority':

    try{
        $mysqli->autocommit(FALSE);
        
        $priority = explode(",",$_POST["priority"]);
        $index = 1;                
        for($i=0;$i<count($priority);$i++){
            $id = $priority[$i];
            if(!$mysqli->query("UPDATE apparel_images SET priority=$index WHERE id=$id")){
                throw new Exception($mysqli->error);
            }
            $index++;
        }
        $mysqli->commit();
        redirect('index.php?p=apparel-form&id='.$_REQUEST['id_apparel'].'&msg=ok');
    }catch(Excpetion $e){
        $mysqli->rollback();
        $result = $e->getMessage();
        redirect('index.php?p=apparel-form&id='.$_REQUEST['id_apparel'].'&msg=ko');
    }
    
    $mysqli->autocommit(TRUE);
	break;
		
  case 'save-apparel':

    $target_dir = $_SERVER['DOCUMENT_ROOT']."/img/apparel";    
    $cover = $_FILES["cover"];
    $id = $mysqli->real_escape_string($_REQUEST['id']);
    $id_type = $mysqli->real_escape_string($_REQUEST['id_type']);
    $codice = $mysqli->real_escape_string($_REQUEST['codice']);
    $taglia = implode(',',$_REQUEST['taglie']);
    $prezzo = $mysqli->real_escape_string($_REQUEST['prezzo']);
    $is_stock = ($_REQUEST['is_stock']) ? 1 : 0;

    if(!$id){
        if(!$cover["tmp_name"]){
            // debug_array($cover);
            // die();
            redirect('index.php?p=apparel-form&id='.$_REQUEST['id_apparel'].'&msg=ko');
        }        
        $cover_array = explode('.', $_FILES['cover']['name']);
        $cover_ext = end($cover_array);
        if($cover_ext != "jpg" && $cover_ext != "jpeg" && $cover_ext != 'png') {
            echo 'Estensione errata: '.$cover_ext;
            die();
            // redirect('index.php?p=apparel-form&id='.$_REQUEST['id_apparel'].'&msg=ko');
        }
        $filename = time().'.'.$cover_ext;
        try{
            $uploaded = move_uploaded_file($cover["tmp_name"], $target_dir.'/'.$filename);
            if(!$uploaded){
                die("Not uploaded because of error #".$_FILES["cover"]["error"]);
            }
            $query = "INSERT INTO apparel(id_type,codice,taglia,prezzo,is_stock,cover) VALUES($id_type, '$codice', '$taglia', $prezzo, $is_stock,'$filename')";                        
        }catch(Excpetion $e){                        
            die('Errore upload: '.$e->getMessage());
        }
    }else{
        if($cover["tmp_name"]){
            $cover_array = explode('.', $_FILES['cover']['name']);
            $cover_ext = end($cover_array);
            if($cover_ext != "jpg" && $cover_ext != "jpeg" && $cover_ext != 'png') {
                echo 'Estensione errata: '.$cover_ext;
                die();
                // redirect('index.php?p=apparel-form&id='.$_REQUEST['id_apparel'].'&msg=ko');
            }
            $filename = time().'.'.$cover_ext;
            try{
                $catalogo = $mysqli->query("SELECT cover FROM apparel WHERE id=$id");
                $item = $catalogo->fetch_object();
                $uploaded = move_uploaded_file($cover["tmp_name"], $target_dir.'/'.$filename);
                if(!$uploaded){
                    die("Not uploaded because of error #".$_FILES["cover"]["error"]);
                }else{
                    unlink($target_dir.'/'.$item->cover);
                }      
            }catch(Excpetion $e){                        
                die('Errore upload: '.$e->getMessage());
            }                        
        }else{
            $filename = null;
        }
        
        $query = "UPDATE apparel SET codice='$codice', id_type=$id_type, taglia='$taglia', prezzo=$prezzo, is_stock=$is_stock";
        if($filename){ $query .= ", cover='$filename'"; }
        $query .= " WHERE id=$id";
    }

    if(!$mysqli->query($query)){
        die('Errore salvataggio apparel: '.$query.'<br />'.$mysqli->errno.' - '.$mysqli->error);
    }else{
        if(!$id){ $id = $mysqli->insert_id; }        

        $languages = $mysqli->query("SELECT * FROM languages WHERE is_active=1");
        while($language = $languages->fetch_object()){             
            $content = $mysqli->real_escape_string($_REQUEST['description_'.$language->slug]);            
            // echo $content.'<br />';
            if($content){                    
                $descrs = $mysqli->query("SELECT * FROM apparel_description WHERE id_apparel=$id AND id_lang=".$language->id);
                // echo 'N.items: '.$descrs->num_rows.'<br />';
                if($descrs->num_rows>0){ 
                    $descr = $descrs->fetch_object();                
                    $query_upd_descr = "UPDATE apparel_description SET content='$content' WHERE id=".$descr->id;
                    // echo $query_upd_descr.'<br />';
                    if(!$mysqli->query($query_upd_descr)){
                        die($query_upd_descr.'<br />'.$mysqli->errno.' - '.$mysqli->error);
                    }                                    
                }else{
                    $query_new_descr = "INSERT INTO apparel_description(id_apparel,id_lang,content) VALUES($id,".$language->id.",'$content')";
                    // echo $query_new_descr.'<br />';
                    if(!$mysqli->query($query_new_descr)){
                        die($query_upd_descr.'<br />'.$mysqli->errno.' - '.$mysqli->error);
                    }
                }
            }
        }
        redirect('index.php?p=apparel-form&id='.$id.'&msg=ok');
    }
		
	break;

	case 'save-product-image':
		$target_dir = $_SERVER['DOCUMENT_ROOT']."/img/apparel";
		$image = $_FILES["image"];
		$id_apparel = $_REQUEST['id_apparel'];
		//check file extensions
		$image_ext = end(explode('.', $_FILES['image']['name']));        

		if($image_ext != "jpg" && $image_ext != "jpeg" && $cover_ext != 'png') {
			redirect('index.php?p=apparel-form&id='.$id_apparel.'&msg=ko');
		}
        $filename = time().'.'.$image_ext;
        try{
            $uploaded = move_uploaded_file($image["tmp_name"], $target_dir.'/'.$filename);
            if(!$uploaded){
                die("Not uploaded because of error #".$_FILES["cover"]["error"]);
            }
            $query = "INSERT INTO apparel_images(id_apparel,name) VALUES($id_apparel, '$filename')";
			if(!$mysqli->query($query)){
                die('Errore salvataggio apparel image: '.$query.'<br />'.$mysqli->errno.' - '.$mysqli->error);
				// redirect('index.php?p=apparel-form&id='.$id_apparel.'&msg=ko');
			}else{
				redirect('index.php?p=apparel-form&id='.$id_apparel.'&msg=ok');
			}
        }catch(Excpetion $e){                        
            die('Errore upload: '.$e->getMessage());
        }   		
		break;
}
