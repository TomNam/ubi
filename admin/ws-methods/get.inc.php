<?php
/*
*   UBI MAIOR - WEB SERVICE
*   GET METHODS HANDLER
*   Author: Simone Landi <s.landi@namaqua.cc>
*   Created on 20/05/2018
*/

switch ($_REQUEST["a"]) {

	case 'generate-pass':
        echo hash('sha512',$_GET["pass"]);
        break;

    case 'delete-apparel':
        $target_dir = $_SERVER['DOCUMENT_ROOT']."/img/apparel";    
        $id = $_REQUEST["id"];        
        $immagini = $mysqli->query("SELECT * FROM apparel_images WHERE id=$id");
        while($immagine = $immagini->fetch_object()){        
            if(is_file($target_dir.'/'.$immagine->name)){
                unlink($target_dir.'/'.$immagine->name);
            }            
        }
        $delete_image_query = "DELETE FROM apparel_images WHERE id_apparel=$id";
        $mysqli->query($delete_image_query);
        
        $items = $mysqli->query("SELECT * FROM apparel WHERE id=$id");
        $item = $items->fetch_object();
        if(is_file($target_dir.'/'.$item->cover)){
            unlink($target_dir.'/'.$item->cover);
        }

        $delete_descr_query = "DELETE FROM apparel_description WHERE id_apparel=$id";
        $delete_apparel_query = "DELETE FROM apparel WHERE id=$id";
        
        if(!$mysqli->query($delete_descr_query) || !$mysqli->query($delete_apparel_query)){
            redirect("index.php?p=apparel-list&id=".$_REQUEST['id_apparel']."&msg=ko");
        }else{
            redirect("index.php?p=apparel-list&id=".$_REQUEST['id_apparel']."&msg=ok");
        }        
        
        break;

    case 'delete-apparel-image':
        $target_dir = $_SERVER['DOCUMENT_ROOT']."/img/apparel";    
        $id = $_REQUEST["id"];        
        $immagini = $mysqli->query("SELECT * FROM apparel_images WHERE id=$id");
        $immagine = $immagini->fetch_object();
        if(is_file($target_dir.'/'.$immagine->name)){
            unlink($target_dir.'/'.$immagine->name);
        }
        $query = "DELETE FROM apparel_images WHERE id=$id";
        if(!$mysqli->query($query)){
            redirect("index.php?p=apparel-form&id=".$_REQUEST['id_apparel']."&msg=ko");
        }else{
            redirect("index.php?p=apparel-form&id=".$_REQUEST['id_apparel']."&msg=ok");
        }        
        
        break;

}
