<?php error_reporting(0);
$id = $_REQUEST['id'];

if($id){
    $catalogo = $mysqli->query("SELECT * FROM apparel WHERE id=$id");
    $item = $catalogo->fetch_object();
    $item->taglie = explode(',',$item->taglia);
}

?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h3 class="importer-title">Catalogo Apparel - <?=($id) ? 'Modifica' : 'Aggiungi Nuovo'?> Prodotto</h3>
            <div class="well text-left">
                <a class="btn btn-default pull-right" href="index.php?p=apparel-list">Torna Indietro</a>
                <div class="spacer10"></div>
                <form action="ws.php" method="POST" enctype="multipart/form-data">
                    <fieldset>
                        <legend>Informazioni Prodotto:</legend>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>Codice</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">#</span>
                                        <input type="text" name="codice" class="form-control" placeholder="inserisci codice" value="<?=$item->codice?>" required />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>Tipologia</label>
                                    <select name="id_type" class="form-control" required>
                                        <option></option>
                                        <?php
                                        $types = $mysqli->query("SELECT * from apparel_type ORDER BY type ASC");
                                        while($type = $types->fetch_object()){
                                            $selected_option = ($type->id==$item->id_type) ? 'selected="selected"' : '';
                                            ?>
                                            <option value="<?=$type->id?>" <?=$selected_option?>><?=$type->type?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label>Prezzo</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">€</span>
                                        <input type="number" name="prezzo" class="form-control" placeholder="inserisci prezzo" value="<?=$item->prezzo?>" required />
                                    </div>                                    
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label><i class="glyphicon glyphicon-resize-full"></i> Taglia</label>
                                    <div class="spacer10"></div>
                                    <?php                                    
                                    $taglie = ['S','M','L','XL','XXL'];
                                    foreach ($taglie as $taglia) {
                                        $checked = in_array($taglia, $item->taglie) ? 'checked="checked"' : '';
                                        ?>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="taglie[]" value="<?=$taglia?>" <?=$checked?>> <?=$taglia?>
                                        </label>
                                        <?php
                                    }
                                    ?>                                                                        
                                    <div class="spacer10"></div>
                                </div>
                            </div> 
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label><i class="glyphicon glyphicon-picture"></i> Copertina</label>
                                    <input type="file" name="cover" class="form-control" <?=(!$id) ? 'required' : ''?> />
                                    <?php if($item->cover): ?>
                                        <div class="spacer5"></div>
                                        <img src="../img/apparel/<?=$item->cover?>" height="70" alt="cover prodotto - ubi maior italia" />
                                    <?php endif; ?>

                                </div>
                            </div> 
                            <div class="col-xs-12 col-sm-6">                                
                                <label><i class="glyphicon glyphicon-shopping-cart"></i> In Stock</label>
                                <div class="spacer1"></div>                                
                                <label class="switch">                                    
                                    <?php if($item->is_stock==0):?>
                                        <input type="checkbox" class="stock-slider" name="is_stock" value="<?=$item->is_stock?>">
                                        <span class="slider round"></span>
                                    <?php else: ?>
                                        <input type="checkbox" class="stock-slider" name="is_stock" value="<?=$item->is_stock?>" checked="checked">
                                        <span class="slider round"></span>
                                    <?php endif; ?>
                                </label>
                            </div>                          
                        </div>
                    </fieldset>
                    
                    <div class="spacer20"></div>

                    <fieldset>
                        <legend>Descrizione Prodotto</legend>
              
                        <?php
                        $langs = [];
                        $languages = $mysqli->query("SELECT * FROM languages WHERE is_active=1");
                        while($language = $languages->fetch_object()){ $langs[] = $language; }
                        ?>                        
                        
                        <div class="tab">
                            <?php 
                            $c=0; 
                            foreach ($langs as $lang) {
                                $class_tab = ($c===0) ? 'active' : '';
                                ?><a class="tablinks <?=$class_tab?>" onclick="openDescription(event, '<?=$lang->slug?>')"><?=$lang->name?></a><?php
                                $c++;
                            }
                            ?>                             
                        </div>
                        
                        <div class="tab-content">
                            <?php 
                            $c=0; 
                            foreach ($langs as $lang) {
                                $style_tab = ($c===0) ? 'display:block;' : '';
                                $descrs = $mysqli->query("SELECT content FROM apparel_description WHERE id_apparel=".$id." AND id_lang=".$lang->id);
                                if($descrs->num_rows>0){
                                    $descr = $descrs->fetch_object();
                                    $content = $descr->content;
                                }else{
                                    $content = '';
                                }
                                
                                ?>
                                <div id="<?=$lang->slug?>" class="tabcontent" style="<?=$style_tab?>">
                                    <label>Descrizione <?=ucfirst($lang->slug)?></label>
                                    <textarea name="<?='description_'.$lang->slug?>" rows="10" class="form-control" placeholder="inserisci descrizione prodotto <?=$lang->slug?>"><?=$content?></textarea>
                                </div>
                                <?php
                                $c++;
                            }
                            ?>
                        </div>                        
                    </fieldset>                    
                    <div class="spacer20"></div>
                    <input type="hidden" name="id" value="<?=$id?>" />
                    <input type="hidden" name="a" value="save-apparel" />
                    <button class="btn btn-info btn-lg center-block">Salva</button>                    
                </form>
                <div class="spacer40"></div>
                <?php if($id): ?>
                    <form class="form-inline" action="ws.php" method="POST" enctype="multipart/form-data">
                        <fieldset>
                            <legend>Immagini Prodotto</legend>                        
                                <div class="form-group">
                                    <label>Carica Immagine</label>
                                    <input type="file" name="image" class="form-control" required />
                                </div>                            
                                <input type="hidden" name="a" value="save-product-image" />
                                <input type="hidden" name="id_apparel" value="<?=$id?>" />
                                <button type="submit" class="btn btn-default">Aggiungi Immagine</button>                        
                        </fieldset>                    
                    </form>
                    <hr />
                    <ul id="sortable">
                        <?php                     
                        $immagini = $mysqli->query("SELECT * FROM apparel_images WHERE id_apparel=".$id." ORDER BY priority ASC"); 
                        if($immagini->num_rows===0){
                            ?>
                            <li>Non sono presenti immagini per questo prodotto</li>
                            <?php
                        }else{
                            while($immagine = $immagini->fetch_object()){
                                ?>
                                <li id="item_<?=$immagine->id?>" value="<?=$immagine->id?>" style="cursor:move">
                                    <i class="handle glyphicon glyphicon-move"></i>
                                    <img src="../img/apparel/<?=$immagine->name?>" height="70" alt="immagine prodotto" />
                                    <a class="btn btn-danger" onclick="return confirm('Cancellare questa immagine definitivamente?');" href="ws.php?a=delete-apparel-image&id_apparel=<?=$id?>&id=<?=$immagine->id?>">X elimina</a>
                                </li>
                                <?php
                            }
                        }                    
                        ?>                                        
                    </ul>
                    <div class="spacer10"></div>
                    <form action="ws.php" method="POST">
                        <input type="hidden" id="value_sortable" name="priority" />
                        <input type="hidden" name="id_apparel" value="<?=$id?>" />
                        <input type="hidden" name="a" value="save-priority" />
                        <button class="btn btn-primary  center-block">Salva Priorità</button>                        
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>