<?php error_reporting(0);?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h3 class="importer-title">Catalogo Apparel </h3>
            <div class="well text-left">
                <a class="btn btn-default pull-right" href="index.php?p=apparel-form">+ Aggiungi Prodotto</a>
                <div class="spacer20"></div>
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th width="100">Cover</th>
                            <th>Codice</th>
                            <th width="300">Opzioni</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $catalogo = $mysqli->query("SELECT * FROM apparel");
                        if($catalogo->num_rows === 0){
                            ?>
                            <tr>
                                <td colspan="3">Non sono presenti prodotti al monento</td>
                            </tr>
                            <?php
                        }else{
                            while($item = $catalogo->fetch_object()){
                                ?>
                                <tr>
                                    <td>
                                        <img src="../img/apparel/<?=$item->cover?>" width="100" alt="cover - prodotto apparel" />
                                    </td>
                                    <td>
                                        <b><?=$item->codice?></b>
                                    </td>
                                    <td>
                                        <a class="btn btn-default" href="index.php?p=apparel-form&id=<?=$item->id?>">Modifica</a>
                                        <a class="btn btn-danger" onclick="return confirm('Eliminare questo elemento definitivamente?');" href="ws.php?a=delete-apparel&id=<?=$item->id?>">Elimina</a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }                        
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>