<?php include("../lib/stdlib.php"); error_reporting(1);?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - Ubi Marior</title>

    <!-- Bootstrap -->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/admin.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="admin-login">

    <?php
    if(isset($_GET['auth']) && $_GET['auth'] && $_GET['auth']=="ko"){
      ?>
      <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-sm-7 col-sm-offset-3 text-center">
          <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <strong>Ops!</strong> Username o Password errate. Si prega di riprovare.
          </div>
        </div>
      </div>
      <?php
    }
    ?>

    <div class="spacer30"></div>
    <div class="row">
      <div class="col-xs-10 col-xs-offset-1 col-sm-4 col-sm-offset-4 text-center">
        <div class="spacer20"></div>
        <a href="index.php"><img src="../images/logo_ubi_white.png" alt="Ubi Major" class="logo"></a>
        <div class="spacer20"></div>
        <div class="well text-center">
          <form action="authentication.php" method="post" class="text-center">
            <h2 class="text-center form-signin-heading">Effettua il Login!</h2>            
            <div class="form-group text-left">
              <label>Username:</label>
              <input type="text" name="utente" class="form-control" placeholder="Username" required autofocus>
            </div>
            <div class="form-group text-left">
              <label>Password:</label>
              <input type="password" name="password" class="form-control" placeholder="Password" required>
            </div>
            <br />
            <input type="hidden" name="secure" value="<?=$_GET["secure"]?>" />
            <button class="btn btn-lg btn-success btn-block" type="submit">Log in</button>
          </form>
        </div>
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  </body>
</html>
