<div class="container m-t-100">
    <div class="row">
        <div class="col-xs-12 col-sm-12" style="">
            <div id="map" class="m-t-50" style="width:100%; min-height:600px"></div>
        </div>
    </div>
</div>


<script>
    function initMap() {
        var markerRed = 'images/marker-red.png';
        var markerWhite = 'images/marker-white.png';

        var Holland = '<div><img src="images/reseller-rake.png" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">RAKE RIGGING</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>1  Overleek 7 <br>1671 GD Medemblik<br>Holland<br>Ph. +31 653604628</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.rakerigging.nl</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>info@rakerigging.nl</td></tr></table></div></div>';
        
        var Italy = '<div><img src="images/reseller-ubi.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">UBI MAIOR ITALIA</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Via Serravalle 35-39 <br>50065 Molino del Piano<br>Italy<br>Ph. +39 0558364421</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.ubimaioritalia.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>info@ubimaioritalia.com</td></tr></table></div></div>';
        
        var England = '<div><img src="images/reseller-rigshop.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">THE RIG SHOP</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Unit 7, Saxon Wharf Lower York Street <br>SO14 5QF, Northam Southampton<br>UK<br>Ph. +44 (0)2380338341</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.rigshop.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>sales@rigshop.com</td></tr></table></div></div>';
        
        var England2 = '<div><img src="images/reseller-dsmgroup.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">DSM GROUP</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Riverside Works, Huddersfield Road<br>WF14 9DL, Mirfield, West Yorkshire<br>UK<br>Ph. +44 (0)1924497845</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.dsm-group.co.uk</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>sales@dsm-group.co.uk</td></tr></table></div></div>';
        
        var Spain = '<div><img src="images/reseller-evmmarine.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">EMV MARINE</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Moll de la Ribera local 13-14<br>08912, Badalona, Barcelona<br>Spain<br>Ph. +34 933207531</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.emvmarine.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>info@emvmarine.com</td></tr></table></div></div>';

        var France = '<div><img src="images/reseller-gremco.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">GREMCO SAS</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>1955 Chemin de Saint-Bernard<br>| 06220 Vallauris<br>France<br>Ph. +44(0)1924497845<br>Ph. +33 493641919</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.gremco-fr.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>gremco@gremco-sas.com</td></tr></table></div></div>';
        
        var Australia = '<div><img src="images/reseller-myyacht.png" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">MY YACHT</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Kirribilli Marina<br> Unit 14-1 Bradly Ave <br> Milsons Point <br> NSW 2061 <br> Australia</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.myyacht.com.au</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>parts@myyacht.com.au</td></tr></table></div></div>';
        
        var Hungary = '<div><img src="images/reseller-csepregi.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">CSEPREGI VIZISPORT</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>2 Weiss Manfréd út <br> 1211, Budapest <br> Hungary <br>Ph. +36 12764873</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.csepregi-vizisport.hu</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>info@csepregi-vizisport.hu</td></tr></table></div></div>';
        
        var Usa = '<div><img src="images/reseller-euromarine.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">EURO MARINE TRADING INC</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>62 Halsey Street, Unit M <br> 02840, Newport, RI <br> Usa<br>Ph. +001 401-849-0060</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.euromarinetrading.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>info@euromarinetrading.com</td></tr></table></div></div>';
        
        var Greece = '<div><img src="images/reseller-atlantamarine.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">ATLANTA MARINE</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Iosifidou 3, Elliniko Argiroupoli <br> 16777, Athens <br> Greece<br>Ph. +30 2109910722</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.atalantamarine.com</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>info@atalantamarine.com</td></tr></table></div></div>';

        var Germany = '<div><img src="images/reseller-lindemann.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">LINDEMANN</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Wendenstr. 455 <br>20537 Hamburg<br> Germany <br>Ph. +49 (0)40211197 - 12</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.lindemann-kg.de</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>verkauf@lindemann-kg.de</td></tr></table></div></div>';

        var Finland = '<div><img src="images/reseller-ropex.jpg" alt="" /></div><div class="panel border-0"><div class="panel-heading"><div class="panel-title">ROPEX</div></div><div class="panel-body"><table class="infoWindowTable"><tr><td class="text-center"><i class="fa fa-map-marker"></i></td><td>Turku, 20200 <br> Finland <br>Ph. +358 (0)504064 689</td></tr><tr><td class="text-center"><i class="fa fa-globe"></i></td><td>www.ropex.fi</td></tr><tr><td class="text-center"><i class="fa fa-envelope"></i></td><td>myynti@ropex.fi</td></tr></table></div></div>';
        
        var locations = [
            [Italy, 43, 12, 1, markerRed],
            [England, 52.35, -1.17, 2, markerWhite],
            [England2, 55.35, -1.17, 3, markerWhite],
            [Spain, 40.47, -3.7, 4, markerWhite],
            [France, 47.49, 2.7, 5, markerWhite],
            [Hungary, 47.46, 19.17, 6, markerWhite],
            [Holland, 52.12, 5.3, 7, markerWhite],
            [Australia, -26, 135, 8, markerWhite],
            [Usa, 41.5, -71.3, 9, markerWhite],
            [Greece, 37.9, 23.7, 10, markerWhite],
            [Germany, 53.54, 10.05, 11, markerWhite],
            [Finland, 60.44, 22.20, 12, markerWhite]
        ];

        // Create a map object and specify the DOM element for display.
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 23,
                lng: 63
            },

            scrollwheel: false,
            mapTypeControl: false,
            streetViewControl: false,
            zoom: 2,
            backgroundColor: 'none',
            styles: [{
                "featureType": "all",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#000000"
                }]
            }, {
                "featureType": "all",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "all",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "gamma": 0.01
                }, {
                    "lightness": 20
                }]
            }, {
                "featureType": "all",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "saturation": -31
                }, {
                    "lightness": -33
                }, {
                    "weight": 2
                }, {
                    "gamma": 0.8
                }]
            }, {
                "featureType": "all",
                "elementType": "labels.icon",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "administrative.country",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "simplified"
                }, {
                    "color": "#543f3f"
                }, {
                    "saturation": "54"
                }]
            }, {
                "featureType": "administrative.land_parcel",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 30
                }, {
                    "saturation": 30
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "all",
                "stylers": [{
                    "color": "#4c4c51"
                }, {
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "geometry",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "all",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#4c4c51"
                }]
            }, {
                "featureType": "landscape.natural.landcover",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [{
                    "saturation": 20
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 20
                }, {
                    "saturation": -20
                }]
            }, {
                "featureType": "road",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "lightness": 10
                }, {
                    "saturation": -30
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "saturation": 25
                }, {
                    "lightness": 25
                }]
            }, {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "all",
                "stylers": [{
                    "lightness": -20
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#212127"
                }]
            }]
        });


        var infowindow = new google.maps.InfoWindow(
            //  content: contentString
        );

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                icon: locations[i][4]
            });

            google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }
    }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA6cuailXQ_7tu-cpJrUTnIqXyUuBpCuB8&callback=initMap" async defer></script>
