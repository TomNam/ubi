<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">FR AVVOLGITORI</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_FR_INTRO']?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSER']?> </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="110" class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET1']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET1_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET2']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET2_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET3']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET3_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET4']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_FR_DET4_TXT']?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/cat-fr/fr-first.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-fr">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_FR_IMG_TXT']?>
        </p>
        <a class="various fancybox.iframe btn btn-lg btn-bordered fr-but m-r-20" href="https://www.youtube.com/embed/TCZdkqG04ps">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_VIDEO']?>
        </a>                       
        <a class="btn btn-bordered fr-but" href="index.php?p=catalog&idl=1">
            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>            

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text-->
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_FR_IMG_TXT']?>
    </p>
    <a class="various fancybox.iframe btn btn-lg btn-bordered fr-but m-r-20" href="https://www.youtube.com/embed/TCZdkqG04ps">
        <?=$lang[$_COOKIE['ubi_lang']]['_ALL_VIDEO']?>
    </a>                       
    <a class="btn btn-bordered fr-but" href="index.php?p=catalog&idl=1">
        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / mobile category-intro-text-->


<!-------------------- category-options -------------------->
<div class="container-fluid p-b-20"> 
    <div class="col-sm-6 p-l-40 m-t-40 bordered-right text-center">
        <h2 class="text-white m-t-40">FR CLASSIC</h2>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH1']?>
        </p>
        <img src="images/cat-fr/fr-150.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_ROLLER']?> <span class="bold">87-250mm </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_FURLING']?><span class="bold"> 6-10mm </span> <br>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_PIN']?> <span class="bold">6-16 mm</span> 
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?>  <span class="bold">500-13000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?> <span class="bold">1000-26000 Kg</span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-boat-size.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_BOAT']?>  <span class="bold">18-100 ft</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=1&f_type=CLASSIC" class="btn btn-bordered  btn-lrg fr-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

    <div class="col-sm-6 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-40">FR FREE TACK</h2>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH2']?>
        </p>
        <img src="images/cat-fr/fr-150m.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_ROLLER']?> <span class="bold">100-250mm </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_FURLING']?><span class="bold"> 8-10mm </span> <br>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_PIN']?> <span class="bold">8-16 mm</span> 
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?>  <span class="bold">1500-13000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?> <span class="bold">3000-26000 Kg</span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-boat-size.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_BOAT']?>  <span class="bold">26-100 ft</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=1&f_type=M" class="btn btn-bordered  btn-lrg fr-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>
</div>

<hr style="border-top:none;border-bottom:1px dashed #333;"></hr>

<div class="container-fluid p-b-30"> 
    <div class="col-sm-6 p-l-40 m-t-20 bordered-right text-center">
        <h2 class="text-white m-t-40">FR REWIND</h2>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH3']?>
        </p>
        <img src="images/cat-fr/fr-150rw.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_ROLLER']?> <span class="bold">100-250mm </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_FURLING']?><span class="bold"> 8-10mm </span> <br>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_PIN']?> <span class="bold">8-16 mm</span> 
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?>  <span class="bold">1500-13000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?> <span class="bold">2600-26000 Kg</span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-boat-size.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_BOAT']?>  <span class="bold">26-100 ft</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=1&f_type=RW" class="btn btn-bordered  btn-lrg fr-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

    <div class="col-sm-6 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-20">FR REWIND FREE TACK</h2>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH4']?>
        </p>
        <img src="images/cat-fr/fr-150rwm.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_ROLLER']?> <span class="bold">100-250mm </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_FURLING']?><span class="bold"> 8-10mm </span> <br>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_PIN']?> <span class="bold">8-16 mm</span> 
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?>  <span class="bold">1500-13000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?> <span class="bold">3000-26000 Kg</span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-boat-size.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_FR_TECH_BOAT']?>  <span class="bold">26-100 ft</span> <br>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=1&f_type=RWM" class="btn btn-bordered btn-lrg fr-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>
</div><!-- / category options-->
<hr style="border-top:none;border-bottom:1px dashed #333;"></hr>
<!-------------------- accessories -------------------->
<div class="container-fluid p-b-80">
    <h2 class="text-white m-t-30 m-b-30 text-center"><?=$lang[$_COOKIE['ubi_lang']]['_FR_ACCESSORIES']?></h2>
    <div class="col-sm-12 text-center" style="margin:0 0 15px;">
        <img src="images/cat-fr/fr-accessori01.jpg" alt="FR Avvolgitori - accessories" class="img-responsive">
    </div>
    <div class="col-sm-12 text-center">
        <img src="images/cat-fr/fr-accessori02.jpg" alt="FR Avvolgitori - accessories" class="img-responsive">
    </div>
</div><!-- / accessories -->
<!-------------------- photo & drawing -------------------->
<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix p-b-50" style="padding-right:0;padding-left:0;">
        <div class="row equal p-t-50">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-fr/fr-drawing.jpg">
                    <img src="images/cat-fr/fr-drawing.png" alt="FR Avvolgitori" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-fr/fr-drawing-photo.jpg">
                    <img src="images/cat-fr/fr-drawing-photo.jpg" alt="FR Avvolgitori" class="img-responsive"> 
                </a>
            </div>
        </div>
        <div class="row equal p-t-50">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-fr/fr-drawing-photo2.jpg">
                    <img src="images/cat-fr/fr-drawing-photo2.jpg" alt="FR Avvolgitori" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-fr/fr-drawing2.jpg">
                    <img src="images/cat-fr/fr-drawing2.png" alt="FR Avvolgitori" class="img-responsive"> 
                </a>
            </div>
        </div>
        <div class="row equal p-t-50">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-fr/fr-drawing3.jpg">
                    <img src="images/cat-fr/fr-drawing3.png" alt="FR Avvolgitori" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-fr/fr-drawing-photo3.jpg">
                    <img src="images/cat-fr/fr-drawing-photo3.jpg" alt="FR Avvolgitori" class="img-responsive"> 
                </a>
            </div>
        </div>
    </div>
</div><!-- / photo & drawing -->
<!-------------------- big drawing -------------------->
<div class="container-fluid p-b-50">
    <div class="col-sm-12 text-center">
        <img src="images/cat-fr/fr-tech2.jpg" alt="FR Avvolgitori - technical details" class="img-responsive">
    </div>
</div><!-- / big drawing -->
<!-------------------- carousel -------------------->
<div class="product-carousel">
  <img src="images/cat-fr/fr-slider1.jpg" alt="FR Avvolgitori slideshow">
  <img src="images/cat-fr/fr-slider2.jpg" alt="FR Avvolgitori slideshow">
  <img src="images/cat-fr/fr-slider3.jpg" alt="FR Avvolgitori slideshow">
  <img src="images/cat-fr/fr-slider4.jpg" alt="FR Avvolgitori slideshow">
</div><!-- / carousel -->

<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-fr/fr-footer.png" alt="" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=36" target="_blank" class="btn btn-bordered btn-lrg fr-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_FR_CATALOGUE']?></a>
  </div>
</div>
</div>


