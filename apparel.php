<?php include("./lib/dbconn.php"); $id = $_REQUEST["id"];?>
<div class="container-fluid p-t-80">
    <div class="row">        
        <div class="col-sm-12 col-md-3 category-left">
            <?php include('./apparel/sidebar.php'); ?>
        </div><!-- / category left -->        
        <div class="col-sm-12 col-md-9 products-container">     

            <!-- Quick links to other lines -->
            <div class="row text-center">
                <div class="dropup-cont">
                    <div class="dropup quick-links-catalog-mobile">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Other lines
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="index.php?p=catalog&idl=4">Yatch Club</a></li>
                            <li><a href="index.php?p=catalog&idl=3">Regata</a></li>
                            <li><a href="index.php?p=catalog&idl=2">X3M</a></li>
                            <li><a href="index.php?p=catalog&idl=8"><li>Jiber</a></li>
                            <li><a href="index.php?p=catalog&idl=1"><?=($_COOKIE['ubi_lang']=='it') ? 'Avvolgitori' : 'Furler'?></a></li>
                            <li><a href="index.php?p=catalog&idl=5">Accessories</a></li>
                            <li><a href="index.php?p=apparel">Apparel</a></li>
                        </ul>
                    </div>
                </div>

                <div class="text-center">
                    <ul class="quick-links-catalog">
                        <a href="index.php?p=catalog&idl=4"><li>Yatch Club</li></a>
                        <a href="index.php?p=catalog&idl=3"><li>Regata</li></a>
                        <a href="index.php?p=catalog&idl=2"><li>X3M</li></a>
                        <a href="index.php?p=catalog&idl=8"><li>Jiber</li></a>
                        <a href="index.php?p=catalog&idl=1"><li><?=($_COOKIE['ubi_lang']=='it') ? 'Avvolgitori' : 'Furler'?></li></a>
                        <a href="index.php?p=catalog&idl=5"><li>Accessories</li></a>
                        <a href="index.php?p=apparel"><li>Apparel</li></a>
                    </ul>
                </div>
            </div>
            <!-- / Quick links to other lines -->     
            <div class="row text-center">
                <div class="col-xs-12">
                    <?php if($id): ?>
                        <h4 class="text-center"><?=($_COOKIE['ubi_lang'] === 'it') ? 'Dettagli Prodotto' : 'Product Details'?></h4>
                    <?php else: ?>
                        <h4 class="text-center"><?=($_COOKIE['ubi_lang'] === 'it') ? 'Lista Prodotti' : 'Products List'?></h4>
                    <?php endif; ?>
                </div>
            </div>

            <div class="row" style="display: table;width: 100%;">
                <?php
                
                
                if($id){
                    include('./apparel/detail.php');
                }else{
                    include('./apparel/list.php');
                }
                ?>
            </div>
        </div>
    </div>
</div>
