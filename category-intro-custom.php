<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">CUSTOM</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_CUST_INTRO']?>
            </h5>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/cat-custom/custom-first.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-custom">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_CUST_IMG_TXT']?>
        </p>                 
        <a class="btn btn-bordered cust-but" href="pdf/UbiMaiorItalia_2017_Catalog_eng.pdf#page=62" target="_blank">
            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_CATALOGUE']?>
        </a>            

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text-->
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_CUST_IMG_TXT']?>
    </p>                 
    <a class="btn btn-bordered cust-but" href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=62" target="_blank">
        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_CATALOGUE']?>
    </a>            

</div><!-- / mobile category-intro-text-->

<!-------------------- custom-options -------------------->
<div class="container"> 
    <style>
        .cust-options-cont img{margin:20px 10px;border:1px solid #ccc;}
        .cust-options-cont{padding:40px 0;}
        .cust-options-cont h3{color:#fff;}
    </style>
    
    <div class="row cust-options-cont text-center">
        <h3><?=$lang[$_COOKIE['ubi_lang']]['_CUST_ONEOFF']?></h3><br>
        <div class="col-sm-4">
            <img src="images/cat-custom/one-off1.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-4">
            <img src="images/cat-custom/one-off2.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-4">
            <img src="images/cat-custom/one-off3.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-6 text-right">
            <img src="images/cat-custom/one-off4.jpg" width="50%" alt="">
        </div>
        <div class="col-sm-6 text-left">
            <img src="images/cat-custom/one-off5.jpg" width="50%" alt="">
        </div>
    </div>

    <div class="darker-divider"></div>

    <div class="row cust-options-cont text-center">
        <h3><?=$lang[$_COOKIE['ubi_lang']]['_CUST_CONTRACT']?></h3><br>
        <div class="col-sm-4">
            <img src="images/cat-custom/contract1.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-4">
            <img src="images/cat-custom/contract2.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-4">
            <img src="images/cat-custom/contract3.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-6 text-right">
            <img src="images/cat-custom/contract4.jpg" width="50%" alt="">
        </div>
        <div class="col-sm-6 text-left">
            <img src="images/cat-custom/contract5.jpg" width="50%" alt="">
        </div>
    </div>

    <div class="darker-divider"></div>
    
    <div class="row cust-options-cont text-center">
        <h3><?=$lang[$_COOKIE['ubi_lang']]['_CUST_PERSONAL']?></h3><br>
        <div class="col-sm-4">
            <img src="images/cat-custom/personal1.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-4">
            <img src="images/cat-custom/personal2.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-4">
            <img src="images/cat-custom/personal3.jpg" width="80%" alt="">
        </div>
        <div class="col-sm-6 text-right">
            <img src="images/cat-custom/personal4.jpg" width="50%" alt="">
        </div>
        <div class="col-sm-6 text-left">
            <img src="images/cat-custom/personal5.jpg" width="50%" alt="">
        </div>
    </div>

</div><!-- / custom options-->

<!-------------------- carousel -------------------->
<div class="product-carousel">
  <img src="images/cat-custom/cust-slider1.jpg" alt="FR Avvolgitori slideshow">
  <img src="images/cat-custom/cust-slider2.jpg" alt="FR Avvolgitori slideshow">
  <img src="images/cat-custom/cust-slider3.jpg" alt="FR Avvolgitori slideshow">
  <img src="images/cat-custom/cust-slider4.jpg" alt="FR Avvolgitori slideshow">
</div><!-- / carousel -->

<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-fr/fr-footer.png" alt="" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2017_Catalog_eng.pdf#page=62" target="_blank" class="btn btn-bordered sail-but m-t-50"> <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_CUST_CATALOGUE']?> </a>
  </div>
</div>
</div>


