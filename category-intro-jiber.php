<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">JB JIBER</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_JB_INTRO']?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSER']?> </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET1']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET1_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET2']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET2_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET3']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET3_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET4']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET4_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET5']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_JB_DET5_TXT']?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/category-jiber.png" alt="Jiber" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-jiber">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_JB_IMG_TXT']?>
        </p>
        <a class="various fancybox.iframe btn btn-lg btn-bordered jib-but m-r-20" href="https://www.youtube.com/embed/QLoQcrERZyU">
            <?=$lang[$_COOKIE['ubi_lang']]['_ALL_VIDEO']?>
        </a>            
        <a class="btn btn-lg btn-bordered jib-but" href="index.php?p=catalog&idl=8">
            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>            

    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text -->
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_JB_IMG_TXT']?>
    </p>
    <a class="various fancybox.iframe btn btn-lg btn-bordered jib-but m-r-20" href="https://www.youtube.com/embed/QLoQcrERZyU">
        <?=$lang[$_COOKIE['ubi_lang']]['_ALL_VIDEO']?>
    </a>            
    <a class="btn btn-lg btn-bordered jib-but" href="index.php?p=catalog&idl=8">
        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / mobile category-intro-text-->

<div class="container-fluid p-b-50"> 
    <div class="col-sm-6 p-l-40 m-t-40 bordered-right text-center">
        <h2 class="text-white m-t-40">JIBER</h2>
        <h4 class="text-white">(<?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH1']?>)</h4>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH1_1']?>
        </p>
        <img src="images/cat-jiber/jiber-rod.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH1_2']?> <span class="bold">30-37ft / 37-45 ft / 45-52ft </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH1_3']?> <span class="bold">15-40 ft </span>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?>  <span class="bold">6500-17000 Kg</span>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=8&f_type=JB" class="btn btn-bordered jib-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

    <div class="col-sm-6 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-40">JIBER TX</h2>
        <h4 class="text-white">(<?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH2']?>)</h4>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH2_1']?>
        </p>
        <img src="images/cat-jiber/jiber-tx.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH2_2']?> <span class="bold">30-37ft / 37-45 ft / 45-52ft </span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?>  <span class="bold">6500-50000 Kg</span>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=8&f_type=JB-TX" class="btn btn-bordered jib-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>
</div>

<div class="darker-divider"></div>

<div class="container-fluid p-b-50"> 
    <div class="row text-center">
        <h2 class="text-white m-t-50 m-b-50"><?=$lang[$_COOKIE['ubi_lang']]['_JB_CHOOSE']?></h2>
        <img src="images/cat-jiber/tamburi.png" width="100%">
    </div>
    <div class="row m-t-50 m-b-50">
        <div class="col-sm-5 col-sm-offset-1">
            <img src="images/cat-jiber/jiber-tx-esploso.png" alt="Jiber-TX" class="img-responsive">
        </div>
        <div class="col-sm-5">
            <img src="images/cat-jiber/jiber-tx-one.png" alt="Jiber-TX" class="img-responsive"> 
        </div>
    </div>
    <div class="row text-center">
        <img src="images/cat-jiber/jiber-sequenza.jpg" width="100%">
    </div>
    <div class="row text-center">
        <h2 class="text-white m-t-50 m-b-50"><?=$lang[$_COOKIE['ubi_lang']]['_JB_WHY_JB']?></h2>
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-2 padding-30"><img src="images/cat-jiber/why1.jpg" alt="Jiber-TX" class="img-responsive"></div>
                <div class="col-sm-4 padding-30 text-left text-fff"><?=$lang[$_COOKIE['ubi_lang']]['_JB_WHY_JB1']?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-2 padding-30"><img src="images/cat-jiber/why2.jpg" alt="Jiber-TX" class="img-responsive"></div>
                <div class="col-sm-4 padding-30 text-fff text-left"><?=$lang[$_COOKIE['ubi_lang']]['_JB_WHY_JB2']?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-2 padding-30"><img src="images/cat-jiber/why3.jpg" alt="Jiber-TX" class="img-responsive"></div>
                <div class="col-sm-4 padding-30 text-fff text-left"><?=$lang[$_COOKIE['ubi_lang']]['_JB_WHY_JB3']?></div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-2 padding-30"><img src="images/cat-jiber/why4.jpg" alt="Jiber-TX" class="img-responsive"></div>
                <div class="col-sm-4 padding-30 text-fff text-left"><?=$lang[$_COOKIE['ubi_lang']]['_JB_WHY_JB4']?></div>
            </div>
        </div>
        <div class="col-sm-6">
            <img src="images/cat-jiber/jiber-tx-tech2.png" alt="JIBER-TX technical details" class="img-tech2"> 
        </div>
    </div>
</div>

<div class="darker-divider"></div>

<div class="container-fluid p-b-50">
    <div class="col-sm-6 text-center">
        <img src="images/jiber-tx/jiber-tx-tech.png" alt="JIBER-TX technical details" class="img-responsive">
    </div>
    <div class="col-sm-6 p-l-40">
        <h2 class="text-white"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_TECH_DET']?></h2>
        <div class="row m-t-40">
            <div class="col-sm-2"></div>
            <div class="col-sm-4 text-fff text-center"><?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH_DET_CLASSIC']?></div>
            <div class="col-sm-4 text-fff text-center"><?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH_DET_JB']?></div>
        </div>
        <div class="row m-t-10">
            <div class="col-sm-2 text-fff p-t-40"><?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH_DET_STATIC']?></div>
            <div class="col-sm-4"><img src="images/cat-jiber/h-path2.jpg" class="img-responsive"></div>
            <div class="col-sm-4"><img src="images/cat-jiber/jib-path2.jpg" class="img-responsive"></div>
        </div>
        <div class="row m-t-10">
            <div class="col-sm-2 text-fff p-t-40"><?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH_DET_DYNAMIC']?></div>
            <div class="col-sm-4"><img src="images/cat-jiber/h-pres.jpg" class="img-responsive"></div>
            <div class="col-sm-4"><img src="images/cat-jiber/jib-pres.jpg" class="img-responsive"></div>
        </div>
        <div class="row m-t-10">
            <div class="col-sm-2 text-fff p-t-40"><?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH_DET_TURBULENT']?></div>
            <div class="col-sm-4"><img src="images/cat-jiber/h-turbulent.jpg" class="img-responsive"></div>
            <div class="col-sm-4"><img src="images/cat-jiber/jib-turbulent.jpg" class="img-responsive"></div>
        </div>
        <div class="row m-t-10">
            <div class="col-sm-2 text-fff p-t-40"><?=$lang[$_COOKIE['ubi_lang']]['_JB_TECH_DET_VELOCITY']?></div>
            <div class="col-sm-4"><img src="images/cat-jiber/h-vel.jpg" class="img-responsive"></div>
            <div class="col-sm-4"><img src="images/cat-jiber/jib-vel.jpg" class="img-responsive"></div>
        </div>
    </div>
</div>

<div class="product-carousel">
  <img src="images/cat-jiber/jiber-gallery1.jpg" alt="JIBER-TX slideshow">
  <img src="images/cat-jiber/jiber-gallery2.jpg" alt="JIBER-TX slideshow">
  <img src="images/cat-jiber/jiber-gallery3.jpg" alt="JIBER-TX slideshow">
</div>

<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-jiber/footer-jiber.png" alt="" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=30" target="_blank" class="btn btn-bordered jib-but m-t-50"><i class=" m-r-10 fa fa-cog"></i><?=$lang[$_COOKIE['ubi_lang']]['_JB_CATALOGUE']?></a>
  </div>
</div>
</div>


