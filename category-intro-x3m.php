<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-30 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">X3M FLIGHT</h1>
            <h5 class="text-white hint-text">
                <?=$lang[$_COOKIE['ubi_lang']]['_X3M_INTRO']?>
            </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i><?=$lang[$_COOKIE['ubi_lang']]['_ALL_CLOSER']?></p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td width="110" class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET1']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET1_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET2']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET2_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET3']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET3_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET4']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET4_TXT']?></td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white"><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET5']?></td>
                        <td><?=$lang[$_COOKIE['ubi_lang']]['_X3M_DET5_TXT']?></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/cat-x3m/x3m-first.png" alt="X3M" class="image-responsive-height">
        </div>
    </div>
</div>

<div class="category-intro-expl expl-x3m">
    <div class="category-intro-text hidden-xs">
        <div class="cat-open-close">
            &laquo;
        </div>
        <!--<div class="cat-open-open">
            &raquo;
        </div>-->
        <p class="m-b-50">
            <?=$lang[$_COOKIE['ubi_lang']]['_X3M_IMG_TXT']?>
        </p>                      
        <a class="btn btn-lrg btn-bordered x3m-but" href="index.php?p=catalog&idl=2">
            <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
        </a>            
    </div><!-- / category-intro-text-->
</div>

<div class="category-intro-text visible-xs-inline-block"><!-- mobile category-intro-text -->
    <p class="m-b-50">
        <?=$lang[$_COOKIE['ubi_lang']]['_X3M_IMG_TXT']?>
    </p>                      
    <a class="btn btn-lrg btn-bordered x3m-but" href="index.php?p=catalog&idl=2">
        <i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCTS']?>
    </a>            
</div><!-- / mobile category-intro-text -->
<!------------------ category options ------------------>
<div class="container-fluid p-b-50"> 
    <div class="col-sm-6 p-l-40 m-t-20 bordered-right text-center">
        <h2 class="text-white m-t-40">X3M S</h2>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_X3M_TECH1']?>
        </p>
        <img src="images/cat-x3m/x3m-s.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?> <span class="bold">32-120mm </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?><span class="bold"> 8-28mm </span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?>  <span class="bold">1500-15000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?> <span class="bold">3000-30000 Kg</span>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=2" class="btn btn-bordered x3m-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>

    <div class="col-sm-6 p-l-40 m-t-40 text-center">
        <h2 class="text-white m-t-20">X3M SO</h2>
        <p class="fs-15 text-white m-t-20">
            <?=$lang[$_COOKIE['ubi_lang']]['_X3M_TECH2']?> 
        </p>
        <img src="images/cat-x3m/x3m-so.png" class="image-responsive m-t-20" width="60%" alt="">
        <table class="table-tech m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    ⌀ <?=$lang[$_COOKIE['ubi_lang']]['_ALL_SHEAVE']?> <span class="bold">32-120mm </span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_LINE']?><span class="bold"> 8-28mm </span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_MAXLOAD']?>  <span class="bold">1500-15000 Kg</span> <br>
                    <?=$lang[$_COOKIE['ubi_lang']]['_ALL_BREAK']?> <span class="bold">3000-30000 Kg</span>
                </td>
            </tr>
        </table>
        <div style="width:100%;text-align:center;margin-top:20px;">
            <a href="index.php?p=catalog&idl=2" class="btn btn-bordered x3m-but"><?=$lang[$_COOKIE['ubi_lang']]['_ALL_PRODUCT']?></a>
        </div>
    </div>
</div><!------------------ / category options ------------------>

<div class="clearfix product-drawings-box features-img-right">
    <div class="container-fluid clearfix">
        <div class="row equal p-t-50">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-x3m/x3m-drawing-section1.jpg">
                    <img src="images/cat-x3m/x3m-drawing-section1.jpg" alt="Jiber-TX" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-x3m/x3m-drawing-section-photo1.jpg">
                    <img src="images/cat-x3m/x3m-drawing-section-photo1.jpg" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="row equal p-t-50">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont cat-tech-fff">
                <a class="fancybox" rel="group" href="images/cat-x3m/x3m-drawing-section-photo2.jpg">
                    <img src="images/cat-x3m/x3m-drawing-section-photo2.jpg" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-x3m/x3m-drawing-section2.jpg">
                    <img src="images/cat-x3m/x3m-drawing-section2.jpg" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
        </div>

        <div class="row equal p-t-50">
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont cat-tech-bg">
                <a class="fancybox" rel="group" href="images/cat-x3m/x3m-drawing-section3.jpg">
                    <img src="images/cat-x3m/x3m-drawing-section3.jpg" alt="Jiber-TX" class="img-responsive">
                </a>
            </div>
            <div class="col-md-6 col-sm-8 text-center cat-tech-cont">
                <a class="fancybox" rel="group" href="images/cat-x3m/x3m-drawing-section-photo3.jpg">
                    <img src="images/cat-x3m/x3m-drawing-section-photo3.jpg" alt="Jiber-TX" class="img-responsive"> 
                </a>
            </div>
        </div>

    </div>
</div>

<div class="container-fluid">
    <div class="extreme-esploso"></div>
</div>

<div class="product-carousel">
  <img src="images/cat-x3m/x3m-slider1.jpg" alt="X3M slideshow">
  <img src="images/cat-x3m/x3m-slider2.jpg" alt="X3M slideshow">
  <img src="images/cat-x3m/x3m-slider3.jpg" alt="X3M slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/cat-x3m/x3m-footer.png" alt="Ubi Maior Italia - x3m line" class="img-responsive">
    <a href="pdf/UbiMaiorItalia_2019_Catalog_eng.pdf#page=26" target="_blank" class="btn btn-bordered x3m-but m-t-50"><i class=" m-r-10 fa fa-cog"></i> <?=$lang[$_COOKIE['ubi_lang']]['_X3M_CATALOGUE']?></a>
  </div>
</div>
</div>
