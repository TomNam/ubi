<div class="container">
    <div class="row p-t-100 xs-p-t-0 xs-m-t-0 xs-m-b-0 xs-p-b-50 p-b-100 m-t-100 m-b-100">
        <div class="col-sm-7">
            <h1 class="text-white m-b-40 m-t-60">JIBER-TX</h1>
            <h5 class="text-white hint-text">
        JIBER-TX is a structural jib furler suitable for composite and textile forestay. 
        It can be fitted on every cable and furlers and it allows every boat to be equipped like a modern ocean solo racer.
      </h5>
            <p class="closer m-t-20"><i class="fa fa-plus-circle m-r-10"></i>Look closer </p>
            <div class="table-container">
                <table id="table-closer">
                    <tr>
                        <td class="all-caps text-white">JAVELIN AND SHUTTLE</td>
                        <td>&nbsp; ALUMINIUM alloy 6082T6, machined from a single piece and anodized</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">FREE TACK SHUTTLE</td>
                        <td>&nbsp;  Bearing system by UBI MAIOR ITALIA&#174;</td>
                    </tr>
                    <tr>
                        <td class="all-caps text-white">OTHER</td>
                        <td>&nbsp;  AISI 316 and 17-4Ph STEEL hub and screws</td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-sm-4 col-sm-offset-1 product-page-main-image">
            <img src="images/jiber-tx/jiber-tx-first.png" alt="X3M" class="image-responsive-height">
        </div>
    </div>
</div>
<div class="bg-white clearfix jiber-tx-ft features-img-right">
    <div class="container-fluid clearfix  p-b-50">
        <div class="row equal p-t-50">
            <div class="col-md-5 col-sm-8 text-center">
                <h2 class="padding-20">
                  Features
                </h2>
                <p class="fs-15 m-b-50">
                    The jib can be hoisted and dropped every time and the halyard tension can be adjusted continuously, as in a traditional jib furler.
                </p>
                <img src="images/jiber-tx/jiber-tx-esploso.png" alt="Jiber-TX" class="img-responsive">
                <p class="fs-15 m-t-50">
                    The advantage of this innovative system is the absence of the classic foil, which means less weight, less drag and less sag on the forestay, meaning a faster, higher and easier sailing. The furling system can be configured as single or continuous line, over- or under-deck.
                    JIBER-TX is a patent of UBI MAIOR ITALIA&#174;.
                </p>
                <!--<a href="#" class="btn btn-gray btn-bordered m-t-30 all-caps">Technical Specifications</a>-->

            </div>
            <div class="col-sm-6 clearfix"></div>
        </div>
    </div>
</div>
<div class="container-fluid p-t-50 p-b-50">
    <div class="col-sm-6 text-center bordered-right">
        <img src="images/jiber-tx/jiber-tx-tech.png" alt="JIBER-TX technical details" class="img-responsive">
    </div>
    <div class="col-sm-6 p-l-40">
        <h2 class="text-white m-t-40">
          Technical details
        </h2>
        <p class="fs-15 text-white m-t-20">
            The measures indicated are the minimum and maximum range. <br> For the single product's details see the catalog
        </p>
        <table id="table-tech" class="m-t-40 hint-text">
            <tr>
                <td><img src="images/icon-length.png" /></td>
                <td>
                    BOAT LENGHT <span class="bold">37-45 ft / 65-85 ft </span> <br>
                </td>
            </tr>
            <tr>
                <td><img src="images/icon-weight.png" /></td>
                <td>
                  MAX WORKING LOAD <span class="bold">1000-25000 Kg</span> <br>
                  BREAKING LOAD  <span class="bold">20000-50000 Kg</span> <br>
                  WEIGHT <span class="bold">1600-4200 gr</span>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="product-carousel">
  <img src="images/jiber-tx/jiber-tx_gal_1.jpg" alt="JIBER-TX slideshow">
  <img src="images/jiber-tx/jiber-tx_gal_2.jpg" alt="JIBER-TX slideshow">
</div>
<div class="container p-t-50 p-b-50 text-center">
<div class="row">
  <div class="col-sm-4 col-sm-push-4 col-xs-12">
    <img src="images/jiber-tx/jiber-tx-footer.png" alt="" class="img-responsive">
    <a href="https://dl.dropboxusercontent.com/u/3158723/UbiMaiorItalia_2017_Catalog_eng.pdf#page=30" target="_blank" class="btn btn-bordered m-t-50">SEE ALL OTHER JIBER'S PRODUCTS</a>
  </div>
</div>
</div>
